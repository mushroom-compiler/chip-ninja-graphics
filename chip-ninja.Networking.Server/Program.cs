﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using chip_ninja.Networking.Server;

namespace chip_ninja.Networking.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.Write("\n* * * * * * * * *");
            Console.Write("\n*  S E R V E R  *");
            Console.Write("\n* * * * * * * * *");
            Console.Write("\n");
            Console.ResetColor();

            Server server = new Server();
            server.startServer();
            //server.addAction("state", new ThreadStart());
            //Thread t = new Thread(() => { if (Console.ReadLine() == "exit") Environment.Exit(0); });
            //t.Start();

            while (true)
                if (Console.ReadLine() == "exit") Environment.Exit(0);

            //Console.ReadLine();
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Diagnostics;

using chip_ninja.Networking.Helper.Data;
using chip_ninja.Networking.Client;

namespace chip_ninja.Networking.Server
{
    public class Server
    {
        private List<Thread> threads;

        private Thread listenThread;
        private Thread sendThread;
        private UdpClient udp;
        private bool running;
        private Dictionary<string, ConnectedClient> connectedClients;
        //private Dictionary<string, ConnectedClient> simulatedClients;
        private List<ConnectedClient> connectedClientsList;
        private int numConnected = 0;
        Random rand;
        private Dictionary<string, Thread> actions;

        Stopwatch sW;
        TimeSpan tS;
        int bgIndex = 0, indexTmp = 0;

        //int port = 80;

        byte[] data;

        public Server()
        {
            running = false;
            connectedClients = new Dictionary<string, ConnectedClient>();
            connectedClientsList = new List<ConnectedClient>();
            rand = new Random();
            threads = new List<Thread>();
            actions = new Dictionary<string, Thread>();
        }

        public void addAction(string action, ThreadStart function)
        {
            Thread t = new Thread(function);
            actions.Add(action, t);
        }

        public void startServer()
        {
            string Host = System.Net.Dns.GetHostName();
            string IPv4 = null;

            for (int i = 0; i <= System.Net.Dns.GetHostEntry(Host).AddressList.Length - 1; i++)
                if (System.Net.Dns.GetHostEntry(Host).AddressList[i].IsIPv6LinkLocal == false)
                    IPv4 = System.Net.Dns.GetHostEntry(Host).AddressList[i].ToString();
            Console.WriteLine("Starting server on host " + Host + " : " + IPv4);

            //port = 0;
            udp = new UdpClient(80);
            //port = ((IPEndPoint)udp.Client.LocalEndPoint).Port;

            Console.WriteLine("UDP port : " + ((IPEndPoint)udp.Client.LocalEndPoint).Port.ToString());

            running = true;

            tS = new TimeSpan();
            sW = new Stopwatch();
            sW.Start();

            listenThread = new Thread(listenAndHandleMessages);
            listenThread.Priority = ThreadPriority.Highest;
            listenThread.Start();

            sendThread = new Thread(updateAndSendData);
            sendThread.Start();
        }

        public void updateLogic()
        {
        }


        void updateAndSendData()
        {
            while (running)
            {
                System.Threading.Thread.Sleep(10);

                if (connectedClients.Count > 0)
                {
                    lock (connectedClients)
                    {
                        foreach (ConnectedClient c in connectedClientsList)
                        {
                            foreach (ConnectedClient c2 in connectedClientsList)
                            {
                                if (c2.name != c.name)
                                {
                                    Data.data msg = new Data.data();

                                    msg.state = "position";
                                    msg.name = c2.name;
                                    msg.x = c2.x;
                                    msg.y = c2.y;
                                    msg.rectX = c2.rectX;
                                    msg.rectY = c2.rectY;
                                    msg.rectWidth = c2.rectWidth;
                                    msg.rectHeight = c2.rectHeight;
                                    msg.index = bgIndex;
                                    sendData(msg, c.name);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void listenAndHandleMessages()
        {
            while (running)
            {
                IPEndPoint ipendpoint = new IPEndPoint(IPAddress.Any, 80);
                data = udp.Receive(ref ipendpoint);
                object d = Data.DataHelper.DeserializeMsg<Data.data>(data);
                Data.data ds = (Data.data)d;

                if (ds.state == "connect")
                {
                    handleConnection(ds.name, ipendpoint);
                }
                else if (ds.state == "position")
                {
                    //Console.WriteLine(ds.name + " : " + ds.action + " : " + ds.x + " " + ds.y);
                    lock (connectedClients)
                    {
                        tS = sW.Elapsed;

                        //if (indexTmp != 0 && tS.TotalSeconds % indexTmp == 0)
                        //    bgIndex = indexTmp;
                        //if (indexTmp == 0 && tS.TotalSeconds % 4 == 0)
                        //    bgIndex = indexTmp;
                        /*
                        if (indexTmp != 0 && ((sW.ElapsedMilliseconds / 100) % indexTmp) == 1)
                            bgIndex = indexTmp;
                        if (indexTmp == 0 && ((sW.ElapsedMilliseconds / 100) % 4) == 1)
                            bgIndex = indexTmp;
                         */
                        int secondsElaspsed = rand.Next(3, 7);
                        int lastIndex = 0;
                        if (sW.Elapsed.Seconds == secondsElaspsed)
                        {
                            bgIndex = indexTmp;
                            lastIndex = indexTmp;
                            while (lastIndex == indexTmp)
                                lock (new object())
                                    indexTmp = rand.Next(0, 3);
                        }

                        //Console.WriteLine(tS.TotalMilliseconds);

                        for (int i = 0; i < connectedClientsList.Count; i++)
                        {
                            if (connectedClientsList[i].name != ds.name && i == 0)
                            {
                                connectedClientsList[i + 1].x = ds.x;
                                connectedClientsList[i + 1].y = ds.y;
                                connectedClientsList[i + 1].rectX = ds.rectX;
                                connectedClientsList[i + 1].rectY = ds.rectY;
                                connectedClientsList[i + 1].rectWidth = ds.rectWidth;
                                connectedClientsList[i + 1].rectHeight = ds.rectHeight;
                                connectedClientsList[i + 1].index = ds.index;

                                sendData(ds, connectedClientsList[i].name);
                            }
                            if (connectedClientsList[i].name != ds.name && i == 1)
                            {
                                connectedClientsList[i - 1].x = ds.x;
                                connectedClientsList[i - 1].y = ds.y;
                                connectedClientsList[i - 1].rectX = ds.rectX;
                                connectedClientsList[i - 1].rectY = ds.rectY;
                                connectedClientsList[i - 1].rectWidth = ds.rectWidth;
                                connectedClientsList[i - 1].rectHeight = ds.rectHeight;
                                connectedClientsList[i - 1].index = ds.index;

                                sendData(ds, connectedClientsList[i].name);
                            }
                        }

                        //foreach (ConnectedClient c in connectedClientsList)
                        //{
                        //    if (c.name != ds.name)
                        //    {
                        //        connectedClients[c.name].x = ds.x;
                        //        connectedClients[c.name].y = ds.y;
                        //        connec
                        //        c.x = ;
                        //        c.y = ;

                        //        sendData(ds, c.name);
                        //    }
                        //}
                    }
                }
                else
                    Console.WriteLine(ds.state + " is not defined");
            }
        }

        public void sendData(Data.data msg, string name)
        {
            byte[] dataD = Data.DataHelper.SerializeMessage<Data.data>(msg);

            if (connectedClients.ContainsKey(name))
            {
                if (connectedClients[name].ipendpoint != null)
                    udp.Send(dataD, dataD.Length, connectedClients[name].ipendpoint);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(name);
                Console.ResetColor();
                Console.Write(" could not be found");
                Console.WriteLine();
            }
        }

        public void sendData(string msg, string name)
        {
            byte[] dataD = System.Text.Encoding.ASCII.GetBytes(msg);

            if (connectedClients.ContainsKey(name))
                if (connectedClients[name].ipendpoint != null)
                    udp.Send(dataD, dataD.Length, connectedClients[name].ipendpoint);
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(name);
                    Console.ResetColor();
                    Console.Write(" Could not connect to " + name);
                    Console.WriteLine();
                }
        }

        public void handleConnection(string name, IPEndPoint ipep)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.Write(name);
            Console.ResetColor();
            Console.Write(" is trying to connect : Status :");

            lock (connectedClients)
            {
                if (!connectedClients.ContainsKey(name) && connectedClients.Count < 2)
                {
                    connectedClientsList.Add(new ConnectedClient(name, ipep, 0, 0, 0, 0, 0, 0, 0));
                    connectedClients.Add(name, new ConnectedClient(name, ipep,0, 0, 0, 0, 0, 0, 0));
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(" Accepted");
                    Console.ResetColor();
                    Console.Write(" ({0})\n", ipep.ToString());

                    Data.data ds = new Data.data();
                    ds.state = "accept";
                    ds.name = name;
                    sendData(ds, name);
                }
                else if (connectedClients.Count >= 2)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(" Rejected");
                    Console.ResetColor();
                    Console.Write(" (Too many players)\n");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(" Rejected");
                    Console.ResetColor();
                    Console.Write(" (Name already exists)\n");
                }
            }
        }
    }

}

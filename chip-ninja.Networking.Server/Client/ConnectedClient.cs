﻿using System.Net;

namespace chip_ninja.Networking.Client
{
    public class ConnectedClient
    {
        public string name;
        public IPEndPoint ipendpoint;
        public float x;
        public float y;

        public int rectX, rectY, rectWidth, rectHeight;

        public int index;


        public ConnectedClient(string n, IPEndPoint ipep, float X, float Y, int rectX, int rectY, int rectWidth, int rectHeight, int index)
        {
            name = n;
            ipendpoint = ipep;
            x = X;
            y = Y;
            this.index = index;
            
            //Rect
            this.rectX = rectX;
            this.rectY = rectY;
            this.rectWidth = rectWidth;
            this.rectHeight = rectHeight;
        }
        
        public ConnectedClient(string n, float X, float Y, int rectX, int rectY, int rectWidth, int rectHeight, int index)
        {
            name = n;
            x = X;
            y = Y;
            this.index = index;
            
            //Rect
            this.rectX = rectX;
            this.rectY = rectY;
            this.rectWidth = rectWidth;
            this.rectHeight = rectHeight;
        }
    }
}

﻿using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using chip_ninja.Networking.Helper.Data;

namespace chip_ninja.Networking.Client
{
    public class Client
    {
        UdpClient udpClient;
        IPEndPoint ipendpoint;
        //int port;
        Thread listenThread;
        public bool connected;
        public int x;
        public int y;
        public int rectX, rectY, rectWidth, rectHeight;
        public string name;
        public int index;
        public Dictionary<string, ConnectedClient> connectedClients;

        public Client(string n, string ip)
        {
            //this.port = port;
            connectedClients = new Dictionary<string, ConnectedClient>();
            name = n;
            ipendpoint = new IPEndPoint(IPAddress.Parse(ip), 80);
            udpClient = new UdpClient();
            udpClient.Connect(ipendpoint);

            listenThread = new Thread(listenForData);
            listenThread.Priority = ThreadPriority.Highest;
            listenThread.Start();

            Data.data connectMsg = new Data.data();
            connectMsg.state = "connect";
            connectMsg.name = name;
            sendData(connectMsg);
        }

        void listenForData()
        {
            while (true)
            {
                byte[] data = udpClient.Receive(ref ipendpoint);
                object d = Data.DataHelper.DeserializeMsg<Data.data>(data);
                Data.data ds = (Data.data)d;

                if (ds.state == "accept" && ds.name == name)
                    connected = true;

                if (connected)
                {
                    switch (ds.state)
                    {
                        case "position":
                            if (connectedClients.ContainsKey(ds.name))
                            {
                                connectedClients[ds.name].x = (int)ds.x;
                                connectedClients[ds.name].y = (int)ds.y;

                                connectedClients[ds.name].rectX = (int)ds.rectX;
                                connectedClients[ds.name].rectY = (int)ds.rectY;
                                connectedClients[ds.name].rectWidth = (int)rectWidth;
                                connectedClients[ds.name].rectHeight = (int)rectHeight;

                                connectedClients[ds.name].index = (int)ds.index;
                            }
                            else
                            {
                                lock (connectedClients)
                                    connectedClients.Add(ds.name, new ConnectedClient(ds.name, ds.x, ds.y, ds.rectX, ds.rectY, ds.rectWidth, ds.rectHeight, ds.index));
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        public void sendData(string action, string name, float x, float y, int index)
        {
            Data.data data = new Data.data();
            data.state = action;
            data.name = name;
            data.x = x;
            data.y = y;
            data.index = index;
            sendData(data);
        }

        public void sendData(Data.data ds)
        {
            byte[] b = Data.DataHelper.SerializeMessage<Data.data>(ds);
            udpClient.Send(b, b.Length);
        }

        public void sendMessage(string msg)
        {
            msg = name + "$" + msg;
            byte[] data = Encoding.ASCII.GetBytes(msg);
            udpClient.Send(data, data.Length);
        }

        public override string ToString()
        {
            return name + " : (" + x.ToString() + "," + y.ToString() + ")";
        }
    }
}

// Shader for creating a Depth of field postprocess.
// Also includes a technique for rendering a depth
// values of the scene.

// Depth buffer resolve parameters
// x = focus distance
// y = focus range
// z = near clip
// w = far clip / ( far clip - near clip )
float4 DoFParams;

#define SAMPLE_COUNT 12

float4x4 WorldViewProj;

float3 ViewVector;

uniform extern float2 Taps[SAMPLE_COUNT];
uniform extern float DiscRadius = 2.75f;
uniform extern float2 TexelSize;

texture SceneTex;
texture SceneBlurTex;
texture SceneDepthTex;


// Textures
sampler2D SceneSampler = sampler_state
{
	Texture = <SceneTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
};
sampler2D SceneBlurSampler = sampler_state
{
	Texture = <SceneBlurTex>;
	MinFilter = Anisotropic;
    MagFilter = Anisotropic;
    MipFilter = Linear;
};
sampler2D SceneDepthSampler = sampler_state
{
	Texture = <SceneDepthTex>;
	MinFilter = Point;
	MagFilter = Point;
	MipFilter = None;
};

struct OutputVS
{
    float4 posH    : POSITION0;
    float2 depth   : TEXCOORD0;
};

//pack the depth in a 32-bit rgba color
float4 mapDepthToARGB32(const float value)
{
	const float4 bitSh = float4(256.0 * 256.0 * 256.0, 256.0 * 256.0, 256.0, 1.0);
	const float4 mask = float4(0.0, 1.0 / 256.0, 1.0 / 256.0, 1.0 / 256.0);
	float4 res = frac(value * bitSh);
	res -= res.xxyz * mask;
	return res;
}

//unpack the depth from a 32-bit rgba color
float getDepthFromARGB32(const float4 value)
{
	const float4 bitSh = float4(1.0 / (256.0 * 256.0 * 256.0), 1.0 / (256.0 * 256.0), 1.0 / 256.0, 1.0);
	return(dot(value, bitSh));
}

///Storing the depth to a texture
OutputVS DepthVS(float3 posL : POSITION0)
{
    // Zero out our output.
	OutputVS outVS = (OutputVS)0;
	
	// Transform to homogeneous clip space.
	outVS.posH = mul(float4(posL, 1.0f), WorldViewProj);
	
	outVS.depth = outVS.posH.zw;

	// Done--return the output.
    return outVS;
}

float4 DepthPS_ARGB32(float2 depth : TEXCOORD0) : COLOR
{
	// z / w; put depth in the [0, 1] range
	float d = depth.x / depth.y;
	
	return mapDepthToARGB32(d);
}

float4 DepthPS_R32F(float2 depth : TEXCOORD0) : COLOR
{
	// z / w; put depth in the [0, 1] range
	return depth.x / depth.y;
}

///Drawing the depth of field
float4 DepthOfFieldPS_ARGB32( float2 texCoord : TEXCOORD0 ) : COLOR0
{
    float4 vDepthTexel      = tex2D( SceneDepthSampler, texCoord );
    float4 vSceneSampler      = tex2D( SceneSampler, texCoord );
    float4 vSceneBlurSampler  = tex2D( SceneBlurSampler, texCoord );
    
    // This is only required if using a floating-point depth buffer format
	//fDepth = 1.0 - fDepth;
	//float fDepth = vDepthTexel.x;
	float fDepth = getDepthFromARGB32(vDepthTexel);
    
    // Back-transform depth into camera space
    float fSceneZ = ( -DoFParams.z * DoFParams.w ) / ( fDepth - DoFParams.w );
    
    // Compute blur factor
    float fBlurFactor = saturate( abs( fSceneZ - DoFParams.x ) / DoFParams.y );
    
    // Compute resultant pixel
    float4 color;
    color.rgb = lerp( vSceneSampler.rgb, vSceneBlurSampler.rgb, fBlurFactor );
    color.a		= 1.0;

    return color;
}

float4 DepthOfFieldPS_R32F( float2 texCoord : TEXCOORD0 ) : COLOR0
{
    float2 vDepthTexel      = tex2D( SceneDepthSampler, texCoord );
    float4 vSceneSampler      = tex2D( SceneSampler, texCoord );
    float4 vSceneBlurSampler  = tex2D( SceneBlurSampler, texCoord );
    
    // This is only required if using a floating-point depth buffer format
	//fDepth = 1.0 - fDepth;
	float fDepth = vDepthTexel.x;
    
    // Back-transform depth into camera space
    float fSceneZ = ( -DoFParams.z * DoFParams.w ) / ( fDepth - DoFParams.w );
    
    // Compute blur factor
    float fBlurFactor = saturate( abs( fSceneZ - DoFParams.x ) / DoFParams.y );
    
    // Compute resultant pixel
    float4 color;
    color.rgb = lerp( vSceneSampler.rgb, vSceneBlurSampler.rgb, fBlurFactor );
    color.a		= 1.0;

    return color;
}

technique DepthOfField_ARGB32
{
    pass P0
    {
        PixelShader = compile ps_2_0 DepthOfFieldPS_ARGB32();
    }
}

technique DepthOfField_R32F
{
    pass P0
    {
        PixelShader = compile ps_2_0 DepthOfFieldPS_R32F();
    }
}

technique BuildDepthBuffer_ARGB32
{
    pass P0
    {
		VertexShader = compile vs_2_0 DepthVS();
        PixelShader = compile ps_2_0 DepthPS_ARGB32();
    }
}

technique BuildDepthBuffer_R32F
{
    pass P0
    {
		VertexShader = compile vs_2_0 DepthVS();
        PixelShader = compile ps_2_0 DepthPS_R32F();
    }
}

//Phong shading

float4x4 World;
float4x4 WorldInvTrans;
float4x4 WorldViewProj;

float4	LightDiffuse0;
float4	LightAmbient0;
float4	LightSpecular0;
float3	LightDir0;

float SpecPower0;
float SpecIntens0;

float3   EyePos;

texture  DiffuseTex;
float TexScale;

sampler TexS = sampler_state
{
	Texture = <DiffuseTex>;
	MinFilter = Anisotropic;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	MaxAnisotropy = 8;
	AddressU  = WRAP;
    AddressV  = WRAP;
};
 
struct OutputVS
{
    float4 posH    : POSITION0;
    float3 normalW : TEXCOORD0;
    float3 toEyeW  : TEXCOORD1;
    float2 tex0    : TEXCOORD2;
    float3 pos	   : TEXCOORD3;
};


OutputVS PhongVS(float3 posL : POSITION0, float3 normalL : NORMAL0, float2 tex0: TEXCOORD0)
{
    // Zero out our output.
	OutputVS outVS = (OutputVS)0;
	
	// Transform normal to world space.
	outVS.normalW = mul(float4(normalL, 0.0f), WorldInvTrans).xyz;
	
	// Transform vertex position to world space.
	float4 posW  = mul(float4(posL, 1.0f), World);
	
	outVS.pos = posW.xyz;
	
	// Compute the unit vector from the vertex to the eye.
	outVS.toEyeW = EyePos;
	
	// Transform to homogeneous clip space.
	outVS.posH = mul(float4(posL, 1.0f), WorldViewProj);
	
	// Pass on texture coordinates to be interpolated in rasterization.
	outVS.tex0 = tex0;

	// Done--return the output.
    return outVS;
}

float4 PhongPS(float3 normalW : TEXCOORD0, float3 toEyeW  : TEXCOORD1, float2 tex0 : TEXCOORD2, float3 pos : TEXCOORD3) : COLOR
{
	// Interpolated normals can become unnormal--so normalize.
	normalW = normalize(normalW);
	toEyeW  = normalize(toEyeW - pos);
	
	// Light vector is opposite the direction of the light.
	float3 lightVecW = -LightDir0;
	
	// Compute the reflection vector.
	float3 r = normalize(reflect(-lightVecW, normalW));
	
	// Determine the diffuse light intensity that strikes the vertex.
	float s = saturate(dot(lightVecW, normalW));
	
	// Compute the ambient, diffuse and specular terms separatly. 
	float3 diffuse = s * LightDiffuse0.rgb;
	float3 specular = LightSpecular0.rgb * SpecIntens0 * pow(max(dot(toEyeW, r), 0), SpecPower0);
	
	// Get the texture color.
	float4 texColor = tex2D(TexS, tex0 * TexScale);
	
	// Combine the color from lighting with the texture color.
	float3 color = (diffuse + LightAmbient0) * texColor.rgb + specular;
		
	// Sum all the terms together and copy over the diffuse alpha.
    return float4(color, texColor.a);
}

technique Phong
{
    pass P0
    {
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_2_0 PhongVS();
        pixelShader  = compile ps_2_0 PhongPS();
    }
}
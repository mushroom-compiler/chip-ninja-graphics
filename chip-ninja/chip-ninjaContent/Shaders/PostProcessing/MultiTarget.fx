float screenWidth;
float screenHeight;
float4 ambientColor;

float lightStrength;
float lightDecay;
float3 lightPosition;
float4 lightColor;
float lightRadius;
float specularStrength;

bool highQuality;
bool metal = true;

Texture NormalMap;
sampler NormalMapSampler = sampler_state {
	texture = <NormalMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU = mirror;
	AddressV = mirror;
};

Texture ColorMap;
sampler ColorMapSampler = sampler_state {
	texture = <ColorMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU = mirror;
	AddressV = mirror;
};

struct Fragment
{
	float4 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
	float4 Color : COLOR0;
};

struct Color
{
	float4 Color : COLOR0;
};

Fragment VS_VS(float4 inPos: POSITION0, float2 texCoord: TEXCOORD0, float4 color: COLOR0)
{
	Fragment Output = (Fragment)0;
	
	Output.Position = inPos;
	Output.TexCoord = texCoord;
	Output.Color = color;
	
	return Output;
}

Color PS_PointLight(Fragment PSIn) : COLOR0
{	
	Color Output = (Color)0;
	
	float3 normal;
	float3 lightDirNorm;
	float3 halfVec;
	float amount;
	float3 reflect;
	float specular;

	float4 colorMap = tex2D(ColorMapSampler, PSIn.TexCoord);
	//Future use :
	if (highQuality)
		normal = (2.0f * (tex2D(NormalMapSampler, PSIn.TexCoord))) - 1.0f;
	//Not sure :
	//float normal2 = tex2D(NormalMapSampler, PSIn.TexCoord).rgb;
	
	float3 pixelPosition;
	pixelPosition.x = screenWidth * PSIn.TexCoord.x;
	pixelPosition.y = screenHeight * PSIn.TexCoord.y;
	pixelPosition.z = 0;

	float3 lightDirection = lightPosition - pixelPosition;
	//Future use : 
	if (highQuality)
		lightDirNorm = normalize(lightDirection);
	//Future use : 
	if (highQuality)
		halfVec = float3(0, 0, 1);
	
	//Future use : 
	if (highQuality)
		amount = max(dot(normal, lightDirNorm), 0);
	float coneAttenuation = saturate(1.0f - length(lightDirection) / lightDecay); 
	
	//Future use : 
	if (highQuality)
	{
		reflect = normalize(2 * amount * normal - lightDirNorm);
	//Future use : 
		specular = min(pow(saturate(dot(reflect, halfVec)), 10), amount);
	}
	
	if (metal)
	{
		//float3 reflect = normalize(2 * amount * normal - lightDirNorm);
		//float specular = min(pow(saturate(dot(reflect, halfVec)), 10), amount);
		
		//if(normal2 > 0.0f)
		if (highQuality)
			Output.Color = colorMap * coneAttenuation * lightColor * lightStrength + (specular * coneAttenuation * specularStrength) /*- spcular*/;
		else
			Output.Color = colorMap * coneAttenuation * lightColor * lightStrength;

		//Output.Color = colorMap * float4(reflect, lightColor.a) + coneAttenuation + specular / ;
		//else Output.Color = float4(0, 0, 0, 0);

		//else Output.Color = colorMap * float4(.1,.1,.1,1);
	}
	else
	{
		//special effect 1
		//Output.Color = colorMap * coneAttenuation * lightColor * lightStrength + coneAttenuation - specular + sin(amount)*4397.33 - floor(sin(amount)*4397.33) - abs(2.*coneAttenuation - floor(coneAttenuation) - 1.) + .1;
		Output.Color = colorMap * coneAttenuation * lightColor * lightStrength + coneAttenuation + specular;
		//Output.Color = colorMap * coneAttenuation * lightColor * lightStrength + coneAttenuation * 0.2f;
	}

	return Output;
}

technique DeferredPointLight
{
    pass Pass1
    {
		VertexShader = compile vs_3_0 VS_VS();
        PixelShader = compile ps_3_0 PS_PointLight();
    }
}


#include "VertexShader_3_0.fxh"

float BlurDistance = 0.003f;

texture ColorMap;
sampler ColorMapSampler : register(s0)
{
   Texture = <ColorMap>;
   MinFilter = Linear;
   MagFilter = Linear;
   MipFilter = Linear;   
   AddressU  = Clamp;
   AddressV  = Clamp;
};

float4 DoF_PS(float2 texCoord: TEXCOORD0) : COLOR0
{
	float4 Color;
	
	Color  = tex2D( ColorMapSampler, float2(texCoord.x+BlurDistance, texCoord.y+BlurDistance));
	Color += tex2D( ColorMapSampler, float2(texCoord.x-BlurDistance, texCoord.y-BlurDistance));
	Color += tex2D( ColorMapSampler, float2(texCoord.x+BlurDistance, texCoord.y-BlurDistance));
	Color += tex2D( ColorMapSampler, float2(texCoord.x-BlurDistance, texCoord.y+BlurDistance));

	Color = Color / 4;	
	
    return Color;
 
    //float2 center = {0.5f, 0.5f};
	//float maxDistSQR = 0.7071f; //precalulated sqrt(0.5f)

	//float2 diff = abs(texCoord - center);
	//float distSQR = length(diff);
											
	//float blurAmount = (distSQR / maxDistSQR) / 100.0f;

	//float2 prevCoord = texCoord;
	//prevCoord[0] -= blurAmount;

	//float2 nextCoord = texCoord;
	//nextCoord[0] += blurAmount;

	//float4 color = ((tex2D(ColorMapSampler, texCoord)
	//			  + tex2D(ColorMapSampler, prevCoord)
	//			  + tex2D(ColorMapSampler, nextCoord))/3.0f);
		
	//return color;
}

technique DoF
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS_VS();
		PixelShader = compile ps_3_0 DoF_PS();
	}
}
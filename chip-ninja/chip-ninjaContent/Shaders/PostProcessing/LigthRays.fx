#include "VertexShader_3_0.fxh"

int NBSAMPLES = 64;

float2 lightPosOnScreen;
float2 direction;

float Density = .5f;
float Decay = .95f;
float Weight = 1.0f;
float Exposure = .15f;

float g = 0.85f;
float g2 = 0.7225f;

sampler2D Scene: register(s0){
	AddressU = Clamp;
	AddressV = Clamp;
};

float4 lightRay_PS( float2 texCoord : TEXCOORD0 ) : COLOR0
{
	half2 DeltaTexCoord = (texCoord - lightPosOnScreen.xy) * 1.0f / NBSAMPLES * Density;
	float fCos = dot(lightPosOnScreen, direction) / length(direction);
	half3 color = tex2D(Scene,texCoord);
	half IlluminationDecay = 1.0f;
	
	for(int i = 0; i < NBSAMPLES; i++)
	{
		texCoord -= DeltaTexCoord;
		color +=  tex2D(Scene, texCoord) * IlluminationDecay * Weight;
		IlluminationDecay *= Decay;
	}
	
	float3 sColor = float3(color * RayleighPhase(fCos) + MiePhase(g, g2, fCos) * 0.3f * float3(0.713, 0.494, 0.356));
	float3 cHDR = HDR(sColor, Exposure + 0.3f);
	//float cLuminescence = cHDR.r * 0.27f + cHDR.g * 0.67f + cHDR.b * 0.06f;
    return float4(ToneMap(cHDR) /** cLuminescence*/, 1);

//	//apparition ?
//    //return float4(ToneMap(HDR(sin(tan(color) * cos( RayleighPhase(fCos)) + MiePhase(g, g2, fCos) * 0.3f) * float3(0.713, 0.494, 0.356), Exposure + 0.3f)), 1);
//    //menu ?
//	//return float4(ToneMap(HDR(log(tan(color) * cos(exp( RayleighPhase(fCos))) + MiePhase(g, g2, fCos) * 0.3f) * float3(0.713, 0.494, 0.356), Exposure + 0.3f)), 1);
}

technique LightRay
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS_VS();
		PixelShader = compile ps_3_0 lightRay_PS();
	}
}

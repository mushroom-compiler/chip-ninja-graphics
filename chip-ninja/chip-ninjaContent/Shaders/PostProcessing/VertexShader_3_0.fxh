struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TexCoord0;    
};
struct VertexShaderInput
{
    float4 Position : POSITION0;
	float2 TexCoord : TexCoord0;	
};
VertexShaderOutput VS_VS(VertexShaderInput input)
{
    VertexShaderOutput output = (VertexShaderOutput)0;
    
    output.Position = float4(input.Position.xyz,1);
    output.TexCoord = input.TexCoord;        
    
    return output;
}

float RayleighPhase(float fCos)
{
	return 0.75f * (1.0f + fCos*fCos);
}

float MiePhase(float g, float g2, float fCos)
{
	return 1.5f * ((1.0f - g2) / (2.0f + g2)) * (1.0 + fCos*fCos) / pow(abs(1.0f + g2 - 2.0f*g2*fCos), 1.5f);
}

float3 HDR( float3 LDR, float Exposure)
{
	return 1.0f - exp( - Exposure * LDR );
}

float3 ToneMap(float3 HDR)
{
	return (HDR / (HDR + .5f));
}

//float4 BrightPass(float2 texCoord)
//{
//    //float4 c = tex2D(TextureSampler, texCoord);

//    return saturate((c - BloomThreshold) / (1 - BloomThreshold));
//}
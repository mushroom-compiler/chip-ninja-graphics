#include "VertexShader_3_0.fxh"

float ambient;
float4 ambientColor;
float ambientLight;

Texture ColorMap;
sampler ColorMapSampler = sampler_state {
	texture = <ColorMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU = mirror;
	AddressV = mirror;
};

Texture ShadowMap;
sampler ShadowMapSampler = sampler_state {
	texture = <ShadowMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU = mirror;
	AddressV = mirror;
};

Texture NormalMap;
sampler NormalMapSampler = sampler_state {
	texture = <NormalMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU = mirror;
	AddressV = mirror;
};

float4 PS_Combined(float4 color : COLOR0, float2 texCoords : TEXCOORD0) : COLOR0
{	
	float4 color2 = tex2D(ColorMapSampler, texCoords);
	float4 shading = tex2D(ShadowMapSampler, texCoords);
	float normal = tex2D(NormalMapSampler, texCoords).rgb;
	
	if (normal > 0.0f)
	{
		//float4 finalColor = color2 * ambientColor;
		//finalColor += shading;
		//return finalColor;

		float4 finalColor = color2 * ambientColor * ambient;
		finalColor += (shading * color2) * ambientLight;
		
		return finalColor;
	}

	else
	{
		return float4(0, 0, 0, 0);
	}
}

technique DeferredCombined
{
    pass Pass1
    {
		VertexShader = compile vs_3_0 VS_VS();
        PixelShader = compile ps_3_0 PS_Combined();
    }
}
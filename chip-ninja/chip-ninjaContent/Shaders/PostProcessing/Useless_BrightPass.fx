uniform extern float BloomThreshold;

float2 halfPixel;
sampler TextureSampler : register(s0);


float4 BrightPassPS(float2 texCoord : TEXCOORD0) : COLOR0
{
	texCoord -= halfPixel;
    float4 c = tex2D(TextureSampler, texCoord);

    return saturate((c - BloomThreshold) / (1 - BloomThreshold));
}


technique BloomExtract
{
    pass P0
    {
        PixelShader = compile ps_3_0 BrightPassPS();
        
        ZWriteEnable = true;
    }
}
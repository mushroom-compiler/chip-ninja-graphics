#include "VertexShader_3_0.fxh"

float2 lightPosOnScreen;

float2 screenRatio = float2(16,9);

float sunSize = 1500;

sampler2D Scene: register(s0){
	AddressU = Clamp;
	AddressV = Clamp;
};

texture lightTex;
sampler LightTex = sampler_state
{
    Texture = (lightTex);
    AddressU = CLAMP;
    AddressV = CLAMP;
};

float4 LightMask_PS(float2 texCoord : TEXCOORD0 ) : COLOR0
{
	float4 color = 0;
	
	color += (pow(tex2D(LightTex,.5 - ((texCoord - lightPosOnScreen) * screenRatio) / (sunSize) * .5f),2)) * 2;						
	
	return color * tex2D(Scene,texCoord);
}

technique LightMask
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS_VS();
		PixelShader = compile ps_3_0 LightMask_PS();
	}
}
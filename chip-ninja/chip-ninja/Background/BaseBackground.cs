﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using chip_ninja.Components;
using chip_ninja.Graphics.Particule;
using chip_ninja.Graphics.PostProcess;
using chip_ninja.Graphics.PostProcess.NormalMapping;
using chip_ninja.Graphics.PostProcess.RayShaft;
using chip_ninja.Graphics.PostProcess.DepthOfField;
using chip_ninja.Background;
using chip_ninja.Enemies;

namespace chip_ninja.Background
{
    public class BaseBackground
    {
        protected string currentLevel;
        protected chip_ninja game;
        protected Song currentSong;

        public BaseBackground(Game game)
        {
            this.game = (chip_ninja)game;
        }

        public virtual void Enter(chip_ninja game)
        {
        
        }

        public virtual void PlaySong()
        {
            if (currentSong != null)
                MediaPlayer.Play(currentSong);
        }

        public virtual void Update(GameTime gametime, Camera camera)
        {
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch SpriteBatch, Map map, Camera camera)
        {
        }
    }
}

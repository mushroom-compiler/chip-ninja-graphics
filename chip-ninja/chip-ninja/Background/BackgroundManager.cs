﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using chip_ninja.Components;
using chip_ninja.Graphics.Particule;
using chip_ninja.Graphics.PostProcess;
using chip_ninja.Graphics.PostProcess.NormalMapping;
using chip_ninja.Graphics.PostProcess.RayShaft;
using chip_ninja.Graphics.PostProcess.DepthOfField;
using chip_ninja.Background;
using chip_ninja.Enemies;

namespace chip_ninja.Background
{
    public class BackgroundManager
    {
        public BaseBackground currentBg;
        chip_ninja game;

        public BackgroundManager(Game game)
        {
            this.game = (chip_ninja)game;
        }

        public void ChangeBackGround(BaseBackground bg)
        {
            bg.Enter(game);
            currentBg = bg;
            currentBg.PlaySong();
        }

        public void Update(GameTime gameTime, Camera camera)
        {
            currentBg.Update(gameTime, camera);
        }

        public void Draw(GameTime gameTime, SpriteBatch SpriteBatch, Map map, Camera camera)
        {
            currentBg.Draw(gameTime, SpriteBatch, map, camera);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;

using chip_ninja.Components;
using chip_ninja.Graphics.PostProcess.NormalMapping;
using chip_ninja.Enemies;
using chip_ninja.Background;
using chip_ninja.Background.FunkyCircle;
using chip_ninja.Graphics.PostProcess.Bloom;

namespace chip_ninja.Background
{
    public class BackgroundLevel3 : BaseBackground
    {
        GraphicsDevice g;

        BloomComponent bloom;

        Texture2D stripe;

        List<FunkyCircle.FunkyCircle> fCircles = new List<FunkyCircle.FunkyCircle>();

        private static readonly ThreadLocal<Random> rand = new ThreadLocal<Random>(() => new Random());

        public BackgroundLevel3(Game game, GraphicsDevice Graphics)
            : base(game)
        {
            currentLevel = "Je sais plus...";
            this.g = Graphics;

            stripe = game.Content.Load<Texture2D>("crt_stripe");

            for (int i = 0; i < 4; i++)
                fCircles.Add(new FunkyCircle.FunkyCircle(game.Content.Load<Texture2D>("circle"), new Vector2(rand.Value.Next(0, 1920), rand.Value.Next(0, 999)), (float)(rand.Value.NextDouble() + .3f) * 1.7f, Color.FromNonPremultiplied(rand.Value.Next(0, 255), rand.Value.Next(0, 254), rand.Value.Next(0, 253), 255)));

            bloom = new BloomComponent(game);
            bloom.LoadContent();
            bloom.Settings = BloomSettings.preset;

            currentSong = game.Content.Load<Song>("Songs/Quazar - Funky Stars");
        }

        float tempo = 0;

        public override void Enter(chip_ninja game)
        {
            game.gameScreen.map = new Map("level1", game.Content, game.gameScreen.EnemyList);
            game.player = new Player(game.gameScreen.map.PlayerSpawn);
            game.player.LoadContent(game.Content);
            game.gameScreen.camera = new Camera(game.gameScreen.map.PlayerSpawn);

        }

        public override void Update(GameTime gameTime, Camera camera)
        {
            for (int i = 0; i < fCircles.Count; i++)
                fCircles[i].Update((float)gameTime.ElapsedGameTime.TotalSeconds / 10);

            fCircles.RemoveAll(x => !x.visible);

            tempo += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (fCircles.Count <= 8 || tempo >= 450)
            {
                fCircles.Add(new FunkyCircle.FunkyCircle(game.Content.Load<Texture2D>("circle"), new Vector2(rand.Value.Next((int)Math.Abs(camera.Matrix.Translation.X) - (((int)Math.Abs(camera.Matrix.Translation.X) < 500) ? 0 : 500), 2200 + (int)Math.Abs(camera.Matrix.Translation.X)), rand.Value.Next((int)Math.Abs(camera.Matrix.Translation.Y), (int)Math.Abs(camera.Matrix.Translation.Y) + 800)), (float)(rand.Value.NextDouble() + .3f) * 1.7f, Color.FromNonPremultiplied(rand.Value.Next(0, 255), rand.Value.Next(0, 254), rand.Value.Next(0, 253), 255)));
                tempo = 0;
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch SpriteBatch, Map map, Camera camera)
        {
            if (game.effetcs)
                bloom.SetRT();

            g.Clear(Color.Black);

            SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, camera.Matrix);

            for (int i = 0; i < fCircles.Count; i++)
                fCircles[i].Draw(SpriteBatch);

            if (game.stateManager.CurrentState == game.gameScreen)
            {
                game.player.Draw(SpriteBatch, gameTime);
                for (int i = 0; i < game.gameScreen.EnemyList.Count; i++)
                    game.gameScreen.EnemyList[i].Draw(SpriteBatch, gameTime);
                game.gameScreen.map.Draw(SpriteBatch);
            }
                SpriteBatch.End();
            
            if (game.effetcs)
                bloom.Draw(gameTime);

            SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            SpriteBatch.Draw(game.Content.Load<Texture2D>("stripes"), Vector2.Zero, Color.Black);
            SpriteBatch.End();
        }
    }
}

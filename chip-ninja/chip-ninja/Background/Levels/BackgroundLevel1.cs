﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using chip_ninja.Components;
using chip_ninja.Graphics.Particule;
using chip_ninja.Graphics.PostProcess;
using chip_ninja.Graphics.PostProcess.RayShaft;
using chip_ninja.Enemies;
using chip_ninja.Interfaces;

namespace chip_ninja.Background
{
    public class BackgroundLevel1 : BaseBackground
    {
        Parallax parallax;

        RenderTarget2D scene, bG, Map;

        PostProcessManager ppm;
        RayShaft rays;

        GraphicsDevice g;
        Texture2D texture;
        Texture2D secondTexture;

        float time = 2.3f;

        public BackgroundLevel1(Game game, GraphicsDevice Graphics)
            : base(game)
        {
            currentLevel = "Under The Pin";

            this.g = Graphics;

            parallax = new Parallax(this.game);
            parallax.AddBackground("Textures/Backgrounds/bg_3", 2f, game.Content);
            parallax.AddBackground("Textures/Backgrounds/bg_2", 3f, game.Content);
            parallax.AddBackground("Textures/Backgrounds/bg_1", 4f, game.Content);

            ppm = new PostProcessManager(this.game);

            rays = new RayShaft(this.game, Vector2.One * .5f, @"Textures/flare", 1.5f, .97f, .97f, .5f, .25f, 64, new Vector2(16, 9));
            rays.lightPosOnScreen = Vector2.One * .5f;

            ppm.AddEffect(rays);
            ppm.ExtractParameters();

            scene = new RenderTarget2D(g, this.game.screenRectangle.Width, this.game.screenRectangle.Height, false, SurfaceFormat.Color, DepthFormat.None);
            bG = new RenderTarget2D(g, this.game.screenRectangle.Width, this.game.screenRectangle.Height, false, SurfaceFormat.Color, DepthFormat.None);


            currentSong = game.Content.Load<Song>("Songs/TrialsOfMan");
            texture = new Texture2D(g, 1, 1, false, SurfaceFormat.Color);
            texture.SetData<Color>(new Color[] { Color.FromNonPremultiplied(255,255,255, 175) });
            secondTexture = new Texture2D(g, 1, 1, false, SurfaceFormat.Color);
            secondTexture.SetData<Color>(new Color[] { Color.FromNonPremultiplied(255, 0, 0, 175) });
        }

        public override void Enter(chip_ninja game)
        {
            game.gameScreen.map = new Map("level1", game.Content, game.gameScreen.EnemyList);
            game.gameScreen.EnemyList = new List<IAdverseEntity>( game.gameScreen.map.EnemyList);
            game.player = new Player(game.gameScreen.map.PlayerSpawn);
            game.player.LoadContent(game.Content);
            game.gameScreen.camera = new Camera(game.gameScreen.map.PlayerSpawn);
        }

        public override void Update(GameTime gameTime, Camera camera)
        {
            time += (float)gameTime.ElapsedGameTime.TotalMinutes / 2;
            rays.Update(this.game.Content, time, this.game.screenRectangle);

            parallax.Update();
        }

        public override void Draw(GameTime gameTime, SpriteBatch SpriteBatch, Map map, Camera camera)
        {
            List<IAdverseEntity> enemyList = game.gameScreen.EnemyList;
            g.SetRenderTarget(scene);
            g.Clear(ClearOptions.Target, Color.LightGoldenrodYellow, 0, 0);

            if (game.effetcs)
            {
                SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                //SpriteBatch.Draw(mouseMask, new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, mouseMask.Width, mouseMask.Height), Color.Black);
                //SpriteBatch.Draw(backgroundImage, new Rectangle(0, 0, screenRect.Width, screenRect.Height), Color.Black);

                parallax.Draw(Color.Black, this.game.screenRectangle, SpriteBatch);

                SpriteBatch.End();
                SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, camera.Matrix);
                map.Draw(SpriteBatch);
                //SpriteBatch.Draw(mask, new Rectangle(x, y, screenRect.Width, screenRect.Height), Color.Black);

                this.game.player.Draw(SpriteBatch, gameTime);
                //SpriteBatch.Draw(texture, game.player.CurrentState.Hitbox, Color.White);
                //SpriteBatch.Draw(secondTexture, game.player.CurrentState.DamageHitbox, Color.White);

                //We draw the enemies here 


                foreach (var entity in enemyList)
                {
                    entity.Draw(SpriteBatch, gameTime);
                }



                //game.enemy.DrawThrown(SpriteBatch);
                //pS.Draw(SpriteBatch);
                SpriteBatch.End();
            }
            g.SetRenderTarget(null);

            if (game.effetcs)
                ppm.Draw(gameTime, scene);

            g.SetRenderTarget(scene);

            g.Clear(Color.LightGoldenrodYellow);
            //GraphicsDevice.Clear(Color.FromNonPremultiplied(138, 157, 179, 255));

            //SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            //SpriteBatch.Draw(mouseMask, new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, mouseMask.Width, mouseMask.Height), Color.White);
            //SpriteBatch.Draw(backgroundImage, new Rectangle(0, 0, screenRect.Width, screenRect.Height), Color.White);
            //SpriteBatch.End();

            //GraphicsDevice.SetRenderTarget(null);

            //GraphicsDevice.SetRenderTarget(bG);
            //SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Opaque);
            //DoF.Draw();
            //DoF.DoF_effect.CurrentTechnique.Passes[0].Apply();
            //SpriteBatch.Draw(Content.Load<Texture2D>(bgm.backgroundTexs[0]), new Rectangle((int)bgm.bgPosBefore[0].X, (int)bgm.bgPos[0].Y, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), new Rectangle(0, 0, Content.Load<Texture2D>(bgm.backgroundTexs[0]).Width, Content.Load<Texture2D>(bgm.backgroundTexs[0]).Height), Color.White);
            //SpriteBatch.Draw(Content.Load<Texture2D>(bgm.backgroundTexs[0]), new Rectangle((int)bgm.bgPos[0].X, (int)bgm.bgPos[0].Y, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), new Rectangle(0, 0, Content.Load<Texture2D>(bgm.backgroundTexs[0]).Width, Content.Load<Texture2D>(bgm.backgroundTexs[0]).Height), Color.White);
            //SpriteBatch.End();

            //GraphicsDevice.SetRenderTarget(scene);
            //GraphicsDevice.Clear(Color.FromNonPremultiplied(138, 157, 179, 255));

            //SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            //DoF.tex = (Texture2D)bG;
            //DoF.Draw();
            //DoF.DoF_effect.CurrentTechnique.Passes[0].Apply();
            //SpriteBatch.Draw(bG, new Rectangle((int)bgm.bgPosBefore[0].X, (int)bgm.bgPos[0].Y, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), new Rectangle(0, 0, Content.Load<Texture2D>(bgm.backgroundTexs[0]).Width, Content.Load<Texture2D>(bgm.backgroundTexs[0]).Height), Color.White);
            //SpriteBatch.Draw(bG, new Rectangle((int)bgm.bgPos[0].X, (int)bgm.bgPos[0].Y, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), new Rectangle(0, 0, Content.Load<Texture2D>(bgm.backgroundTexs[0]).Width, Content.Load<Texture2D>(bgm.backgroundTexs[0]).Height), Color.White);
            //SpriteBatch.End();

            SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

            parallax.Draw(Color.White, this.game.screenRectangle, SpriteBatch);

            //SpriteBatch.Draw(bg_color, new Rectangle(0, 0, screenRect.Width, screenRect.Height), Color.White);
            SpriteBatch.End();
            if (game.stateManager.CurrentState == game.gameScreen)
            {
                SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, camera.Matrix);
                map.Draw(SpriteBatch);
                this.game.player.Draw(SpriteBatch, gameTime);
                //SpriteBatch.Draw(texture, game.player.CurrentState.Hitbox, Color.White);
                //SpriteBatch.Draw(secondTexture, game.player.CurrentState.DamageHitbox, Color.White);
                SpriteBatch.DrawString(game.titleScreen.hell, "Life : " + game.player.Life, new Vector2(-camera.Matrix.Translation.X, -camera.matrix.Translation.Y), Color.Red);
                SpriteBatch.DrawString(game.titleScreen.hell, "Fury : " + game.player.FuryCounter, new Vector2(-camera.Matrix.Translation.X, -camera.matrix.Translation.Y + 50), Color.Red);

                //We draw the enemies here 

                foreach (var entity in enemyList)
                {
                    entity.Draw(SpriteBatch, gameTime);
                }


                //pS.Draw(SpriteBatch);
                SpriteBatch.End();
            }
            g.SetRenderTarget(null);
            g.Clear(Color.Black);

            SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive);
            //pS.Draw(SpriteBatch);
            if (game.effetcs)
                SpriteBatch.Draw(ppm.scene, this.game.screenRectangle, Color.White);
            SpriteBatch.Draw(scene, this.game.screenRectangle, Color.White);
            SpriteBatch.End();

            //SpriteBatch.Begin(SpriteSortMode.Immediate, null, null, dSB1, null, alphaTest);
            //SpriteBatch.Draw(mask, new Rectangle(x, y, screenRect.Width, screenRect.Height), Color.White);
            //SpriteBatch.End();

            //SpriteBatch.Begin(SpriteSortMode.Immediate, null, null, dSB2, null, alphaTest);
            //SpriteBatch.Draw(bg_color, Vector2.Zero, Color.White);
            //SpriteBatch.End();
        }

    }
}

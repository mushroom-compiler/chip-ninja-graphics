﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using chip_ninja.Components;
using chip_ninja.Graphics.PostProcess.NormalMapping;
using chip_ninja.Enemies;
using chip_ninja.Background.FunkyCircle;
using chip_ninja.Interfaces;

namespace chip_ninja.Background
{
    public class BackgroundLevel2 : BaseBackground
    {
        RenderTarget2D Map;

        Texture2D bg_effect2, bg_NRM_effect2;
        Texture2D bg2_effect2, bg2_NRM_effect2;
        Texture2D bg_color;

        GraphicsDevice g;

        NormalMapping normalMapping;

        FunkyCircle.FunkyCircle fCircle;

        public BackgroundLevel2(Game game, GraphicsDevice Graphics)
            : base(game)
        {
            currentLevel = "Je sais plus...";

            this.g = Graphics;

            bg_effect2 = this.game.Content.Load<Texture2D>("Textures/Backgrounds/Normals/stonesi");
            bg_NRM_effect2 = this.game.Content.Load<Texture2D>("Textures/Backgrounds/Normals/stonesi_NRM");
            bg2_effect2 = this.game.Content.Load<Texture2D>("Textures/Backgrounds/Normals/xstone02steps");
            bg2_NRM_effect2 = this.game.Content.Load<Texture2D>("Textures/Backgrounds/Normals/xstone02steps_NRM");

            bg_color = this.game.Content.Load<Texture2D>(@"Textures\Backgrounds\titlescreen_color");

            Map = new RenderTarget2D(g, this.game.screenRectangle.Width, this.game.screenRectangle.Height, false, SurfaceFormat.Color, DepthFormat.None);


            normalMapping = new NormalMapping(game, this.game.SpriteBatch, this.game.Content, this.game.gameScreen.map, @"Shaders/PostProcessing/DeferredCombined", @"Shaders/PostProcessing/MultiTarget");

            normalMapping.texs.Add(bg_color);
            normalMapping.addOffset(new Vector2(0, 0));
            //Y = 0

            for (int i = 0; i < 20; i++)
            {
                normalMapping.addTex(bg_effect2, bg_NRM_effect2);
                normalMapping.addOffset(new Vector2(0, 300));
            }


            normalMapping.ExctractParameters();

            currentSong = game.Content.Load<Song>("Songs/sky");

            for (int i = 0; i < this.game.gameScreen.EnemyList.Count + 1; i++)
                normalMapping.addLight(new Light()
                {
                    IsEnabled = true,
                    Color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f),
                    Power = 1.9f,
                    Decay = 200,
                    Position = new Vector3(0, 10, 100),
                });

        }


        public override void Enter(chip_ninja game)
        {
            game.gameScreen.map = new Map("level1", game.Content, game.gameScreen.EnemyList);
            game.player = new Player(game.gameScreen.map.PlayerSpawn);
            game.player.LoadContent(game.Content);
            game.gameScreen.camera = new Camera(game.gameScreen.map.PlayerSpawn);

        }

        int total_offset = 0;
        public override void Update(GameTime gameTime, Camera camera)
        {
            if (this.game.gameScreen.EnemyList.Count != normalMapping.lights.Count - 1)
            {
                normalMapping.lights.Clear();
                for (int i = 0; i < this.game.gameScreen.EnemyList.Count + 1; i++)
                    normalMapping.addLight(new Light()
                    {
                        IsEnabled = true,
                        Color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f),
                        Power = 1.9f,
                        Decay = 200,
                        Position = new Vector3(0, 10, 100),
                    });
            }

            normalMapping.lights[0].Position = new Vector3(camera.Offset.X, camera.Offset.Y, normalMapping.lights[0].Position.Z);

            for (int i = 1; i < this.game.gameScreen.EnemyList.Count + 1; i++)
                normalMapping.lights[i].Position = new Vector3(this.game.gameScreen.EnemyList[i - 1].GetPosition().X + camera.matrix.Translation.X, this.game.gameScreen.EnemyList[i - 1].GetPosition().Y + camera.matrix.Translation.Y, normalMapping.lights[i].Position.Z);


            //Texs offset
            for (int i = 1; i < normalMapping.offsets.Count / 2; i++)
            {
                normalMapping.offsets[i] = new Vector2(total_offset + camera.Matrix.Translation.X, camera.Matrix.Translation.Y + 225);
                total_offset += normalMapping.texs[i].Width;
            }

            total_offset = 0;
            for (int i = normalMapping.offsets.Count / 2; i < normalMapping.offsets.Count; i++)
            {
                normalMapping.offsets[i] = new Vector2(total_offset + camera.Matrix.Translation.X, camera.Matrix.Translation.Y + 225 + normalMapping.texs[i].Height);
                total_offset += normalMapping.texs[i].Width;
            }
            total_offset = 0;
            //

            normalMapping.Update();
        }

        public override void Draw(GameTime gameTime, SpriteBatch SpriteBatch, Map map, Camera camera)
        {
            List<IAdverseEntity> enemyList = game.gameScreen.EnemyList;

            g.Clear(Color.Black);
            if (game.stateManager.CurrentState == game.gameScreen)
            {
                g.SetRenderTarget(Map);
                g.Clear(Color.Transparent);
            }

            //SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            //SpriteBatch.Draw(backgroundImage, new Vector2(0, 0), Color.White);
            //SpriteBatch.End();
            if (game.stateManager.CurrentState == game.gameScreen)
            {
                SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, camera.Matrix);
                //SpriteBatch.Draw(bg_effect2, new Vector2(0, 0), Color.White);
                map.Draw(SpriteBatch);


                SpriteBatch.End();
                //normalMapping.texs[normalMapping.texs.Count - 1] = (Texture2D)Map;
                normalMapping.texs[0] = (Texture2D)Map;
                g.SetRenderTarget(null);

                normalMapping.Draw(gameTime);

                SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, camera.Matrix);
                foreach (var entity in enemyList)
                    entity.Draw(SpriteBatch, gameTime);

                game.player.Draw(SpriteBatch, gameTime);
                SpriteBatch.End();
            }
        }
    }
}

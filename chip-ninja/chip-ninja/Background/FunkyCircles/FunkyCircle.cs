﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Background.FunkyCircle
{
    public class FunkyCircle
    {
        float size;
        Vector2 position;
        Texture2D tex;

        float startTime = 0.0f;
        float lifeTime;
        Vector2 origin;

        float deltaSize;
        float scale;

        Color color;

        public bool visible = true;

        public FunkyCircle(Texture2D tex, Vector2 position, float size, Color color)
        {
            this.tex = tex;
            this.position = position;
            this.scale = size;
            this.color = color;
            visible = true;

            origin = new Vector2(tex.Width / 2, tex.Height / 2);
        }

        public void Update(float deltaTime)
        {
            scale -= 0.0125f;
            if (scale <= 0)
            {
                scale = 0;
                visible = false;
            }
        }

        
        public void Draw(SpriteBatch spriteBatch)
        {
            //Console.WriteLine(size / deltaSize);
            //if (scale <= 0.87f && startTime < 4000)
                spriteBatch.Draw(tex, position, null, color, 0.0f, origin, scale, SpriteEffects.None, 0.0f);
        }
    }
}




//public void Update(float deltaTime)
//{
//Console.WriteLine(visible);

//startTime += deltaTime * 1.5f;
//scale = size / deltaSize;
//Console.WriteLine(startTime);
//if (scale < 0.06f)
//startTime += deltaTime * 20;
//else if (scale < 0.4f)
//startTime += deltaTime * 4;


//float remainingLife = startTime / lifeTime;

//if (scale < 0.05f || startTime >= 3000 || remainingLife > 0.45f)
//visible = false;
//Console.WriteLine(remainingLife);

//deltaSize = 4 * remainingLife * (1 - remainingLife);
//}

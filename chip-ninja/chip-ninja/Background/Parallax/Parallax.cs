﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Background
{
    public class Parallax
    {
        public List<string> backgroundTexsPaths = new List<string>();
        public List<Texture2D> bgTexs = new List<Texture2D>();
        public List<float> bgSpeeds = new List<float>();

        public List<Vector2> bgPos = new List<Vector2>();
        public List<Vector2> bgPosAfter = new List<Vector2>();
        public List<Vector2> bgPosAfter_Base = new List<Vector2>();

        public int bgCount;

        chip_ninja game;
        public Parallax(Game game)
        {
            this.game = (chip_ninja)game;
        }

        public void AddBackground(string bgTex, float speed, ContentManager Content)
        {
            //backgroundTexsPaths.Add(bgTex);
            bgTexs.Add(Content.Load<Texture2D>(bgTex));

            bgSpeeds.Add(speed);
            bgPos.Add(Vector2.Zero);
            bgCount++;

            bgPosAfter.Add(new Vector2(game.screenRectangle.Width, 0));
            bgPosAfter_Base.Add(new Vector2(game.screenRectangle.Width, 0));
        }

        float lastTranslation;
        public void Update()
        {
            lastTranslation = game.gameScreen.lastTranslation;

            for (int i = 0; i < bgCount; i++)
            {
                if (game.player.Velocity.X > 0 && game.player.Movement.X > 0.5f && game.gameScreen.camera.matrix.Translation.X < lastTranslation)
                {
                    bgPos[i] -= new Vector2(game.player.Velocity.X + bgSpeeds[i], 0);
                    bgPosAfter[i] -= new Vector2(game.player.Velocity.X + bgSpeeds[i], 0);

                    if (bgPos[i].X < -bgPosAfter_Base[i].X)
                        bgPos[i] = new Vector2(bgPosAfter[i].X + bgPosAfter_Base[i].X, 0);

                    if (bgPosAfter[i].X < -bgPosAfter_Base[i].X)
                        bgPosAfter[i] = new Vector2(bgPos[i].X + bgPosAfter_Base[i].X, 0);
                }
                if (game.player.Velocity.X < 0 && game.player.Movement.X < -0.5f && game.gameScreen.camera.matrix.Translation.X > lastTranslation)
                {
                    bgPos[i] += new Vector2(game.player.Velocity.X + bgSpeeds[i] * 1.5f, 0);
                    bgPosAfter[i] += new Vector2(game.player.Velocity.X + bgSpeeds[i] * 1.5f, 0);

                    if (bgPos[i].X > bgPosAfter_Base[i].X)
                        bgPos[i] = new Vector2(bgPosAfter[i].X - bgPosAfter_Base[i].X, 0);

                    if (bgPosAfter[i].X > bgPosAfter_Base[i].X)
                        bgPosAfter[i] = new Vector2(bgPos[i].X - bgPosAfter_Base[i].X, 0);
                }
            }
        }

        public void Draw(Color color, Rectangle screenRectangle, SpriteBatch SpriteBatch)
        {
            for (int bg = 0; bg < bgCount; bg++)
            {
                SpriteBatch.Draw(bgTexs[bg], new Rectangle((int)bgPos[bg].X, (int)bgPos[bg].Y, screenRectangle.Width, screenRectangle.Height), new Rectangle(0, 0, bgTexs[bg].Width, bgTexs[bg].Height), color);
                SpriteBatch.Draw(bgTexs[bg], new Rectangle((int)bgPosAfter[bg].X, (int)bgPos[bg].Y, screenRectangle.Width, screenRectangle.Height), new Rectangle(0, 0, bgTexs[bg].Width, bgTexs[bg].Height), color);
            }
        }
    }
}

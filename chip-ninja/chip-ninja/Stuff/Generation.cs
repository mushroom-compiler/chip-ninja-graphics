﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace chip_ninja.Stuff
{
    public class Generation
    {
        public List<Vector2> roomLocation = new List<Vector2>();
        public Vector2 endPosition;
        Random rand;

        public Generation()
        {
            rand = new Random();
            endPosition = new Vector2(rand.Next(20), rand.Next(20));
        }

        public bool hasBeenDiscovered(Vector2 pos)
        {
            int i = 0;
            while (i < roomLocation.Count)
            {
                if (pos == roomLocation[i])
                    return true;
                i++;
            }
            return false;
        }

        public bool findPath(Vector2 current, int minDistance)
        {
            if (current == endPosition && minDistance <= 0)
                return true;
            if (!hasBeenDiscovered(current))
                return false;
            roomLocation.Add(current);
            switch (rand.Next(3))
            {
                case 0:
                    if (findPath(new Vector2(current.X, current.Y - 1), minDistance - 1) == true)
                        return true;
                    break;
                case 1:
                    if (findPath(new Vector2(current.X + 1, current.Y), minDistance - 1) == true)
                        return true;
                    break;
                case 2:
                    if (findPath(new Vector2(current.X, current.Y + 1), minDistance - 1) == true)
                        return true;
                    break;
                case 3:
                    if (findPath(new Vector2(current.X - 1, current.Y), minDistance - 1) == true)
                        return true;
                    break;
            }
            roomLocation.Remove(current);
            return false;
        }
    }
}

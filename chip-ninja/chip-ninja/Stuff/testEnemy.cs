﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja
{
    public class Enemy : GameActor
    {
        enum EnemyState { Patroling = 1, OnAlert = 2 };

        Vector2 jumpVelocity = new Vector2(0, -2.4f);
        Vector2 runningJumpVelocity = new Vector2(0, -10f);
        Vector2 gravity = new Vector2(0, 0.15f);
        Vector2 climbingVelocity = new Vector2(0, -.5f);
        float airVelocity = 0.075f;
        bool isOnLadder;
        GameTime gameTime;
        EnemyState currentState;

        //Player movement limit on patroling state
        int limit = 600;

        public string multi_state = "normal";

        public Vector2 lastPos2;
        public int lastPos2_cte = 0;

        public Enemy(Vector2 pos)
            : base()
        {
            speed = .5f;
            isOnLadder = false;
            position = pos;
            lastPos2 = pos;
            HorizontalDirection = HorizontalDirections.Left;
            currentState = EnemyState.Patroling;
        }

        //bool wasOn_limitNear = false;
        //Vector2 distance;
        //Vector2 lastPosition;
        public void Update(GameTime gameTime, Map map, Player player)
        {
            if (velocity.X == 0 && lastPos2_cte == 0)
            {
                lastPos2 = this.position;
                velocity.Y *= 100;
                lastPos2_cte++;
            }
            if (lastPos2_cte == 1)
            {
                velocity.Y /= 10;
                lastPos2_cte++;
            }

            //lastPosition = this.position;
            if (currentState == EnemyState.Patroling)
            {
                this.position = lastPos2;
                if (Math.Abs(player.Position.X - this.position.X) >= limit && Math.Abs(player.Position.X - this.position.Y) >= limit - 100)
                    currentState = EnemyState.Patroling;
                else currentState = EnemyState.OnAlert;
            }
            if (currentState == EnemyState.OnAlert)
            {
                this.HorizontalDirection = player.HorizontalDirection;

                if (this.HorizontalDirection == HorizontalDirections.Left && player.Position.X < this.position.X)
                    this.moveLeft();
                if (this.HorizontalDirection == HorizontalDirections.Left && player.Position.X > this.position.X)
                    this.moveRight();
                if (this.HorizontalDirection == HorizontalDirections.Right && player.Position.X < this.position.X)
                    this.moveLeft();
                if (this.HorizontalDirection == HorizontalDirections.Right && player.Position.X > this.position.X)
                    this.moveRight();

                //if (player.Position.Y < this.position.Y - 100)
                //{
                //    this.reverseGravity();
                //    if (this.position.Y < player.Position.Y - 50)
                //        this.reverseGravity();
                //}
                //else if (player.Position.Y >= position.Y + 100)
                //{
                //    this.reverseGravity();
                //    if (this.position.Y >= player.Position.Y + 50)
                //        this.reverseGravity();
                //}

                if (Math.Abs(player.Position.X - this.position.X) >= limit)
                {
                    currentState = EnemyState.Patroling;
                    lastPos2 = this.position;
                    //this.jump();
                }

                lastPosToObj = this.position;
                Throw();

                //if (player.Position.Y <= this.position.Y - 100 || player.Position.Y >= this.position.Y + 100)
                //    currentState = EnemyState.Patroling;

                //if (this.position.X < player.Position.X - 100 || this.position.X >= player.Position.X + 100)
                //    currentState = EnemyState.Patroling;
            }

            lastPos = position;
            this.gameTime = gameTime;
            hitbox = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
            CheckLadder(map);
            if (HorizontalDirection == HorizontalDirections.Right)
            {
                checkRightCollison(map, gameTime);
            }
            else if (HorizontalDirection == HorizontalDirections.Left)
                checkLeftCollision(map, gameTime);
            else
                position.X += velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds;
            position.X = (position.X < 0) ? 0 : position.X;
            if (isOnLadder)
            {

            }
            else
            {
                velocity += gravity;
                DetermineDirection();
                if (VerticalDirection == VerticalDirections.Down && position.Y > 0)
                {
                    CheckDownCollision(map, gameTime);
                }
                else
                    position.Y += velocity.Y * speed * gameTime.ElapsedGameTime.Milliseconds;
            }

            /*switch (CurrentState)
            {
                case State.Jumping:
                    if (velocity.Y > 0 && position.Y > 0)
                    {
                        CanGoDown(map, gameTime);
                        position.X += velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds;
                    }
                    else
                        position += velocity * speed * gameTime.ElapsedGameTime.Milliseconds;
                    break;
                case State.Standing:
                    if (velocity.Y > 0 && position.Y > 0)
                    {
                        CanGoDown(map, gameTime);
                        position.X += velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds;
                    }
                    else
                        position += velocity * speed * gameTime.ElapsedGameTime.Milliseconds;
                    break;
                case State.Running:
                    if (velocity.Y > 0 && position.Y > 0)
                    {
                        CanGoDown(map, gameTime);
                        position.X += velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds;
                    }
                    else
                        position += velocity * speed * gameTime.ElapsedGameTime.Milliseconds;
                    break;
            }*/

            movement = position - lastPos;
        }

        float objVelocity = 0.5f;
        public Texture2D objTex;
        Vector2 lastPosToObj;
        public void Throw()
        {
            this.objVelocity *= 2;
        }
        public void DrawThrown(SpriteBatch sb)
        {
            sb.Draw(objTex, new Vector2(this.HorizontalDirection == HorizontalDirections.Left ? lastPosToObj.X - objVelocity : lastPosToObj.X + objVelocity, 500), Color.White);
        }

        public override void Update(GameTime gameTime)
        {
            throw new NotImplementedException();
        }

        public void CheckLadder(Map map)
        {
            bool change = false;
            for (int i = (int)position.Y / map.TileHeight; i < hitbox.Bottom / map.TileHeight; i++)
            {
                for (int j = (int)position.X / map.TileWidth; j < hitbox.Right / map.TileWidth + 1; j++)
                {
                    if (map.TileMap[i, j] == 3)
                    {
                        isOnLadder = true;
                        change = true;
                    }
                }
            }
            if (!change && isOnLadder)
                isOnLadder = false;
        }

        public override void jump()
        {
            if (isOnLadder)
            {
                velocity.Y = state.JumpVelocity.Y;
            }
            else if (velocity.Y >= -.5f)
            {
                velocity.Y = state.JumpVelocity.Y;
                state = (state == JumpingState) ? DoubleJumpingState : JumpingState;
            }
        }

        public override void climbLadder()
        {
            if (isOnLadder)
            {
                velocity.Y = climbingVelocity.Y;
                position.Y += velocity.Y * speed * gameTime.ElapsedGameTime.Milliseconds;
            }
        }

        public override void glideLadder()
        {
            if (isOnLadder)
            {
                velocity.Y = -climbingVelocity.Y;
                position.Y += velocity.Y * speed * gameTime.ElapsedGameTime.Milliseconds;
            }
        }

        public override void throwRandom()
        {
            Random rand = new Random();
            velocity = new Vector2((float)((rand.NextDouble() * 12) - 6), (float)((rand.NextDouble() * 12) - 6));
        }

        public override void moveRight()
        {
            if (state == JumpingState)
                airRight();
            else
                velocity.X = state.MovementVelocity;

        }

        public override void moveLeft()
        {
            if (state == JumpingState || state == DoubleJumpingState)
                airLeft();
            else
                velocity.X = -state.MovementVelocity;
        }

        public override void stopMoving()
        {
            velocity.X *= state.InertiaResistance;
        }

        /* public override void ApplyAirResistance()
         {
             velocity.X *= state.AirResistance;
         }*/

        public override void airLeft()
        {
            velocity.X = (velocity.X - airVelocity < -state.MovementVelocity) ? -state.MovementVelocity : velocity.X - airVelocity;
        }

        public override void airRight()
        {
            velocity.X = (velocity.X + airVelocity > state.MovementVelocity) ? state.MovementVelocity : velocity.X + airVelocity;
        }

        public override void reverseGravity()
        {
            gravity = new Vector2(0, -gravity.Y);
        }

        //Temporary
        public void Attack()
        {
            this.tmp = texture;
            texture = attackTex;
        }

        public void UndoAttack()
        {
            texture = this.tmp;
        }
        //End Temporary

        public override void Run()
        {
            CurrentState = RunningState;
            JumpingState.MovementVelocity = CurrentState.MovementVelocity;
            //speed = runningSpeed;
        }

        public override void Walk()
        {
            CurrentState = StandingState;
            JumpingState.MovementVelocity = CurrentState.MovementVelocity;
            //speed = walkingSpeed;
        }

        /* public override void runRight()
         {
             velocity.X = 2;
         }

         public override void runLeft()
         {
             velocity.X = -2;
         }*/

        public void DetermineDirection()
        {
            if (velocity.Y > 0)
                VerticalDirection = VerticalDirections.Down;
            else
                VerticalDirection = VerticalDirections.Up;
            if (velocity.X > 0)
                HorizontalDirection = HorizontalDirections.Right;
            else
                HorizontalDirection = HorizontalDirections.Left;
        }

        public int GetLeftObstacleLine(int pos_x, Map map, int line)
        {
            if (line < 0)
                return -1;
            int tmp = (pos_x < 0) ? 0 : pos_x;
            for (int i = tmp; i > 0; i--)
            {
                if (map.TileMap[line, i] != 0 && map.TileMap[line, i] != 2)
                    return i;
            }
            return -1;
        }

        public Vector2 GetNearestLeftObstacle(int playerHeight, Rectangle hitbox, Map map)
        {
            int lineNumber = playerHeight / map.TileHeight + 1;
            Vector2 pos = new Vector2(-1, -1);
            int tmp = -1;
            int past = -1;

            for (int i = 0; i < lineNumber; i++)
            {
                tmp = GetLeftObstacleLine((int)hitbox.Left / map.TileWidth, map, (int)hitbox.Top / map.TileHeight + i);
                if (tmp != -1 && (past < tmp || past == -1))
                {
                    past = tmp;
                    pos = new Vector2((int)hitbox.Y / map.TileHeight + i, past);
                }
            }
            return pos;
        }

        public void checkLeftCollision(Map map, GameTime gameTime)
        {
            Vector2 nearestObstacleMapPosition = GetNearestLeftObstacle(hitbox.Height, hitbox, map);
            int nearestObstacleRealPosition = (int)nearestObstacleMapPosition.Y * map.TileHeight;
            float supposedPosition = (velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds);
            if (nearestObstacleMapPosition.X != -1)
            {
                float lol = Math.Max(supposedPosition, nearestObstacleRealPosition + 20 - hitbox.Left);
                if (lol > 0)
                    velocity.X = 0;
                position.X += lol;
            }
            else
                position.X += supposedPosition;

        }

        public int GetRightObstacleLine(int pos_x, Map map, int line)
        {
            if (line < 0)
                return -1;
            int tmp = (pos_x < 0) ? 0 : pos_x;
            for (int i = tmp; i < map.Width; i++)
            {
                if (map.TileMap[line, i] != 0 && map.TileMap[line, i] != 2)
                    return i;
            }
            return -1;
        }

        public Vector2 GetNearestRightObstacle(int playerHeight, Rectangle hitbox, Map map)
        {
            int lineNumber = playerHeight / map.TileHeight + 1;
            Vector2 pos = new Vector2(-1, -1);
            int tmp = -1;
            int past = -1;

            for (int i = 0; i < lineNumber; i++)
            {
                tmp = GetRightObstacleLine(((int)hitbox.Right) / map.TileWidth, map, (int)hitbox.Top / map.TileHeight + i);
                if (tmp != -1 && (past > tmp || past == -1))
                {
                    past = tmp;
                    pos = new Vector2((int)hitbox.Y / map.TileHeight + i, past);
                }
            }
            return pos;
        }

        public void checkRightCollison(Map map, GameTime gameTime)
        {
            Vector2 nearestObstacleMapPosition = GetNearestRightObstacle(hitbox.Height, hitbox, map);
            int nearestObstacleRealPosition = (int)nearestObstacleMapPosition.Y * map.TileHeight;
            float supposedPosition = (velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds);
            if (nearestObstacleMapPosition.X != -1)
            {
                float lol = Math.Min(supposedPosition, nearestObstacleRealPosition - hitbox.Right);
                if (lol < 0)
                    velocity.X = 0;
                position.X += lol;
            }
            else
                position.X += supposedPosition;
        }

        public int GetDownObstacleColumn(int pos_y, Map map, int column)
        {
            column = column > map.Width ? map.Width : column;
            if (column > map.Width)
                return -1;
            int tmp = (pos_y < 0) ? 0 : pos_y;
            for (int i = tmp; i < map.Height; i++)
            {
                if (map.TileMap[i, column] != 0)
                    return i;
            }
            return -1;
        }

        public Vector2 GetNearestDownObstacle(int playerWidth, Rectangle hitbox, Map map)
        {
            int columnNumber = playerWidth / map.TileWidth + 1;
            Vector2 pos = new Vector2(-1, -1);
            int tmp = -1;
            int past = -1;

            for (int i = 0; i < columnNumber + 1; i++)
            {
                tmp = GetDownObstacleColumn(((int)hitbox.Bottom + 19) / map.TileHeight, map, (((int)hitbox.Right - 19) / map.TileWidth) + i);
                if (past > tmp && tmp != -1 || past == -1)
                {
                    past = tmp;
                    pos = new Vector2((int)hitbox.X / map.TileWidth + i, past);
                }
            }
            return pos;
        }

        public void CheckDownCollision(Map map, GameTime gameTime)
        {
            Vector2 nearestObstacleMapPosition = GetNearestDownObstacle(hitbox.Width, hitbox, map);
            int nearestObstacleYPosition = (int)nearestObstacleMapPosition.Y * map.TileHeight;
            float supposedPosition = (velocity.Y * speed * gameTime.ElapsedGameTime.Milliseconds);
            float lol = Math.Min(supposedPosition, nearestObstacleYPosition - hitbox.Bottom);
            if (nearestObstacleMapPosition.Y != -1)
            {
                if (lol == supposedPosition && state != DoubleJumpingState)
                {
                    state = JumpingState;
                }
                if (lol == 0)
                {
                    velocity.Y = 0;
                    state = (state == RunningState) ? RunningState : StandingState;
                    JumpingState.MovementVelocity = CurrentState.MovementVelocity;
                }
                /* if (lol < 0)
                 {
                     position.Y += lol;
                     lol = 0;
                 }*/

                position.Y += lol * speed;
            }
            else
                position.Y += supposedPosition;
        }



    }
}

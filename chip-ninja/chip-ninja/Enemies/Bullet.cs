﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using chip_ninja.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Enemies
{
    class Bullet : GameActor, IAdverseEntity
    {
        bool alive;
        int timeSpan; //in milliseconds
        int damage;
        int owner; //0 for ennemy, 1 pour player

        public Bullet(Vector2 position, Vector2 velocity)
        {

            damage = 10;
            speed = .4f;
            this.position = position;
            this.velocity = velocity;
            alive = true;
            timeSpan = 1000000;
            owner = 0;
        }

        public Vector2 GetPosition()
        {
            return position;
        }

        public int GetDataCode()
        {
            return 33;
        }

        public bool IsAlly()
        {
            return (owner == 1);
        }

        public void LoadContent(ContentManager Content)
        {
            LoadContent(Content, "Bullet", 1, 1);
        }

        public void Update(GameTime gameTime, Map map, Player player, List<IAdverseEntity> enemyList)
        {
            lastPos = position;
            hitbox = new Rectangle((int)position.X, (int)Position.Y, animation.FrameSize.X, animation.FrameSize.Y);

            //Check For Collisions

            if (velocity.X > 0)
            {
                checkRightCollison(map, gameTime, player, enemyList);
            }
            else if (velocity.X < 0)
            {
                checkLeftCollision(map, gameTime, player, enemyList);
            }

            position.X += velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds;

            if (position.X < 0)
                alive = false;

            if (owner == 0 && hitbox.Intersects(player.Hitbox))
            {
                alive = false;
                player.GetDamage(damage);
            }
            if (owner == 1)
            {
                List<IAdverseEntity> newSequence = enemyList.ConvertAll(new Converter<IAdverseEntity, IAdverseEntity>(InflictDamage));
            }

            if (timeSpan <= 0)
            {
                alive = false;
            }

            animation.Update(gameTime, velocity);
        }

        public IAdverseEntity InflictDamage(IAdverseEntity entity)
        {
            if (entity != this && hitbox.Intersects(entity.GetHitbox()))
            {
                this.alive = false;
                entity.OnDamage();
            }
            return entity;
        }

        public void OnDamage()
        {
            if (owner != 1)
            {
                velocity.X = - velocity.X + Math.Sign(- velocity.X) * 0.5f;

                owner = 1;
            }
        }

        public bool IsAlive()
        {
            return alive;
        }

        public Rectangle GetHitbox()
        {
            return hitbox;
        }


        public override void Update(GameTime gameTime)
        {
            throw new NotImplementedException();
        }

        public int GetLeftObstacleLine(int pos_x, Map map, int line)
        {
            if (line < 0)
                return -1;
            int tmp = (pos_x < 0) ? 0 : pos_x;
            for (int i = tmp; i > 0; i--)
            {
                if (map.TileMap[line, i] != 0 && map.TileMap[line, i] != 2)
                    return i;
            }
            return -1;
        }

        public Vector2 GetNearestLeftObstacle(int playerHeight, Rectangle hitbox, Map map)
        {
            int lineNumber = playerHeight / map.TileHeight + 1;
            Vector2 pos = new Vector2(-1, -1);
            int tmp = -1;
            int past = -1;

            for (int i = 0; i < lineNumber; i++)
            {
                tmp = GetLeftObstacleLine((int)hitbox.Left / map.TileWidth, map, (int)hitbox.Top / map.TileHeight + i);
                if (tmp != -1 && (past < tmp || past == -1))
                {
                    past = tmp;
                    pos = new Vector2((int)hitbox.Y / map.TileHeight + i, past);
                }
            }
            return pos;
        }

        public void checkLeftCollision(Map map, GameTime gameTime, Player player, List<IAdverseEntity> enemyList)
        {
            Vector2 nearestObstacleMapPosition = GetNearestLeftObstacle(hitbox.Height, hitbox, map);
            int nearestObstacleRealPosition = (int)nearestObstacleMapPosition.Y * map.TileHeight;
            float supposedPosition = (velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds);
            if (nearestObstacleMapPosition.X != -1)
            {
                IAdverseEntity closest = new Met(new Vector2(-1, -1));

                float positionObstacle_map = Math.Max(supposedPosition, nearestObstacleRealPosition + 20 - hitbox.Left);
                float positionEntity;

                if (owner == 0)
                    positionEntity = Math.Max(supposedPosition, hitbox.Left - player.Hitbox.Right);
                else
                {
                    foreach (var entity in enemyList)
                    {
                        if (entity.GetHitbox().Right > closest.GetHitbox().Right && entity.GetHitbox().Right < hitbox.Left)
                            closest = entity;
                    }
                    positionEntity = Math.Min(supposedPosition, hitbox.Left - closest.GetHitbox().Right);
                }

                float closestHurdle = Math.Max(positionObstacle_map, positionEntity);

                if (closestHurdle != supposedPosition)
                {
                    velocity.X = 0;
                    position.X += positionObstacle_map;
                    alive = false;

                    if (closestHurdle == hitbox.Left - player.Hitbox.Right)
                    {
                        player.GetDamage(damage);
                    }
                    else if (closestHurdle == hitbox.Left - closest.GetHitbox().Right)
                    {
                        closest.OnDamage();
                    }
                }
            }

        }

        public int GetRightObstacleLine(int pos_x, Map map, int line)
        {
            if (line < 0)
                return -1;
            int tmp = (pos_x < 0) ? 0 : pos_x;
            for (int i = tmp; i < map.Width; i++)
            {
                if (map.TileMap[line, i] != 0 && map.TileMap[line, i] != 2)
                    return i;
            }
            return -1;
        }

        public Vector2 GetNearestRightObstacle(int playerHeight, Rectangle hitbox, Map map)
        {
            int lineNumber = playerHeight / map.TileHeight + 1;
            Vector2 pos = new Vector2(-1, -1);
            int tmp = -1;
            int past = -1;

            for (int i = 0; i < lineNumber; i++)
            {
                tmp = GetRightObstacleLine(((int)hitbox.Right) / map.TileWidth, map, (int)hitbox.Top / map.TileHeight + i);
                if (tmp != -1 && (past > tmp || past == -1))
                {
                    past = tmp;
                    pos = new Vector2((int)hitbox.Y / map.TileHeight + i, past);
                }
            }
            return pos;
        }

        public void checkRightCollison(Map map, GameTime gameTime, Player player, List<IAdverseEntity> enemyList)
        {
            Vector2 nearestObstacleMapPosition = GetNearestRightObstacle(hitbox.Height, hitbox, map);
            int nearestObstacleRealPosition = (int)nearestObstacleMapPosition.Y * map.TileHeight;
            float supposedPosition = (velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds);
            if (nearestObstacleMapPosition.X != -1)
            {
                IAdverseEntity closest = new Met(new Vector2(-1, -1));

                float positionObstacle_map = Math.Min(supposedPosition, nearestObstacleRealPosition - hitbox.Right);

                float positionEntity;

                if (owner == 0)
                    positionEntity = Math.Min(supposedPosition, player.Hitbox.Left - hitbox.Right);
                else
                {
                    foreach (var entity in enemyList)
                    {
                        if (entity.GetHitbox().Left > closest.GetHitbox().Left && entity.GetHitbox().Left < hitbox.Right)
                            closest = entity;
                    }
                    positionEntity = Math.Min(supposedPosition, closest.GetHitbox().Left - hitbox.Right);
                }

                float closestHurdle = Math.Min(positionObstacle_map, positionEntity);

                if (closestHurdle != supposedPosition)
                {
                    velocity.X = 0;
                    position.X += closestHurdle;
                    alive = false;
                    if (closestHurdle == player.Hitbox.Left - hitbox.Right)
                    {
                        player.GetDamage(damage);
                    }
                    else if (closestHurdle == closest.GetHitbox().Left - hitbox.Right)
                    {
                        closest.OnDamage();
                    }
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using chip_ninja.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Enemies
{
    public class Met : GameActor, IAdverseEntity
    {
        Vector2 gravity;
        bool isOnGround;
        bool alive;

        public Met(Vector2 position)
        {
            gravity = new Vector2(0, 0.15f);
            speed = .3f;
            this.position = position;
            alive = true;
        }

        public int GetDataCode()
        {
            return 32;
        }

        public bool IsAlly()
        {
            return false;
        }

        public void LoadContent(ContentManager Content)
        {
            LoadContent(Content, "met", 1, 1);
        }

        public Vector2 GetPosition()
        {
            return position;
        }

        public void Update(GameTime gameTime, Map map, Player player, List<IAdverseEntity> enemyList)
        {
            lastPos = position;
            hitbox = new Rectangle((int)position.X, (int)Position.Y, animation.FrameSize.X, animation.FrameSize.Y);

            //IA(player, map, gameTime);

            //IA part

            //Decide where to go based on player location

            if (Math.Abs(position.X - player.Position.X) < 3000)
            {
                if (Math.Abs(position.X - player.Position.X) < 5)
                {
                    velocity.X = 0;
                }
                else if (player.Position.X > position.X)
                {
                    velocity.X = 1f;
                }
                else if (player.Position.X < position.X)
                {
                    velocity.X = -1f;
                }
                else
                {
                    velocity.X = 0;
                }
            }

            //Check if there is any pitfall

            if (isOnGround)
            {
                if (velocity.X < 0)
                {
                    CheckNearestPitLeft(map, gameTime);
                }

                if (velocity.X > 0)
                {
                    CheckNearestPitRight(map, gameTime);
                }
            }

            //Check For Collisions

            if (velocity.X > 0)
            {
                checkRightCollison(map, gameTime);
            }
            else if (velocity.X < 0)
            {
                checkLeftCollision(map, gameTime);
            }

            position.X += velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds;

            position.X = (position.X < 0) ? 0 : position.X;

            velocity += gravity;

            if (velocity.Y > 0 && position.Y > 0)
            {
                CheckDownCollision(map, gameTime);
            }
            else
                position.Y += velocity.Y * speed * gameTime.ElapsedGameTime.Milliseconds;

            if (hitbox.Intersects(player.Hitbox))
            {
                player.GetDamage(20);
            }

            animation.Update(gameTime, velocity);
        }



        public void OnDamage()
        {
            alive = false;
        }

        public bool IsAlive()
        {
            return alive;
        }

        public Rectangle GetHitbox()
        {
            return hitbox;
        }



        public void CheckNearestPitLeft(Map map, GameTime gameTime)
        {
            int nearestPit_tile = GetNearestLeftPitLine(hitbox.Left / map.TileWidth, map, hitbox.Bottom / map.TileHeight);
            int nearestPit_abslolute = nearestPit_tile * map.TileWidth + 20;
            float supposedPosition = (velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds);
            if (nearestPit_tile != -1)
            {
                float minimumShifting = Math.Min(-supposedPosition, hitbox.Left - nearestPit_abslolute);

                if (-supposedPosition > hitbox.Left - nearestPit_abslolute)
                {
                    velocity.X = 0;
                    position.X -= minimumShifting;
                }
            }
        }


        public void CheckNearestPitRight(Map map, GameTime gameTime)
        {
            int nearestPit_tile = GetNearestRightPitLine(hitbox.Right / map.TileWidth, map, hitbox.Bottom / map.TileHeight);
            int nearestPit_abslolute = nearestPit_tile * map.TileWidth;
            float supposedPosition = (velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds);
            if (nearestPit_tile != -1)
            {
                float minimumShifting = Math.Min(supposedPosition, nearestPit_abslolute - hitbox.Right);

                if (minimumShifting != supposedPosition)
                {
                    velocity.X = 0;
                    position.X += minimumShifting;
                }
            }
        }









        public int GetNearestLeftPitLine(int pos_x, Map map, int line)
        {
            if (line < 0)
                return -1;
            int tmp = (pos_x < 0) ? 0 : pos_x;
            for (int i = tmp; i > 0; i--)
            {
                if (map.TileMap[line, i] == 0)
                    return i;
            }
            return -1;
        }

        public int GetNearestRightPitLine(int pos_x, Map map, int line)
        {
            if (line < 0)
                return -1;
            int tmp = (pos_x < 0) ? 0 : pos_x;
            for (int i = tmp; i < map.Width; i++)
            {
                if (map.TileMap[line, i] == 0)
                    return i;
            }
            return -1;
        }



        public override void Update(GameTime gameTime)
        {
            throw new NotImplementedException();
        }

        public int GetLeftObstacleLine(int pos_x, Map map, int line)
        {
            if (line < 0)
                return -1;
            int tmp = (pos_x < 0) ? 0 : pos_x;
            for (int i = tmp; i > 0; i--)
            {
                if (map.TileMap[line, i] != 0 && map.TileMap[line, i] != 2)
                    return i;
            }
            return -1;
        }

        public Vector2 GetNearestLeftObstacle(int playerHeight, Rectangle hitbox, Map map)
        {
            int lineNumber = playerHeight / map.TileHeight + 1;
            Vector2 pos = new Vector2(-1, -1);
            int tmp = -1;
            int past = -1;

            for (int i = 0; i < lineNumber; i++)
            {
                tmp = GetLeftObstacleLine((int)hitbox.Left / map.TileWidth, map, (int)hitbox.Top / map.TileHeight + i);
                if (tmp != -1 && (past < tmp || past == -1))
                {
                    past = tmp;
                    pos = new Vector2((int)hitbox.Y / map.TileHeight + i, past);
                }
            }
            return pos;
        }

        public void checkLeftCollision(Map map, GameTime gameTime)
        {
            Vector2 nearestObstacleMapPosition = GetNearestLeftObstacle(hitbox.Height, hitbox, map);
            int nearestObstacleRealPosition = (int)nearestObstacleMapPosition.Y * map.TileHeight;
            float supposedPosition = (velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds);
            if (nearestObstacleMapPosition.X != -1)
            {
                float lol = Math.Max(supposedPosition, nearestObstacleRealPosition + 20 - hitbox.Left);
                if (lol != supposedPosition)
                {
                    velocity.X = 0;
                    position.X += lol;
                }
            }
            //else
            //position.X += supposedPosition;

        }

        public int GetRightObstacleLine(int pos_x, Map map, int line)
        {
            if (line < 0)
                return -1;
            int tmp = (pos_x < 0) ? 0 : pos_x;
            for (int i = tmp; i < map.Width; i++)
            {
                if (map.TileMap[line, i] != 0 && map.TileMap[line, i] != 2)
                    return i;
            }
            return -1;
        }

        public Vector2 GetNearestRightObstacle(int playerHeight, Rectangle hitbox, Map map)
        {
            int lineNumber = playerHeight / map.TileHeight + 1;
            Vector2 pos = new Vector2(-1, -1);
            int tmp = -1;
            int past = -1;

            for (int i = 0; i < lineNumber; i++)
            {
                tmp = GetRightObstacleLine(((int)hitbox.Right) / map.TileWidth, map, (int)hitbox.Top / map.TileHeight + i);
                if (tmp != -1 && (past > tmp || past == -1))
                {
                    past = tmp;
                    pos = new Vector2((int)hitbox.Y / map.TileHeight + i, past);
                }
            }
            return pos;
        }

        public void checkRightCollison(Map map, GameTime gameTime)
        {
            Vector2 nearestObstacleMapPosition = GetNearestRightObstacle(hitbox.Height, hitbox, map);
            int nearestObstacleRealPosition = (int)nearestObstacleMapPosition.Y * map.TileHeight;
            float supposedPosition = (velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds);
            if (nearestObstacleMapPosition.X != -1)
            {
                float lol = Math.Min(supposedPosition, nearestObstacleRealPosition - hitbox.Right);
                if (lol != supposedPosition)
                {
                    velocity.X = 0;
                    position.X += lol;
                }
            }
            //else
            //position.X += supposedPosition;
        }

        public int GetDownObstacleColumn(int pos_y, Map map, int column)
        {
            if (column > map.Width)
                return -1;
            int tmp = (pos_y < 0) ? 0 : pos_y;
            for (int i = tmp; i < map.Height; i++)
            {
                if (map.TileMap[i, column] != 0)
                    return i;
            }
            return -1;
        }

        public Vector2 GetNearestDownObstacle(int playerWidth, Rectangle hitbox, Map map)
        {
            int columnNumber = playerWidth / map.TileWidth + 1;
            Vector2 pos = new Vector2(-1, -1);
            int tmp = -1;
            int past = -1;

            for (int i = 0; i < columnNumber + 1; i++)
            {
                tmp = GetDownObstacleColumn(((int)hitbox.Bottom + 19) / map.TileHeight, map, (((int)hitbox.Right - 19) / map.TileWidth) + i);
                if (past > tmp && tmp != -1 || past == -1)
                {
                    past = tmp;
                    pos = new Vector2((int)hitbox.X / map.TileWidth + i, past);
                }
            }
            return pos;
        }

        public void CheckDownCollision(Map map, GameTime gameTime)
        {
            Vector2 nearestObstacleMapPosition = GetNearestDownObstacle(hitbox.Width, hitbox, map);
            int nearestObstacleYPosition = (int)nearestObstacleMapPosition.Y * map.TileHeight;
            float supposedPosition = (velocity.Y * speed * gameTime.ElapsedGameTime.Milliseconds);
            float lol = Math.Min(supposedPosition, nearestObstacleYPosition - hitbox.Bottom);
            if (nearestObstacleMapPosition.Y != -1)
            {
                if (lol == supposedPosition && state != DoubleJumpingState)
                {
                    isOnGround = false;
                }
                if (lol == 0)
                {
                    velocity.Y = 0;
                    isOnGround = true;
                }
                /* if (lol < 0)
                 {
                     position.Y += lol;
                     lol = 0;
                 }*/

                position.Y += lol * speed;
            }
            else
                position.Y += supposedPosition;
        }


    }
}

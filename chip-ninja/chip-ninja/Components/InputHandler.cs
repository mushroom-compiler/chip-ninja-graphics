﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using chip_ninja.Commands;

namespace chip_ninja.Components
{
    public class InputHandler : GameComponent
    {
        chip_ninja game;
        KeyboardState keyboardState;
        KeyboardState lastKeyboardState;
        GamePadState gamePadState;
        GamePadState lastGamePadState;
        //
        Keys[] keys;
        bool[] anykeyUp;
        string[] SC_US = { ")", "!", "@", "#", "$", "%", "^", "&", "*", "(" };
        string[] SC_FR = { "&", "é", "\"", "'", "(", "-", "è", "_", "ç", "à" };
        public string endText = "";
        //
        public InputHandler(chip_ninja game)
            : base(game)
        {
            this.game = game;
            keyboardState = Keyboard.GetState();
            gamePadState = GamePad.GetState(PlayerIndex.One);

            //
            keys = new Keys[160];
            Keys[] tempkeys;
            tempkeys = Enum.GetValues(typeof(Keys)).Cast<Keys>().ToArray<Keys>();
            int j = 0;

            for (int i = 0; i < tempkeys.Length; i++)
            {
                if (i == 1 || i == 11 || (i > 26 && i < 63) || i >= 63)
                {
                    keys[j] = tempkeys[i];
                    j++;
                }
            }

            anykeyUp = new bool[keys.Length];

            for (int i = 0; i < keys.Length; i++)
                anykeyUp[i] = true;
            //
        }

        public void Update(GameTime gameTime, GameActor gameActor)
        {
            lastKeyboardState = keyboardState;
            keyboardState = Keyboard.GetState();
            lastGamePadState = gamePadState;
            gamePadState = GamePad.GetState(PlayerIndex.One);
            if (game.stateManager.CurrentState == game.titleScreen)
                handleStartInput();
            else if (game.stateManager.CurrentState == game.gameScreen && keyboardState.IsKeyDown(Keys.F9) && lastKeyboardState.IsKeyUp(Keys.F9))
            {
                game.enemyCuttingScreen = new GameScreens.EnemyCuttingScreen(this.game, this.game.stateManager, game.gameScreen.EnemyList[0].GetTexture());
                //game.enemyCuttingScreen.enemyToBeCut = game.gameScreen.EnemyList[0].GetTexture();
                game.stateManager.ChangeState(game.enemyCuttingScreen);
                game.IsMouseVisible = true;
            }
            else if (game.stateManager.CurrentState == game.enemyCuttingScreen && keyboardState.IsKeyDown(Keys.F9) && lastKeyboardState.IsKeyUp(Keys.F9))
            {
                game.stateManager.ChangeState(game.gameScreen);
                game.IsMouseVisible = false;
            }
            else if (game.stateManager.CurrentState == game.multiplayerTitleScreen)
            {
                handleMultiplayerTitleInput();
                handleTextInput();
            }
            else if (game.stateManager.CurrentState == game.optionsScreen)
            {
                handleOptionsInput();
            }
            else if (game.stateManager.CurrentState == game.pauseScreen)
                handlePauseInput();

            else if (game.stateManager.CurrentState == game.gameScreen || game.stateManager.CurrentState == game.multiplayerScreen)
                handleGameInput(ref gameActor);
            else if (game.stateManager.CurrentState == game.gameOverScreen)
                handleGameOverInput();

        }

        public void handleMultiplayerTitleInput()
        {
            if ((keyboardState.IsKeyDown(Keys.Escape) && lastKeyboardState.IsKeyUp(Keys.Escape)) || (gamePadState.Buttons.Back == ButtonState.Pressed && lastGamePadState.Buttons.Back == ButtonState.Pressed) && game.stateManager.CurrentState == game.multiplayerTitleScreen)
                game.stateManager.ChangeState(game.titleScreen);

            //if (keyboardState.IsKeyDown(Keys.Enter))
            //    game.stateManager.ChangeState(game.gameScreen);
            //if (keyboardState.IsKeyDown(Keys.M))
            //    game.stateManager.ChangeState(game.multiplayerScreen);
            if (keyboardState.IsKeyDown(Keys.Down) && lastKeyboardState.IsKeyUp(Keys.Down) && game.multiplayerTitleScreen.cursor.index < game.multiplayerTitleScreen.buttons.Count - 1)
                game.multiplayerTitleScreen.cursor.index++;
            if (keyboardState.IsKeyDown(Keys.Up) && lastKeyboardState.IsKeyUp(Keys.Up) && game.multiplayerTitleScreen.cursor.index > 0)
                game.multiplayerTitleScreen.cursor.index--;
            if (keyboardState.IsKeyUp(Keys.Enter) && lastKeyboardState.IsKeyDown(Keys.Enter))
                game.stateManager.ChangeState(game.multiplayerTitleScreen.buttons[game.multiplayerTitleScreen.cursor.index].screen);
        }

        public void handlePauseInput()
        {
            if (keyboardState.IsKeyDown(Keys.Down) && lastKeyboardState.IsKeyUp(Keys.Down) || (gamePadState.Buttons.Back == ButtonState.Pressed && lastGamePadState.Buttons.Back == ButtonState.Pressed) && game.pauseScreen.cursor.index < game.pauseScreen.buttons.Count - 1)
                game.pauseScreen.cursor.index++;
            if (keyboardState.IsKeyDown(Keys.Up) && lastKeyboardState.IsKeyUp(Keys.Up) && game.pauseScreen.cursor.index > 0)
                game.pauseScreen.cursor.index--;

            if (keyboardState.IsKeyDown(Keys.Enter) && lastKeyboardState.IsKeyUp(Keys.Enter) && game.pauseScreen.cursor.index == 0)
                game.stateManager.PopState();
        }

        public void handleGameOverInput()
        {

            if (keyboardState.IsKeyDown(Keys.Enter) && lastKeyboardState.IsKeyUp(Keys.Enter))
            {
                game.gameScreen = new GameScreens.GameScreen(game, game.stateManager);
                game.stateManager.ChangeState(game.gameScreen);
            }
        }

        public void handleOptionsInput()
        {
            if (keyboardState.IsKeyDown(Keys.Escape) && lastKeyboardState.IsKeyUp(Keys.Escape) && game.stateManager.CurrentState == game.optionsScreen)
                game.stateManager.ChangeState(game.titleScreen);

            if (keyboardState.IsKeyDown(Keys.Down) && lastKeyboardState.IsKeyUp(Keys.Down) && game.optionsScreen.cursor.index < game.optionsScreen.buttons.Count - 1)
                game.optionsScreen.cursor.index++;
            if (keyboardState.IsKeyDown(Keys.Up) && lastKeyboardState.IsKeyUp(Keys.Up) && game.optionsScreen.cursor.index > 0)
                game.optionsScreen.cursor.index--;
            if (keyboardState.IsKeyUp(Keys.Right) && lastKeyboardState.IsKeyDown(Keys.Right))
                game.optionsScreen.buttons[game.optionsScreen.cursor.index].index++;
            if (keyboardState.IsKeyUp(Keys.Left) && lastKeyboardState.IsKeyDown(Keys.Left))
                game.optionsScreen.buttons[game.optionsScreen.cursor.index].index--;
            if (game.optionsScreen.buttons[2].text == "Music : Off" || game.optionsScreen.buttons[2].text == "Musique : Off")
                Microsoft.Xna.Framework.Media.MediaPlayer.IsMuted = true;
            if (game.optionsScreen.buttons[2].text == "Music : On" || game.optionsScreen.buttons[2].text == "Musique : On")
                Microsoft.Xna.Framework.Media.MediaPlayer.IsMuted = false;
        }

        public void handleStartInput()
        {
            if (keyboardState.IsKeyDown(Keys.Escape) && lastKeyboardState.IsKeyUp(Keys.Escape) && game.stateManager.CurrentState == game.titleScreen)
                this.game.Exit();

            //if (keyboardState.IsKeyDown(Keys.Enter))
            //    game.stateManager.ChangeState(game.gameScreen);
            //if (keyboardState.IsKeyDown(Keys.M))
            //    game.stateManager.ChangeState(game.multiplayerScreen);
            if (keyboardState.IsKeyDown(Keys.Down) && lastKeyboardState.IsKeyUp(Keys.Down) && game.titleScreen.cursor.index < game.titleScreen.buttons.Count - 1)
                game.titleScreen.cursor.index++;
            if (keyboardState.IsKeyDown(Keys.Up) && lastKeyboardState.IsKeyUp(Keys.Up) && game.titleScreen.cursor.index > 0)
                game.titleScreen.cursor.index--;
            if (keyboardState.IsKeyUp(Keys.Enter) && lastKeyboardState.IsKeyDown(Keys.Enter))
                game.stateManager.ChangeState(game.titleScreen.buttons[game.titleScreen.cursor.index].screen);
        }

        public void handleTextInput()
        {
            KeyboardState keyboardstate = Keyboard.GetState();
            int i = 0;

            foreach (Keys key in keys)
            {
                if (keyboardstate.IsKeyDown(key))
                {
                    if (anykeyUp[i])
                    {
                        if (key == Keys.Back && endText != "")
                            endText = endText.Remove(endText.Length - 1);
                        if (key == Keys.Space)
                            endText += " ";
                        if (key == Keys.OemPeriod)
                            endText += ".";
                        //if (key == Keys.Decimal)
                        //endText += ".";
                        if (i > 1 && i < 12)
                        {
                            if (keyboardstate.IsKeyDown(Keys.RightShift) || keyboardstate.IsKeyDown(Keys.LeftShift))
                                endText += key.ToString()[1];
                            else endText += SC_US[i - 2];
                        }
                        if (i > 11 && i < 38)
                        {
                            if (keyboardstate.IsKeyDown(Keys.RightShift) || keyboardstate.IsKeyDown(Keys.LeftShift))
                                endText += key.ToString();
                            else endText += key.ToString().ToLower();
                        }
                        if (i == 40)
                            endText += ".";
                    }

                    anykeyUp[i] = false;
                }

                else if (keyboardstate.IsKeyUp(key)) anykeyUp[i] = true;
                i++;
            }
        }

        public void handleGameInput(ref GameActor actor)
        {
            if (keyboardState.IsKeyDown(Keys.Escape))
                this.game.Exit();
            if (keyboardState.IsKeyDown(Keys.F4))
                game.stateManager.ChangeState(game.titleScreen);
            if (keyboardState.IsKeyDown(Keys.M) && lastKeyboardState.IsKeyUp(Keys.M))
                actor.reverseGravity();
            if (keyboardState.IsKeyDown(Keys.I) && lastKeyboardState.IsKeyUp(Keys.E))
                actor.throwRandom();
            if (keyboardState.IsKeyDown(Keys.D) && lastKeyboardState.IsKeyUp(Keys.D))
                Console.Write("");

            /*switch (actor.CurrentState)
            {
                case GameActor.State.Standing:
                    if (keyboardState.IsKeyDown(Keys.Space))
                        actor.jump();
                    if (keyboardState.IsKeyDown(Keys.Right))
                        actor.moveRight();
                    if (keyboardState.IsKeyDown(Keys.Left))
                        actor.moveLeft();
                    if (keyboardState.IsKeyUp(Keys.Left) && keyboardState.IsKeyUp(Keys.Right))
                        actor.stopMoving();
                    if (keyboardState.IsKeyDown(Keys.LeftShift))
                        actor.Run();
                    //if (keyboardState.IsKeyUp(Keys.LeftShift))
                        //actor.Walk();
                    break;
                case GameActor.State.Jumping:
                   if (keyboardState.IsKeyDown(Keys.Right))
                        actor.airRight();
                    if (keyboardState.IsKeyDown(Keys.Left))
                        actor.airLeft();
                    if (keyboardState.IsKeyUp(Keys.Left) && keyboardState.IsKeyUp(Keys.Right))
                        actor.ApplyAirResistance();
                    break;
                case GameActor.State.Running:
                    if (keyboardState.IsKeyDown(Keys.Right))
                        actor.runRight();
                    if (keyboardState.IsKeyDown(Keys.Left))
                        actor.runLeft();
                    if (keyboardState.IsKeyDown(Keys.Space))
                        actor.jump();
                    if (keyboardState.IsKeyUp(Keys.LeftShift))
                        actor.Walk();
                    break;
                default:
                    break;
            }*/

            //if (game.stateManager.CurrentState.ToString() == "chip_ninja.GameScreens.MultiplayerScreen" && keyboardState.IsKeyDown(Keys.LeftControl))
            //    game.player1.multi_state = "attack";
            if (keyboardState.IsKeyDown(Keys.NumPad5) && lastKeyboardState.IsKeyUp(Keys.NumPad5))
            {
                game.stateManager.PushState(game.pauseScreen);
            }
            if (game.stateManager.CurrentState == game.gameScreen && keyboardState.IsKeyDown(Keys.NumPad1) && lastKeyboardState.IsKeyUp(Keys.NumPad1))
                game.gameScreen.bgm.ChangeBackGround(game.gameScreen.bgLevel1);
            else if (game.stateManager.CurrentState == game.gameScreen && keyboardState.IsKeyDown(Keys.NumPad2) && lastKeyboardState.IsKeyUp(Keys.NumPad2))
                game.gameScreen.bgm.ChangeBackGround(game.gameScreen.bgLevel2);
            else if (game.stateManager.CurrentState == game.gameScreen && keyboardState.IsKeyDown(Keys.NumPad3) && lastKeyboardState.IsKeyUp(Keys.NumPad3))
                game.gameScreen.bgm.ChangeBackGround(game.gameScreen.bgLevel3);

            if (keyboardState.IsKeyDown(Keys.Right))
            {
                actor.moveRight();
            }
            if (keyboardState.IsKeyDown(Keys.Left))
            {
                actor.moveLeft();
                //game.player.animation.play = true;
                //game.player.animation.right = false;
            }
            if (keyboardState.IsKeyUp(Keys.Left) && keyboardState.IsKeyUp(Keys.Right))
                actor.stopMoving();
            if (keyboardState.IsKeyDown(Keys.LeftControl) && lastKeyboardState.IsKeyUp(Keys.LeftControl))
                actor.dash(game);
            if (keyboardState.IsKeyDown(Keys.J) && lastKeyboardState.IsKeyUp(Keys.J))
                game.gameScreen.EnemyList.Add(new Enemies.SniperMet(new Vector2(-game.gameScreen.camera.matrix.Translation.X + game.gameScreen.map.Width / 2, -game.gameScreen.camera.matrix.Translation.Y + game.gameScreen.map.Height / 2)));
            if (keyboardState.IsKeyDown(Keys.Space) && lastKeyboardState.IsKeyUp(Keys.Space))
            {
                if (actor.CurrentState != actor.DoubleJumpingState)
                    actor.jump();
            }

            if (keyboardState.IsKeyDown(Keys.LeftShift))
            {
                if (actor.CurrentState == actor.StandingState || actor.CurrentState == actor.WalkingState)
                    actor.Run();
            }
            if (keyboardState.IsKeyUp(Keys.LeftShift))
            {
                if (actor.CurrentState == actor.RunningState)
                    actor.Walk();
            }
            if (keyboardState.IsKeyDown(Keys.E) && lastKeyboardState.IsKeyUp(Keys.E))
            {
                    actor.Attack();
            }


            /*if (actor.CurrentState == actor.StandingState || actor.CurrentState == actor.WalkingState)
             {
                 if (keyboardState.IsKeyDown(Keys.Space))
                     actor.jump();
                 if (keyboardState.IsKeyDown(Keys.LeftShift))
                     actor.Run();
             }
             else if (actor.CurrentState == actor.RunningState)
             {
                 if (keyboardState.IsKeyUp(Keys.LeftShift))
                     actor.Walk();
                 if (keyboardState.IsKeyDown(Keys.Space))
                     actor.jump();
             }
             else if (actor.CurrentState == actor.JumpingState)
             {
                 if (keyboardState.IsKeyDown(Keys.Space) && lastKeyboardState.IsKeyUp(Keys.Space))
                     actor.jump();
             }*/
        }

        Command space;
    }
}

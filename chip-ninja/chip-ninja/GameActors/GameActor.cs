﻿using chip_ninja.States;
using Microsoft.Xna.Framework;

namespace chip_ninja
{
    abstract public class GameActor : DrawableEntity
    {
        public float speed;
        protected Vector2 velocity;
        protected Rectangle hitbox;

        public Rectangle Hitbox
        {
            get { return hitbox; }
        }

        protected Vector2 movement;
        protected Vector2 lastPos;
        private float airVelocity;
        private HorizontalDirections horizontalDirection;

        public HorizontalDirections HorizontalDirection
        {
            get { return horizontalDirection; }
            set { horizontalDirection = value; }
        }
        private VerticalDirections verticalDirection;

        public VerticalDirections VerticalDirection
        {
            get { return verticalDirection; }
            set { verticalDirection = value; }
        }

        public PlayerState JumpingState;
        public PlayerState RunningState;
        public PlayerState StandingState;
        public PlayerState DoubleJumpingState;
        public PlayerState WalkingState;
        public PlayerState AttackState;

        public enum VerticalDirections
        {
            Up,
            Down,
        };

        public enum HorizontalDirections
        {
            Right,
            Left
        };

        protected float AirVelocity
        {
            get { return airVelocity; }
            set { airVelocity = value; }
        }
        private float walkingSpeed;

        public float WalkingSpeed
        {
            get { return walkingSpeed; }
            set { walkingSpeed = value; }
        }
        private float runningSpeed;

        public float RunningSpeed
        {
            get { return runningSpeed; }
            set { runningSpeed = value; }
        }

        public Vector2 LastPos
        {
            get { return lastPos; }
            set { lastPos = value; }
        }
       
        /*public enum State
        {
            Standing,
            Running,
            Jumping
        };

        protected State state;
        
         */

        protected PlayerState state;

        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        public float Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public PlayerState CurrentState
        {
            get { return state; }
            set { state = value; }
        }

        public Vector2 Movement
        {
            get { return movement; }
            set { movement = value; }
        }

        public GameActor()
            : base()
        {
            speed = 0f;
            velocity = new Vector2(0, 0.1f) ;
            state = JumpingState;
        }
        
        public virtual void jump() {}
        public virtual void moveRight() { }
        public virtual void moveLeft() { }
        public virtual void runRight() { }
        public virtual void runLeft() { }
        public virtual void stopMoving() { }
        public virtual void ApplyAirResistance() { }
        public virtual void airRight() { }
        public virtual void airLeft() { }
        public virtual void Run() { }
        public virtual void Walk() { }
        public virtual void reverseGravity() { }
        public virtual void throwRandom() { }
        public virtual void climbLadder() { }
        public virtual void glideLadder() { }
        public virtual void Attack() { }
        public virtual void dash(chip_ninja game) { }
    }
}

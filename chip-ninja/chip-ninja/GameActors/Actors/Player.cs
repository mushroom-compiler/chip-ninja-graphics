﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using chip_ninja.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using chip_ninja.States;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja
{
    public class Player : GameActor
    {
        Vector2 jumpVelocity = new Vector2(0, -2.4f);
        Vector2 runningJumpVelocity = new Vector2(0, -10f);
        Vector2 gravity = new Vector2(0, 0.15f);
        Vector2 climbingVelocity = new Vector2(0, -.5f);
        float airVelocity = 0.3f;
        GameTime gameTime;
        Stopwatch sW = new Stopwatch();
        int life;
        int invincibilityCounter;
        int furyCounter;
        bool alive;
        Player opponent;

        public Player Opponent
        {
            get { return opponent; }
            set { opponent = value; }
        }

        public int FuryCounter
        {
            get { return furyCounter; }
            set { furyCounter = value; }
        }

        public bool isInvincible()
        {
            return (invincibilityCounter > 0);
        }

        public int Life
        {
            get { return life; }
            set { life = value; }
        }

        bool isOnGround;
        List<IAdverseEntity> enemyList;

        public List<IAdverseEntity> EnemyList
        {
            get { return enemyList; }
            set { enemyList = value; }
        }

        public string multi_state = "normal";

        public Player(Vector2 position)
            : base()
        {
            speed = .5f;
            this.position = position;
            life = 100;
            alive = true;
        }

        public void LoadContent(ContentManager Content)
        {
            JumpingState = new JumpingState(Content);
            RunningState = new RunningState(Content);
            StandingState = new StandingState(Content);
            DoubleJumpingState = new DoubleJumpingState(Content);
            WalkingState = new WalkingState(Content);
            AttackState = new AttackState(Content);
            state = JumpingState;
        }

        public void Update(GameTime gameTime, Map map, bool multiplayer, List<IAdverseEntity> enemyList)
        {
            lastPos = position;
            this.gameTime = gameTime;
            this.EnemyList = enemyList;
            isOnGround = false;


            if (velocity.X > 0)
            {
                checkRightCollison(map, gameTime);
            }
            else if (velocity.X < 0)
            {
                checkLeftCollision(map, gameTime);
            }
            //else

            position.X += velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds;
            position.X = (position.X < 0) ? 0 : position.X;

            velocity += gravity;

            if (velocity.Y > 0 && position.Y > 0)
            {
                CheckDownCollision(map, gameTime);
            }
            else
                position.Y += velocity.Y * speed * gameTime.ElapsedGameTime.Milliseconds;


            if (multiplayer)
            {
                position.X = (position.X < 0) ? 0 : position.X;

                if (position.Y > 930)
                {
                    position.Y = 0;
                    velocity.Y = 1;
                }
            }

            if (!multiplayer)
            {
                state.Update(gameTime, this);
            }
            else if (multiplayer)
            {
                state.MultiUpdate(gameTime, this, Opponent);
            }

            hitbox = state.Hitbox;

            if (Math.Abs(velocity.X) <= 0.1 && isOnGround && state != AttackState)
                state = StandingState;


            if (life <= 0)
                Die();

            if (state == AttackState && state.Counter <= 0)
                state = state.Exit(this);


            /*switch (CurrentState)
            {
                case State.Jumping:
                    if (velocity.Y > 0 && position.Y > 0)
                    {
                        CanGoDown(map, gameTime);
                        position.X += velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds;
                    }
                    else
                        position += velocity * speed * gameTime.ElapsedGameTime.Milliseconds;
                    break;
                case State.Standing:
                    if (velocity.Y > 0 && position.Y > 0)
                    {
                        CanGoDown(map, gameTime);
                        position.X += velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds;
                    }
                    else
                        position += velocity * speed * gameTime.ElapsedGameTime.Milliseconds;
                    break;
                case State.Running:
                    if (velocity.Y > 0 && position.Y > 0)
                    {
                        CanGoDown(map, gameTime);
                        position.X += velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds;
                    }
                    else
                        position += velocity * speed * gameTime.ElapsedGameTime.Milliseconds;
                    break;
            }*/

            movement = position - lastPos;

            if (invincibilityCounter > 0)
                invincibilityCounter -= gameTime.ElapsedGameTime.Milliseconds;


        }

        public override void Update(GameTime gameTime)
        {
            throw new NotImplementedException();
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (isVisible)
            {
                state.Animation.Draw(spriteBatch, position);
            }
        }

        public void Die()
        {
            alive = false;
        }

        public bool IsAlive()
        {
            return alive;
        }

        public void GetDamage(int amount)
        {
            if (!isInvincible())
            {
                Console.WriteLine("I've lost " + amount + " points !");
                BecomeInvincible();
                Life -= amount;
            }
        }

        public override void Attack()
        {
            if (AttackState.Counter <= 0)
            {
                AttackState.Enter(this);
                state = AttackState;
            }
        }

        public void BecomeInvincible()
        {
            invincibilityCounter = 1500;
        }

        public override void dash(chip_ninja game)
        {

        }

        public override void jump()
        {
            if (velocity.Y >= -.5f)
            {

                if (state != AttackState)
                {
                    velocity.Y = state.JumpVelocity.Y;
                    state = (state == JumpingState) ? DoubleJumpingState : JumpingState;
                }
                else if (state.RemainingJumps > 0)
                {
                    state.RemainingJumps--;
                    velocity.Y = state.JumpVelocity.Y;
                }
            }

            if (state == DoubleJumpingState)
            {
                Console.Write("");
            }
        }

        public override void throwRandom()
        {
            Random rand = new Random();
            velocity = new Vector2((float)((rand.NextDouble() * 12) - 6), (float)((rand.NextDouble() * 12) - 6));
        }

        public override void moveRight()
        {
            if (state == JumpingState || state == DoubleJumpingState)
                airRight();
            else
            {
                velocity.X = state.MovementVelocity;

                if (state == StandingState || state != AttackState)
                    state = WalkingState;
            }

        }

        public override void moveLeft()
        {
            if (state == JumpingState || state == DoubleJumpingState)
                airLeft();
            else
            {
                velocity.X = -state.MovementVelocity;
                if (state == StandingState || state != AttackState)
                    state = WalkingState;
            }
        }

        public override void stopMoving()
        {
            velocity.X *= state.InertiaResistance;
        }

        /* public override void ApplyAirResistance()
         {
             velocity.X *= state.AirResistance;
         }*/

        public override void airLeft()
        {
            velocity.X = (velocity.X - airVelocity < -state.MovementVelocity) ? -state.MovementVelocity : velocity.X - airVelocity;
        }

        public override void airRight()
        {
            velocity.X = (velocity.X + airVelocity > state.MovementVelocity) ? state.MovementVelocity : velocity.X + airVelocity;
        }

        public override void reverseGravity()
        {
            gravity = new Vector2(0, -gravity.Y);
        }

        public override void Run()
        {
            if (state != AttackState)
                CurrentState = RunningState;
            JumpingState.MovementVelocity = CurrentState.MovementVelocity;
        }

        public override void Walk()
        {
            if (state != AttackState)
                CurrentState = WalkingState;
            JumpingState.MovementVelocity = CurrentState.MovementVelocity;
        }

        /* public override void runRight()
         {
             velocity.X = 2;
         }

         public override void runLeft()
         {
             velocity.X = -2;
         }*/

        public int GetLeftObstacleLine(int pos_x, Map map, int line)
        {
            if (line < 0)
                return -1;
            int tmp = (pos_x < 0) ? 0 : pos_x;
            for (int i = tmp; i > 0; i--)
            {
                if (map.TileMap[line, i] != 0 && map.TileMap[line, i] != 2)
                    return i;
            }
            return -1;
        }

        public Vector2 GetNearestLeftObstacle(int playerHeight, Rectangle hitbox, Map map)
        {
            int lineNumber = playerHeight / map.TileHeight + 1;
            Vector2 pos = new Vector2(-1, -1);
            int tmp = -1;
            int past = -1;

            for (int i = 0; i < lineNumber; i++)
            {
                tmp = GetLeftObstacleLine((int)hitbox.Left / map.TileWidth, map, (int)hitbox.Top / map.TileHeight + i);
                if (tmp != -1 && (past < tmp || past == -1))
                {
                    past = tmp;
                    pos = new Vector2((int)hitbox.Y / map.TileHeight + i, past);
                }
            }
            return pos;
        }

        public void checkLeftCollision(Map map, GameTime gameTime)
        {
            Vector2 nearestObstacleMapPosition = GetNearestLeftObstacle(hitbox.Height, hitbox, map);
            int nearestObstacleRealPosition = (int)nearestObstacleMapPosition.Y * map.TileHeight;
            float supposedPosition = (velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds);
            if (nearestObstacleMapPosition.X != -1)
            {
                float lol = Math.Max(supposedPosition, nearestObstacleRealPosition + 20 - hitbox.Left);
                if (lol != supposedPosition)
                {
                    velocity.X = 0;
                    position.X += lol;
                    if (state != AttackState)
                        CurrentState = StandingState;
                }
            }
        }

        public int GetRightObstacleLine(int pos_x, Map map, int line)
        {
            if (line < 0)
                return -1;
            int tmp = (pos_x < 0) ? 0 : pos_x;
            for (int i = tmp; i < map.Width; i++)
            {
                if (map.TileMap[line, i] != 0 && map.TileMap[line, i] != 2)
                    return i;
            }
            return -1;
        }

        public Vector2 GetNearestRightObstacle(int playerHeight, Rectangle hitbox, Map map)
        {
            int lineNumber = playerHeight / map.TileHeight + 1;
            Vector2 pos = new Vector2(-1, -1);
            int tmp = -1;
            int past = -1;

            for (int i = 0; i < lineNumber; i++)
            {
                tmp = GetRightObstacleLine(((int)hitbox.Right) / map.TileWidth, map, (int)hitbox.Top / map.TileHeight + i);
                if (tmp != -1 && (past > tmp || past == -1))
                {
                    past = tmp;
                    pos = new Vector2((int)hitbox.Y / map.TileHeight + i, past);
                }
            }
            return pos;
        }

        public void checkRightCollison(Map map, GameTime gameTime)
        {
            Vector2 nearestObstacleMapPosition = GetNearestRightObstacle(hitbox.Height, hitbox, map);
            int nearestObstacleRealPosition = (int)nearestObstacleMapPosition.Y * map.TileHeight;
            float supposedPosition = (velocity.X * speed * gameTime.ElapsedGameTime.Milliseconds);
            if (nearestObstacleMapPosition.X != -1)
            {
                float lol = Math.Min(supposedPosition, nearestObstacleRealPosition - hitbox.Right);
                if (lol != supposedPosition)
                {
                    velocity.X = 0;
                    position.X += lol;
                    if (state != AttackState)
                        CurrentState = StandingState;
                    velocity.Y = .5f;
                }
            }
        }

        public int GetDownObstacleColumn(int pos_y, Map map, int column)
        {
            if (column > map.Width)
                return -1;
            int tmp = (pos_y < 0) ? 0 : pos_y;
            for (int i = tmp; i < map.Height; i++)
            {
                if (map.TileMap[i, column] != 0)
                    return i;
            }
            return -1;
        }

        public Vector2 GetNearestDownObstacle(int playerWidth, Rectangle hitbox, Map map)
        {
            int columnNumber = playerWidth / map.TileWidth + 1;
            Vector2 pos = new Vector2(-1, -1);
            int tmp = -1;
            int past = -1;

            for (int i = 0; i < columnNumber + 1; i++)
            {
                tmp = GetDownObstacleColumn(((int)hitbox.Bottom + 19) / map.TileHeight, map, (((int)hitbox.Right - 18) / map.TileWidth) + i);
                if (past > tmp && tmp != -1 || past == -1)
                {
                    past = tmp;
                    pos = new Vector2((int)hitbox.X / map.TileWidth + i, past);
                }
            }
            return pos;
        }

        public void CheckDownCollision(Map map, GameTime gameTime)
        {
            Vector2 nearestObstacleMapPosition = GetNearestDownObstacle(hitbox.Width, hitbox, map);
            int nearestObstacleYPosition = (int)nearestObstacleMapPosition.Y * map.TileHeight;
            float supposedPosition = (velocity.Y * speed * gameTime.ElapsedGameTime.Milliseconds);
            float lol = Math.Min(supposedPosition, nearestObstacleYPosition - hitbox.Bottom);
            if (nearestObstacleMapPosition.Y != -1)
            {
                if (lol == supposedPosition && state != DoubleJumpingState && state != AttackState)
                {
                    state = JumpingState;
                }
                if (lol == 0)
                {
                    velocity.Y = 0;
                    state = (state == AttackState) ? AttackState : (state == RunningState) ? RunningState : (state == WalkingState) ? WalkingState : StandingState;
                    JumpingState.MovementVelocity = CurrentState.MovementVelocity;
                    isOnGround = true;
                }
                /* if (lol < 0)
                 {
                     position.Y += lol;
                     lol = 0;
                 }*/

                position.Y += lol * speed;
            }
            else
                position.Y += supposedPosition;
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Animations
{
    public class AnimationTest
    {
        Texture2D texture;

        Point frameSize = new Point(100, 45);

        bool facingRight;

        public bool FacingRight
        {
            get { return facingRight; }
            set { facingRight = value; }
        }

        public Point FrameSize
        {
            get { return frameSize; }
        }



        Point currentFrame = new Point(0, 0);
        Point sheetSize;

        int timeSinceLastFrame = 0;
        int millisecondsPerFrame = 50;
        Vector2 offset;


        public AnimationTest(Texture2D texture, int frameNumber, int frameLevel, int millisecondsPerFrame)
        {
            this.millisecondsPerFrame = millisecondsPerFrame;
            this.texture = texture;
            sheetSize = new Point(frameNumber, frameLevel);
            frameSize = new Point(texture.Width / frameNumber, texture.Height);
            facingRight = true;
            offset = Vector2.Zero;
        }

        public AnimationTest(Texture2D texture, int frameNumber, int frameLevel, int millisecondsPerFrame, Vector2 offset)
        {
            this.millisecondsPerFrame = millisecondsPerFrame;
            this.texture = texture;
            sheetSize = new Point(frameNumber, frameLevel);
            frameSize = new Point(texture.Width / frameNumber, texture.Height);
            facingRight = true;
            this.offset = offset;
        }


        public void Update(GameTime gameTime, Vector2 velocity)
        {
            timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
            if (timeSinceLastFrame > millisecondsPerFrame)
            {
                timeSinceLastFrame -= millisecondsPerFrame;
                ++currentFrame.X;
                if (currentFrame.X >= sheetSize.X)
                {
                    currentFrame.X = 0;
                    ++currentFrame.Y;
                    if (currentFrame.Y >= sheetSize.Y)
                        currentFrame.Y = 0;
                }
            }

            if (velocity.X > 0)
                facingRight = true;
            else if (velocity.X < 0)
                facingRight = false;
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 position)
        {
            spriteBatch.Draw(texture, position + offset, new Rectangle(currentFrame.X * FrameSize.X, currentFrame.Y * FrameSize.Y, FrameSize.X, FrameSize.Y), Color.White, 0, Vector2.Zero, 1, (facingRight) ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 1);
        }
    }
}

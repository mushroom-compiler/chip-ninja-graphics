﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Animations
{
    public class Animation
    {
        Texture2D animationSprite;
        public int frameCount;
        int currentFrame;
        Rectangle frameRectangle;
        public Vector2 drawPosition;
        float timeUntilNextFrame /* ms */;
        float frameDuration = 50 /* ms */;
        int frameLevel;
        public bool play = false;

        public bool right = true;

        public Animation(Texture2D animationSprite, int frameCount, int frameLevel)
        {
            frameLevel = frameLevel >= 1 ? frameLevel : 1;
            this.animationSprite = animationSprite;
            this.frameCount = frameCount - 1;
            this.frameRectangle = new Rectangle(0, 0, this.animationSprite.Width / frameCount, this.animationSprite.Height / frameLevel);
            this.timeUntilNextFrame = frameDuration;
            this.currentFrame = 0;
            this.frameLevel = frameLevel;
        }

        public void Update(GameTime gameTime)
        {
            timeUntilNextFrame -= gameTime.ElapsedGameTime.Milliseconds;

            if (play)
                if (timeUntilNextFrame <= 0)
                {
                    currentFrame++;
                    timeUntilNextFrame = frameDuration;
                }

            if (currentFrame >= frameCount)
                currentFrame = 0;

            frameRectangle.X = animationSprite.Width / (frameCount + 1) * currentFrame;
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 position)
        {
            spriteBatch.Draw(animationSprite, position, frameRectangle, Color.White, 0.0f, Vector2.Zero, 1.0f, right ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 1.0f);
        }
    }
}

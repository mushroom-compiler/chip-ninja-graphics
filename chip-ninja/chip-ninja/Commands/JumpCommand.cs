﻿
namespace chip_ninja.Commands
{
    class JumpCommand : Command
    {
        public virtual void execute(ref GameActor actor)
        {
            actor.jump();
        }
    }
}

﻿
namespace chip_ninja.Commands
{
    abstract class Command
    {
        public Command() { }
        public virtual void execute(ref GameActor actor) { }
    }
}

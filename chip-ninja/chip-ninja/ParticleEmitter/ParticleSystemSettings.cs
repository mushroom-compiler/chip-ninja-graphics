﻿
namespace chip_ninja.Graphics.Particule
{
    public class ParticleSystemSettings
    {
        private int initialParticleCount = 200;
        private float particlesPerSec = 100.0f;
        private string particleTexFileName = null;
        private float minDirectionAngle = 0.0f;
        private float maxDirectionAngle = 360.0f;
        private float minSpeed = 50.0f;
        private float maxSpeed = 100.0f;
        private float minLifeTime = 1.0f;
        private float maxLifeTime = 2.5f;
        private float minScale = 0.1f;
        private float maxScale = 1.0f;

        public ParticleSystemSettings()
        {
        }

        public void SetDirectionAngles(float min, float max)
        {
            minDirectionAngle = min;
            maxDirectionAngle = max;
        }

        public void SetSpeeds(float min, float max)
        {
            minSpeed = min;
            maxSpeed = max;
        }

        public void SetLifeTimes(float min, float max)
        {
            minLifeTime = min;
            maxLifeTime = max;
        }

        public void SetScales(float min, float max)
        {
            minScale = min;
            maxScale = max;
        }


        public int InitialParticleCount
        {
            get { return initialParticleCount; }
            set { initialParticleCount = value; }
        }

        public float ParticlesPerSec
        {
            get { return particlesPerSec; }
            set { particlesPerSec = value; }
        }

        public string ParticleTexName
        {
            get { return particleTexFileName; }
            set { particleTexFileName = value; }
        }

        public float MinDirectionAngle
        {
            get { return minDirectionAngle; }
            set { minDirectionAngle = value; }
        }

        public float MaxDirectionAngle
        {
            get { return maxDirectionAngle; }
            set { maxDirectionAngle = value; }
        }

        public float MinSpeed
        {
            get { return minSpeed; }
            set { minSpeed = value; }
        }

        public float MaxSpeed
        {
            get { return maxSpeed; }
            set { maxSpeed = value; }
        }

        public float MinLifeTime
        {
            get { return minLifeTime; }
            set { minLifeTime = value; }
        }

        public float MaxLifeTime
        {
            get { return maxLifeTime; }
            set { maxLifeTime = value; }
        }

        public float MinScale
        {
            get { return minScale; }
            set { minScale = value; }
        }

        public float MaxScale
        {
            get { return maxScale; }
            set { maxScale = value; }
        }
    }
}

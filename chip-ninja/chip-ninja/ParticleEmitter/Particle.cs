﻿using Microsoft.Xna.Framework;

namespace chip_ninja.Graphics.Particule
{
    public class Particle
    {
        private Vector2 position;         
        private Vector2 velocity;         
        private float lifeTime = 0.0f;    
        private float startTime = 0.0f;   
        private float scale = 1.0f;       
        private bool active = false;      

        public Particle()
        {
            position = new Vector2();
            velocity = new Vector2();
        }

        public void Initialize(Vector2 pos, Vector2 vel, float _lifeTime, float _scale, bool _active)
        {
            position = pos;
            velocity = vel;
            lifeTime = _lifeTime;
            scale = _scale;
            active = _active;
            startTime = 0.0f;
        }

        public void Update(float deltaTime)
        {
            position += velocity * deltaTime;

            startTime += deltaTime;

            if (startTime > lifeTime)
                active = false;
        }

        public Vector2 Position 
        { 
            get { return position; }
            set { position = value; } 
        }

        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        public float LifeTime
        {
            get { return lifeTime; }
            set { lifeTime = value; }
        }

        public float LifeTimeStart
        {
            get { return startTime; }
            set { startTime = value; }
        }

        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public float Scale
        {
            get { return scale; }
            set { scale = value; }
        }
    }
}

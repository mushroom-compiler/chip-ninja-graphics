﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Graphics.Particule
{
    public class ParticleSystem : DrawableGameComponent
    {
        private Texture2D particleTex;
        private Vector2 origin;
        private Vector2 originPosition;

        //private SpriteBatch spriteBatch;

        //private Color colorr;

        private List<Particle> particleList;

        Random rand = new Random();

        private float elapsedTime = 0.0f;

        private ParticleSystemSettings settings = null;

        public bool visible = true;

        public ParticleSystem(Game game, ParticleSystemSettings newSettings)
            : base(game)
        {
            settings = newSettings;

            particleList = new List<Particle>(settings.InitialParticleCount);

            for (int i = 0; i < settings.InitialParticleCount; ++i)
            {
                particleList.Add(new Particle());
            }
        }

        private Vector2 GetVelocity()
        {
            float angle = RandomMinMax(settings.MinDirectionAngle, settings.MaxDirectionAngle);

            angle = MathHelper.ToRadians(-angle);

            return new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
            //MathHelper.SmoothStep(........................
        }

        protected override void LoadContent()
        {
            particleTex = Game.Content.Load<Texture2D>(settings.ParticleTexName);

            origin = new Vector2(particleTex.Width / 2, particleTex.Height / 2);

            originPosition = new Vector2();

            //spriteBatch = new SpriteBatch(GraphicsDevice);

            //colorr = new Color();

            base.LoadContent();
        }

        protected override void Dispose(bool disposing)
        {
            particleList.Clear();

            base.Dispose(disposing);
        }

        private void UpdateParticlesPerSec(GameTime gameTime)
        {
            elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            float particlePerSec = settings.ParticlesPerSec * elapsedTime;

            foreach (Particle particule in particleList)
            {
                if (particlePerSec > 0.0f)
                {
                    if (particule.Active == false)
                    {
                        InitializeParticle(particule);
                        particlePerSec--;
                    }
                }

                if (particule.Active)
                    particule.Update((float)(gameTime.ElapsedGameTime.TotalSeconds));
            }
        }

        public override void Update(GameTime gameTime)
        {
            UpdateParticlesPerSec(gameTime);

            base.Update(gameTime);
        }

        private void InitializeParticle(Particle particle)
        {
            Vector2 velocity = GetVelocity();

            float speed = RandomMinMax(settings.MinSpeed, settings.MaxSpeed);

            velocity *= speed;

            float lifeTime = RandomMinMax(settings.MinLifeTime, settings.MaxLifeTime);

            float scale = RandomMinMax(settings.MinScale, settings.MaxScale);

            particle.Initialize(originPosition, velocity, lifeTime, scale, true);
        }

        private float RandomMinMax(float min, float max)
        {
            lock (new object())
                return min + (float)rand.NextDouble() * (max - min);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.Begin();
            if (visible)
            foreach (Particle p in particleList)
            {
                if (p.Active)
                {
                    float remainingLife = p.LifeTimeStart / p.LifeTime;
                    float alpha = 4 * remainingLife * (1 - remainingLife);
                    //colorr = new Color(RandomMinMax(0.0f, 1.0f), RandomMinMax(0.0f, 1.0f), RandomMinMax(0.0f, 1.0f));

                    Color color = Color.White * alpha;

                    spriteBatch.Draw(particleTex, p.Position, null, color, 0.0f, origin, p.Scale, SpriteEffects.None, 0.0f);
                }
            }
            //spriteBatch.End();
            //base.Draw(gameTime);
        }

        public Vector2 OriginPosition
        {
            get { return originPosition; }
            set { originPosition = value; }
        }

        public ParticleSystemSettings Settings
        {
            get { return settings; }
            set { settings = value; }
        }

        public Texture2D ParticuleTex
        {
            set { particleTex = value; }
        }
    }
}

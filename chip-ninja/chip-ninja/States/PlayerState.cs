﻿using Microsoft.Xna.Framework;
using chip_ninja.Animations;
using Microsoft.Xna.Framework.Content;

namespace chip_ninja.States
{
    public abstract class PlayerState
    {

        protected AnimationTest animation;
        private Rectangle damageHitbox;

        protected int counter;

        protected PlayerState previousState;

        public virtual int Counter
        {
            get { return counter; }
        }

        public virtual Rectangle DamageHitbox
        {
            get { return damageHitbox; }
            set { damageHitbox = value; }
        }

        public PlayerState(ContentManager Content)
        {
            damageHitbox = Rectangle.Empty;
        }

        public virtual void Enter(Player player)
        {

        }

        public virtual PlayerState Exit(Player player)
        {
            return previousState;
        }

        public virtual void Update(GameTime gameTime, Player player)
        {
            animation.Update(gameTime, player.Velocity);
            hitbox = new Rectangle((int)player.Position.X, (int)player.Position.Y, 35, 35);
        }

        public virtual void MultiUpdate(GameTime gameTime, Player player, Player opponent)
        {
            animation.Update(gameTime, player.Velocity);
            hitbox = new Rectangle((int)player.Position.X, (int)player.Position.Y, 35, 35);
            opponent.animation.Update(gameTime, opponent.Velocity);
        }

        public virtual AnimationTest Animation
        {
            get { return animation; }
            set { animation = value; }
        }

        protected Rectangle hitbox;

        public virtual Rectangle Hitbox
        {
            get { return hitbox; }
            set { hitbox = value; }
        }
        protected Vector2 jumpVelocity;
        
        protected float movementVelocity;

        protected float inertiaResistance;

        protected int remainingJumps;

        public virtual int RemainingJumps
        {
            get { return remainingJumps; }
            set { remainingJumps = value; }
        }

        public virtual float InertiaResistance
        {
            get { return inertiaResistance; }
            set { inertiaResistance = value; }
        }

        public virtual Vector2 JumpVelocity
        {
            get { return jumpVelocity; }
            set { jumpVelocity = value; }
        }

        public virtual float MovementVelocity
        {
            get { return movementVelocity; }
            set { movementVelocity = value; }
        }

    }
}

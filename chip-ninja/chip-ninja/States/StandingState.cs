﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.States
{
    class StandingState : PlayerState
    {
        Vector2 jumpVelocity = new Vector2(0, -2f);
        float movementVelocity = 1;
        float inertiaResistance = 0.1f;
        int remainingJumps = 2;

        public StandingState(ContentManager Content)
            : base(Content)
        {
            animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning"), 5, 1, 100);
        }

        public override void Update(GameTime gameTime, Player player)
        {
            animation.Update(gameTime, player.Velocity);
            hitbox = new Rectangle((int)player.Position.X, (int)player.Position.Y, 35, 35);
        }

        public override int RemainingJumps
        {
            get { return remainingJumps; }
            set { remainingJumps = value; }
        }

        public override Vector2 JumpVelocity
        {
            get { return jumpVelocity; }
            set { jumpVelocity = value; }
        }

        public override float InertiaResistance
        {
            get { return inertiaResistance; }
            set { inertiaResistance = value; }
        }

        public override float MovementVelocity
        {
            get { return movementVelocity; }
            set { movementVelocity = value; }
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.States
{
    class WalkingState : PlayerState
    {
        Vector2 jumpVelocity = new Vector2(0, -2.4f);

        float movementVelocity = 1f;
        float inertiaResistance = 0.8f;
        int remainingJumps = 2;

        public WalkingState(ContentManager Content)
            : base(Content)
        {
            animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning"), 5, 1, 90);
        }

        public override void Update(GameTime gameTime, Player player)
        {
            animation.Update(gameTime, player.Velocity);
            hitbox = new Rectangle((int)player.Position.X, (int)player.Position.Y, 35, 35);
        }

        public override int RemainingJumps
        {
            get { return remainingJumps; }
            set { remainingJumps = value; }
        }

        public override float InertiaResistance
        {
            get { return inertiaResistance; }
            set { inertiaResistance = value; }
        }

        public override Vector2 JumpVelocity
        {
            get { return jumpVelocity; }
            set { jumpVelocity = value; }
        }

        public override float MovementVelocity
        {
            get { return movementVelocity; }
            set { movementVelocity = value; }
        }

    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.States
{
    class RunningState : PlayerState
    {
        Vector2 jumpVelocity = new Vector2(0, -2.4f);
        float movementVelocity = 2;
        float inertiaResistance = 0.85f;
        int remainingJumps = 2;

        public RunningState(ContentManager Content)
            : base(Content)
        {
            animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning"), 5, 1, 60);
        }

        public override void Update(GameTime gameTime, Player player)
        {
            animation.Update(gameTime, player.Velocity);
            hitbox = new Rectangle((int)player.Position.X, (int)player.Position.Y, 35, 35);
        }

        public override int RemainingJumps
        {
            get { return remainingJumps; }
            set { remainingJumps = value; }
        }

        public override float InertiaResistance
        {
            get { return inertiaResistance; }
            set { inertiaResistance = value; }
        }

        public override Vector2 JumpVelocity
        {
            get { return jumpVelocity; }
            set { jumpVelocity = value; }
        }

        public override float MovementVelocity
        {
            get { return movementVelocity; }
            set { movementVelocity = value; }
        }
    }
}

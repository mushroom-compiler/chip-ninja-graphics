﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using chip_ninja;

namespace chip_ninja.States
{
    class PlayerStateManager
    {
        PlayerState currentState;

        public PlayerStateManager()
        {

        }

        public void ChangeState(PlayerState state)
        {
            currentState = state;
        }

    }
}

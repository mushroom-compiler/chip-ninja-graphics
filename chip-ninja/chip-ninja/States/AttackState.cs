﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using chip_ninja.Animations;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.States
{
    class AttackState : PlayerState
    {
        new int counter;
        Vector2 offset;

        public override int Counter
        {
            get { return this.counter; }
        }

        public override int RemainingJumps
        {
            get
            {
                return this.remainingJumps;
            }
            set
            {
                this.remainingJumps = value;
            }
        }

        public AttackState(ContentManager Content)
            : base(Content)
        {
            animation = new AnimationTest(Content.Load<Texture2D>("Attack"), 1, 1, 100, new Vector2(-45, 0));
        }

        public override void Enter(Player player)
        {
            previousState = player.CurrentState;
            jumpVelocity = player.CurrentState.JumpVelocity;
            movementVelocity = player.CurrentState.MovementVelocity;
            inertiaResistance = player.CurrentState.InertiaResistance;
            remainingJumps = player.CurrentState.RemainingJumps;
            hitbox = player.CurrentState.Hitbox;
            counter = 100;
        }

        public override void Update(GameTime gameTime, Player player)
        {
            counter -= gameTime.ElapsedGameTime.Milliseconds;
            animation.Update(gameTime, player.Velocity);
            hitbox = new Rectangle((int)player.Position.X, (int)player.Position.Y, 35, 35);
            if (animation.FacingRight)
                DamageHitbox = new Rectangle(player.Hitbox.Left - 20, player.Hitbox.Y, 100, 50);
            else
                DamageHitbox = new Rectangle(player.Hitbox.Right - 80, player.Hitbox.Y, 100, 50);

            foreach (var entity in player.EnemyList)
            {
                if (player.CurrentState.DamageHitbox.Intersects(entity.GetHitbox()))
                {
                    if (!entity.IsAlly())
                    {
                        player.FuryCounter++;
                        entity.OnDamage();
                    }
                }
            }
        }

        public override void MultiUpdate(GameTime gameTime, Player player, Player opponent)
        {
            counter -= gameTime.ElapsedGameTime.Milliseconds;
            animation.Update(gameTime, player.Velocity);
            hitbox = new Rectangle((int)player.Position.X, (int)player.Position.Y, 35, 35);
            if (animation.FacingRight)
                DamageHitbox = new Rectangle(player.Hitbox.Left - 20, player.Hitbox.Y, 100, 50);
            else
                DamageHitbox = new Rectangle(player.Hitbox.Right - 80, player.Hitbox.Y, 100, 50);

                if (player.CurrentState.DamageHitbox.Intersects(opponent.Hitbox))
                {
                        player.GetDamage(100);
                }
        }

        public override PlayerState Exit(Player player)
        {
            if (remainingJumps == 0)
                return player.DoubleJumpingState;
            return previousState;
        }
    }
}

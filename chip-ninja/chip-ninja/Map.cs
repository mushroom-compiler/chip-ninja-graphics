﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using chip_ninja.Enemies;

namespace chip_ninja
{
    public class Map
    {
        List<Texture2D> tiles; //List of different tiles.
        int[,] tileMap; //A tileMap
        int tileWidth = 20; //Speaks for itself.
        int tileHeight = 20;
        protected int height;
        protected int width;
        int tileSize;
        int tileSet;
        Vector2 playerSpawn;
        int ruleSet;
        List<Interfaces.IAdverseEntity> enemyList;

        public List<Interfaces.IAdverseEntity> EnemyList
        {
            get { return enemyList; }
            set { enemyList = value; }
        }


        public int TileSize
        {
            get { return tileSize; }
        }

        public Vector2 PlayerSpawn
        {
            get { return playerSpawn; }
        }

        public int RuleSet
        {
            get { return ruleSet; }
        }

        public int Height
        {
            get { return height; }
        }

        public int Width
        {
            get { return width; }
        }

        public int TileWidth
        {
            get { return tileWidth; }
        }

        public int TileHeight
        {
            get { return tileHeight; }
        }

        public int[,] TileMap
        {
            get { return tileMap; }
        }

        public Map(string levelName, ContentManager content, List<Interfaces.IAdverseEntity> enemyList)
        {
            this.enemyList = enemyList;
            LoadTileTextures(content); //Add all textures to the list.
            LoadMapData(levelName); //Load the given map data.
        }


        void LoadTileTextures(ContentManager content)
        {
            tiles = new List<Texture2D>();
            tiles.Add(content.Load<Texture2D>(@"Tiles\nothing")); //Add transparent texture to the list.
            tiles.Add(content.Load<Texture2D>(@"Tiles\block"));
            tiles.Add(content.Load<Texture2D>(@"Tiles\block"));//Add the block texture to the list.
            //tiles.Add(content.Load<Texture2D>(@"Tiles\ladder"));
        }

        //We the tilemap, and then, we draw.

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int y = 0; y < tileMap.GetLength(0); y++)
            {
                for (int x = 0; x < tileMap.GetLength(1); x++)
                {
                    if (tileMap[y, x] != 0 && tileMap[y, x] != 3)
                        spriteBatch.Draw(tiles[tileMap[y, x]], new Vector2(x * tileWidth, y * tileHeight), Color.White);
                }
            }
        }

        void LoadMapData(string name)
        {
            string path = name + ".txt"; //Load the given name's txt.

            int rwidth = 0;
            int rheight = File.ReadLines(path).Count(); //We calculate the dimensions of the file.

            StreamReader sReader = new StreamReader(path);
            string line = sReader.ReadLine();
            line = sReader.ReadLine();
            string[] tileNo = line.Split(','); //We separate each tile by ','

            rwidth = tileNo.Count();
            tileMap = new int[rheight, rwidth];
            sReader.Close();

            sReader = new StreamReader(path);

            line = sReader.ReadLine();
            tileNo = line.Split(',');

            height = Convert.ToInt32(tileNo[0]);
            width = Convert.ToInt32(tileNo[1]);
            tileSize = Convert.ToInt32(tileNo[2]);
            tileSet = Convert.ToInt32(tileNo[3]);
            playerSpawn = new Vector2(Convert.ToInt32(tileNo[4]), Convert.ToInt32(tileNo[5]));
            ruleSet = Convert.ToInt32(tileNo[6]);



            for (int y = 0; y < rheight - 1; y++)
            {
                line = sReader.ReadLine();
                tileNo = line.Split(',');

                for (int x = 0; x < rwidth; x++)
                {
                    if (Convert.ToInt32(tileNo[x]) == 30)
                    {
                        EnemyList.Add(new Met(new Vector2(x * tileSize, y * tileSize - 40)));
                        tileMap[y, x] = 0;
                    }
                    else if (Convert.ToInt32(tileNo[x]) == 31)
                    {
                        EnemyList.Add(new SniperMet(new Vector2(x * tileSize, y * tileSize - 40)));
                        tileMap[y, x] = 0;
                    }
                    else
                    tileMap[y, x] = Convert.ToInt32(tileNo[x]);
                }
            }
            sReader.Close();
            height = rheight - 1;
            width = rwidth - 1;
        }


    }

}

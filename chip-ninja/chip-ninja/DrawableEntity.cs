﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using chip_ninja.Animations;

namespace chip_ninja
{
    abstract public class DrawableEntity : Entity
    {
        public AnimationTest animation;
        protected Texture2D texture, attackTex, tmp;

        protected bool isVisible;

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public DrawableEntity()
            : base()
        {
            isVisible = true;
        }

        public void LoadContent(ContentManager Content, string assetName, int frameCount, int frameLevel)
        {
            texture = Content.Load<Texture2D>(assetName);
            //animation = new AnimationTest(texture, frameCount, frameLevel);
            animation = new AnimationTest(texture, frameCount, frameLevel, 50);
        }

        public void LoadContent(ContentManager Content, string assetName)
        {
            texture = Content.Load<Texture2D>(assetName);
        }

        public void LoadAttackTex(ContentManager Content, string path)
        {
            attackTex = Content.Load<Texture2D>(path);
        }

        public Texture2D GetTexture()
        {
            return texture;
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (isVisible)
            {
                    animation.Draw(spriteBatch, position);
            }
        }
    }
}

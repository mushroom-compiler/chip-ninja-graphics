﻿using FarseerPhysics;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;

namespace chip_ninja.Graphics.TextureCutting
{
    public class TexCutter
    {
        public static void CutFirst(World world, Vector2 start, Vector2 end, float thickness)
        {
            Fixture fixture = null;
            Vector2 entryPoint = Vector2.Zero;
            Vector2 exitPoint = Vector2.Zero;

            if (world.TestPoint(start) != null || world.TestPoint(end) != null)
                return;

            world.RayCast((f, p, n, fr) =>
            {
                if (!f.Body.IsStatic)
                {
                    fixture = f;
                    entryPoint = p;
                    return 0;
                }
                return 1;
            }, start, end);

            world.RayCast((f, p, n, fr) =>
            {
                if (fixture == f)
                {
                    exitPoint = p;
                    return 0;
                }
                return 1;
            }, end, start);

            if (fixture == null)
                return;
            if (fixture.Shape.ShapeType != ShapeType.Polygon)
                return;

            if (fixture.Body.BodyType != BodyType.Static)
            {
                TexturedFixture texData = (TexturedFixture)fixture.UserData;

                Vertices first;
                Vertices second;
                CuttingTools.SplitShape(fixture, entryPoint, exitPoint, thickness, out first, out second);


                Body firstFixture = BodyFactory.CreatePolygon(world, first, fixture.Shape.Density, fixture.Body.Position, new TexturedFixture(texData.texture, first, texData.min, texData.max));

                firstFixture.Rotation = fixture.Body.Rotation;
                firstFixture.LinearVelocity = fixture.Body.LinearVelocity;
                firstFixture.AngularVelocity = fixture.Body.AngularVelocity;
                firstFixture.BodyType = BodyType.Dynamic;

                Body secondFixture = BodyFactory.CreatePolygon(world, second, fixture.Shape.Density, fixture.Body.Position, new TexturedFixture(texData.texture, second, texData.min, texData.max));

                secondFixture.Rotation = fixture.Body.Rotation;
                secondFixture.LinearVelocity = fixture.Body.LinearVelocity;
                secondFixture.AngularVelocity = fixture.Body.AngularVelocity;
                secondFixture.BodyType = BodyType.Dynamic;

                world.RemoveBody(fixture.Body);
            }
        }

    }
}

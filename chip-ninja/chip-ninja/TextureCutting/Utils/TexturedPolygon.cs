using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace chip_ninja.Graphics.TextureCutting
{
    public class TexturedPolygon
    {
        public List<Vector2> Points;
        public List<Vector2> UVs;

        public List<Color> Colors;

        public int Count { get { return Points.Count; } }

        private Vector2 lastPosition;
        private float lastRoation;
        private List<VertexPositionColorTexture> cachedPoints;
        private List<int> cachedIndices;
        private int triangles;

        public TexturedPolygon()
        {
            Points = new List<Vector2>();
            UVs = new List<Vector2>();
            Colors = new List<Color>();
            cachedPoints = new List<VertexPositionColorTexture>();
            cachedIndices = new List<int>();
        }

        public void MapUVs(Vector2 topLeft, Vector2 bottomRight)
        {
            UVs.Clear();
            for (int i = 0; i < Points.Count; i++)
                UVs.Add(new Vector2(Math.Abs((Points[i].X + topLeft.X) / (bottomRight.X - topLeft.X)), Math.Abs((Points[i].Y + topLeft.Y) / (bottomRight.Y - topLeft.Y))));
        }

        public void GetTriangleList(Vector2 position, float rotation, out VertexPositionColorTexture[] points, out int vertexCount, out int[] indices, out int triangleCount, bool forceRebuild = false)
        {
            if (lastPosition != position || lastRoation != rotation || !forceRebuild)
            {
                Matrix xform = Matrix.CreateRotationZ(rotation) * Matrix.CreateTranslation(new Vector3(position, 0));
                Vector2[] tempPoints = new Vector2[Count];
                Vector2.Transform(Points.ToArray(), ref xform, tempPoints);

                cachedPoints.Clear();
                cachedIndices.Clear();
                triangles = 0;

                for (int i = 0; i < Count; i++)
                    cachedPoints.Add(new VertexPositionColorTexture(new Vector3(tempPoints[i], 0), Colors[i], UVs[i]));

                for (int i = 1; i < Count; i++)
                {
                    cachedIndices.Add(0);
                    cachedIndices.Add(i);
                    cachedIndices.Add((i + 1) % Count);
                    triangles++;
                }

                points = cachedPoints.ToArray();
                vertexCount = Count;
                indices = cachedIndices.ToArray();
                triangleCount = triangles;

                lastPosition = position;
                lastRoation = rotation;
            }
            else
            {
                points = cachedPoints.ToArray();
                vertexCount = Count;
                indices = cachedIndices.ToArray();
                triangleCount = triangles;
            }
        }


    }
}

﻿using FarseerPhysics.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Graphics.TextureCutting
{
    public class TexturedFixture
    {
        public Texture2D texture;
        public Vector2 min, max;
        public TexturedPolygon polygon;

        public TexturedFixture(Texture2D tex, Vertices vertices)
        {
            texture = tex;
            min = new Vector2(float.MaxValue, float.MaxValue);
            max = new Vector2(float.MinValue, float.MinValue);

            foreach (var point in vertices)
            {
                if (point.X < min.X)
                    min.X = point.X;
                if (point.Y < min.Y)
                    min.Y = point.Y;
                
                if (point.X > max.X)
                    max.X = point.X;
                if (point.Y > max.Y)
                    max.Y = point.Y;
            }
            polygon = new TexturedPolygon();

            polygon.Points = vertices;
            polygon.MapUVs(min, max);

            for (int i = 0; i < polygon.Count; i++)
                polygon.Colors.Add(Color.White);
        }

        public TexturedFixture(Texture2D tex, Vertices verts, Vector2 min, Vector2 max)
        {
            texture = tex;
            this.min = min;
            this.max = max;
            polygon = new TexturedPolygon();

            polygon.Points = verts;
            polygon.MapUVs(min, max);

            for (int i = 0; i < polygon.Count; i++)
                polygon.Colors.Add(Color.White);
        }
    }
}

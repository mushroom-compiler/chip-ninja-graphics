
namespace chip_ninja
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (chip_ninja game = new chip_ninja())
            {
                game.Run();
            }
        }
    }
#endif
}


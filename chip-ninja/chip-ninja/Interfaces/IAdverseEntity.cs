﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace chip_ninja.Interfaces
{
    public interface IAdverseEntity
    {
        bool IsAlive();
        Texture2D GetTexture();
        Rectangle GetHitbox();
        void Update(GameTime gameTime, Map map, Player player, List<IAdverseEntity> enemyList);
        void Draw(SpriteBatch spriteBatch, GameTime gameTime);
        void LoadContent(ContentManager content);
        void OnDamage();
        int GetDataCode();
        bool IsAlly();

        Vector2 GetPosition();
    }
}

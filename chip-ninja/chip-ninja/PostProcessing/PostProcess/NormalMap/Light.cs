﻿using Microsoft.Xna.Framework;

namespace chip_ninja.Graphics.PostProcess.NormalMapping
{
    //public enum LightType
    //{
    //    Point
    //}

    public class Light
    {
        protected float _initialPower;

        public Vector3 Position { get; set; }
        public Vector4 Color;

        public float CurrentPower { get; set; }

        public float specularStrength = 1.0f;
        public float SpecularStrength
        {
            get { return specularStrength; }
            set { specularStrength = value; }
        }

        public float Power
        {
            get { return _initialPower; }
            set
            {
                _initialPower = value;
                CurrentPower = _initialPower;
            }
        }

        public int Decay { get; set; }

        //public LightType LightType { get; private set; }

        public bool IsEnabled { get; set; }

        public Vector3 Direction { get; set; }

        public Light(/*LightType lightType*/)
        {
            //LightType = lightType;
        }

        public void Enable(bool enabled)
        {
            IsEnabled = enabled;
        }

        public void Update(GameTime gameTime)
        {
            if (!IsEnabled) return;
        }

        protected void CopyBaseFields(Light light)
        {
            light.Color = this.Color;
            light.IsEnabled = this.IsEnabled;
            light.Decay = this.Decay;
            //light.LightType = this.LightType;
            light.Position = this.Position;
            light.Power = this.Power;
        }
    }
}
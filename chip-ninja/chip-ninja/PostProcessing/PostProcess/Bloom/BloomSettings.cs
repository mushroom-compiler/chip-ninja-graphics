﻿namespace chip_ninja.Graphics.PostProcess.Bloom
{
    public class BloomSettings
    {
        public readonly string Name;


        public readonly float BloomThreshold;

        public readonly float BlurAmount;

        public readonly float BloomIntensity;
        public readonly float BaseIntensity;

        public readonly float BloomSaturation;
        public readonly float BaseSaturation;

        public BloomSettings(string name, float bloomThreshold, float blurAmount,
                         float bloomIntensity, float baseIntensity,
                         float bloomSaturation, float baseSaturation)
        {
            Name = name;
            BloomThreshold = bloomThreshold;
            BlurAmount = blurAmount;
            BloomIntensity = bloomIntensity;
            BaseIntensity = baseIntensity;
            BloomSaturation = bloomSaturation;
            BaseSaturation = baseSaturation;
        }

        public static BloomSettings preset = new BloomSettings("Default", 0.25f, 4, 1.25f, 1, 1, 1);
    }
}

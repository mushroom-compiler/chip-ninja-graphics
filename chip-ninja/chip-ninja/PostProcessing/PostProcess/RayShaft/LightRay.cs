﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace chip_ninja.Graphics.PostProcess.RayShaft
{
    public class LightRay : BasePostProcess
    {
        public Vector2 lightPosOnScreen;
        public float Density = .5f;
        public float Decay = .95f;
        public float Weight = 1.0f;
        public float Exposure = .15f;
        public int nbSamples = 64;
        //public Vector2 direction;

        public EffectTechnique _currentTech;
        public EffectParameter _density;
        public EffectParameter _decay;
        public EffectParameter _weight;
        public EffectParameter _Exposure;
        public EffectParameter _nbSamples;
        public EffectParameter _lightPosOnScreen;
        public EffectParameter _direction;

        public LightRay(Game game, Vector2 sourcePos, float density, float decay, float weight, float exposure, int nbsamples = 64 /*, Vector2 direction*/)
            : base(game)
        {
            lightPosOnScreen = sourcePos;
            
            Density = density;
            Decay = decay;
            Weight = weight;
            Exposure = exposure;
            this.nbSamples = nbsamples;
            //this.direction = direction;
        }

        public override void ExctractParameters()
        {
            if (effect == null)
                effect = game.Content.Load<Effect>("Shaders/PostProcessing/LigthRays");

            _currentTech = effect.Techniques["LightRay"];
            _density = effect.Parameters["Density"];
            _decay = effect.Parameters["Decay"];
            _weight = effect.Parameters["Weight"];
            _Exposure = effect.Parameters["Exposure"];
            _nbSamples = effect.Parameters["NBSAMPLES"];
            _lightPosOnScreen = effect.Parameters["lightPosOnScreen"];
            _direction = effect.Parameters["direction"];

            base.ExctractParameters();
        }

        public override void Draw(GameTime gameTime)
        {
            if (effect == null)
                effect = game.Content.Load<Effect>("Shaders/PostProcessing/LigthRays");

            effect.CurrentTechnique = _currentTech;

            _density.SetValue(Density);
            _decay.SetValue(Decay);
            _weight.SetValue(Weight);
            _Exposure.SetValue(Exposure);
            _nbSamples.SetValue(nbSamples);
            _lightPosOnScreen.SetValue(lightPosOnScreen);
            _direction.SetValue(new Vector2(0, 45));

            base.Draw(gameTime);

        }
    }
}

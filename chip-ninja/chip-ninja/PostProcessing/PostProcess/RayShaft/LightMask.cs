﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace chip_ninja.Graphics.PostProcess.RayShaft
{
    public class LighteMask : BasePostProcess
    {
        public Texture lightTex;
        public Vector2 lightPosOnScreen;
        public string lightSourceAsset;
        public float sunSize = 1500;
        public Vector2 screenRatio;

        public EffectTechnique _currentTech;
        public EffectParameter _screenRatio;
        public EffectParameter _lightTex;
        public EffectParameter _sunSize;
        public EffectParameter _lightPosOnScreen;

        public LighteMask(Game game, Vector2 sourcePos, string lightSourceAsset, float sunSize, Vector2 screenRatio)
            : base(game)
        {
            lightPosOnScreen = sourcePos;
            this.lightSourceAsset = lightSourceAsset;
            this.sunSize = sunSize;
            this.screenRatio = screenRatio;
            
            if (effect == null)
            {
                effect = game.Content.Load<Effect>("Shaders/PostProcessing/LightMask");
                lightTex = game.Content.Load<Texture2D>(lightSourceAsset);
            }
        }

        public override void ExctractParameters()
        {
            _currentTech = effect.Techniques["LightMask"];

            _screenRatio = effect.Parameters["screenRatio"];
            _lightTex = effect.Parameters["lightTex"];
            _sunSize = effect.Parameters["sunSize"];
            _lightPosOnScreen = effect.Parameters["lightPosOnScreen"];

            base.ExctractParameters();
        }

        public override void Draw(GameTime gameTime)
        {
            effect.CurrentTechnique = _currentTech;

            _screenRatio.SetValue(screenRatio);

            _lightTex.SetValue(lightTex);

            _sunSize.SetValue(sunSize);
            _lightPosOnScreen.SetValue(lightPosOnScreen);

            base.Draw(gameTime);

        }
    }
}

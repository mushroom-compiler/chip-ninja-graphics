﻿using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Graphics.PostProcess
{
    public class PostProcessManager
    {
        protected Game game;
        public Texture2D scene;

        public RenderTarget2D newScene;

        protected List<BasePostProcessEffect> basePostProcessEffects = new List<BasePostProcessEffect>();
        protected int basePostProcessCount;

        public PostProcessManager(Game game)
        {
            this.game = game;
        }

        public void AddEffect(BasePostProcessEffect ppEfect)
        {
            basePostProcessEffects.Add(ppEfect);
        }

        public virtual void ExtractParameters()
        {
            basePostProcessCount = basePostProcessEffects.Count;

            for (int i = 0; i < basePostProcessCount; i++)
            {
                var ppe = basePostProcessEffects[i];
                if (ppe.Enabled)
                    ppe.ExtractParameters();
            }
        }

        public virtual void Draw(GameTime gameTime, Texture2D scene)
        {

            this.scene = scene;

            for (int i = 0; i < basePostProcessCount; i++)
            {
                var ppe = basePostProcessEffects[i];
                if (ppe.Enabled)
                {
                    ppe.orgScene = scene;
                    ppe.Draw(gameTime, this.scene);
                    this.scene = ppe.lastScene;
                }
            }

        }

    }
}

﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Graphics.PostProcess
{
    public class ScreenVertices
    {
        VertexPositionTexture[] corners;
        VertexBuffer VBO_VPCT;


        VertexBuffer VBO_VPT;
        VertexPositionColorTexture[] vertices;

        short[] IBO_VPT, IBO_VPCT;

        Game Game;

        public VertexPositionColorTexture[] Vertices
        {
            get { return vertices; }
            set { vertices = value; }
        }

        public VertexBuffer VBO_VPCT_
        {
            get { return VBO_VPCT; }
            set { VBO_VPCT = value; }
        }

        public ScreenVertices(Game game)
        {
            Game = game;

            corners = new VertexPositionTexture[4];
            corners[0].Position = new Vector3(0, 0, 0);
            corners[0].TextureCoordinate = Vector2.Zero;

            vertices = new VertexPositionColorTexture[4];
            vertices[0].Position = new Vector3(0, 0, 0);
            vertices[0].TextureCoordinate = Vector2.Zero;
        }

        public virtual void Initialize_VPCT()
        {
            vertices = new VertexPositionColorTexture[]
                    {
                        new VertexPositionColorTexture(new Vector3(-1, 1, 0), Color.White, new Vector2(0, 0)),
                        new VertexPositionColorTexture(new Vector3(1, 1, 0), Color.White, new Vector2(1, 0)),
                        new VertexPositionColorTexture(new Vector3(-1, -1, 0), Color.White, new Vector2(0, 1)),
                        new VertexPositionColorTexture(new Vector3(1, -1, 0), Color.White, new Vector2(1, 1))
                    };

            IBO_VPCT = new short[] { 0, 1, 2, 2, 3, 0 };
            VBO_VPCT = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPositionColorTexture), vertices.Length, BufferUsage.None);
            VBO_VPCT.SetData(vertices);
        }

        public virtual void Initialize_VPT()
        {
            corners = new VertexPositionTexture[]
                    {
                        new VertexPositionTexture(
                            new Vector3(1,-1,0),
                            new Vector2(1,1)),
                        new VertexPositionTexture(
                            new Vector3(-1,-1,0),
                            new Vector2(0,1)),
                        new VertexPositionTexture(
                            new Vector3(-1,1,0),
                            new Vector2(0,0)),
                        new VertexPositionTexture(
                            new Vector3(1,1,0),
                            new Vector2(1,0))
                    };

            IBO_VPT = new short[] { 0, 1, 2, 2, 3, 0 };
            VBO_VPT = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPositionTexture), corners.Length, BufferUsage.None);
            VBO_VPT.SetData(corners);
        }

        public virtual void Draw_VPCT()
        {
            Game.GraphicsDevice.SetVertexBuffer(VBO_VPCT);
            Game.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColorTexture>(PrimitiveType.TriangleList, vertices, 0, 4, IBO_VPCT, 0, 2);
        }

        public virtual void Draw_VPT()
        {
            Game.GraphicsDevice.SetVertexBuffer(VBO_VPT);
            Game.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionTexture>(PrimitiveType.TriangleList, corners, 0, 4, IBO_VPT, 0, 2);
        }
    }
}

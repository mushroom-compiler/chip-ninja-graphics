﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Graphics.PostProcess
{
    public class BasePostProcess
    {
        public Texture2D backBuffer;
        public Texture2D orgBuffer;

        public bool Enabled = true;
        protected Effect effect;

        protected Game game;
        public RenderTarget2D newScene;

        ScreenVertices sq;

        protected SpriteBatch spriteBatch
        {
            get { return (SpriteBatch)game.Services.GetService(typeof(SpriteBatch)); }
        }

        public BasePostProcess(Game game)
        {
            this.game = game;
        }

        public virtual void ExctractParameters()
        {
        }

        public virtual void Draw(GameTime gameTime)
        {
            if (Enabled)
            {
                if (sq == null)
                {
                    sq = new ScreenVertices(game);
                    sq.Initialize_VPT();
                }

                effect.CurrentTechnique.Passes[0].Apply();
                sq.Draw_VPT();
            }
        }
    }
}

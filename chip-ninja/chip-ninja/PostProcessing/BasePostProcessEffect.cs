﻿using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace chip_ninja.Graphics.PostProcess
{
    public class BasePostProcessEffect
    {
        protected List<BasePostProcess> postProcesses = new List<BasePostProcess>();
        public Texture2D lastScene;
        public Texture2D orgScene;

        protected Game game;

        protected int processesCount;

        public BasePostProcessEffect(Game game)
        {
            this.game = game;
        }

        public bool Enabled = true;

        public void AddPostProcess(BasePostProcess postProcess)
        {
            postProcesses.Add(postProcess);
        }

        public virtual void ExtractParameters()
        {
            processesCount = postProcesses.Count;

            for (int p = 0; p < processesCount; p++)
                if (postProcesses[p].Enabled)
                    postProcesses[p].ExctractParameters();
        }

        public virtual void Draw(GameTime gameTime, Texture2D scene)
        {
            if (!Enabled)
                return;

            orgScene = scene;

            lastScene = null;

            for (int p = 0; p < processesCount; p++)
            {
                if (postProcesses[p].Enabled)
                {
                    postProcesses[p].orgBuffer = orgScene;

                    if (postProcesses[p].newScene == null)
                        postProcesses[p].newScene = new RenderTarget2D(this.game.GraphicsDevice, this.game.GraphicsDevice.Viewport.Width / 2, this.game.GraphicsDevice.Viewport.Height / 2, false, SurfaceFormat.Color, DepthFormat.None);

                    this.game.GraphicsDevice.SetRenderTarget(postProcesses[p].newScene);

                    if (lastScene == null)
                        lastScene = orgScene;

                    postProcesses[p].backBuffer = lastScene;

                    this.game.GraphicsDevice.Textures[0] = postProcesses[p].backBuffer;
                    postProcesses[p].Draw(gameTime);

                    this.game.GraphicsDevice.SetRenderTarget(null);

                    lastScene = postProcesses[p].newScene;
                }
            }

            if (lastScene == null)
                lastScene = scene;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using chip_ninja.Graphics.PostProcess;

namespace chip_ninja.Graphics.PostProcess.DepthOfField
{
    public class DepthOfField : BasePostProcess
    {
        public Effect DoF_effect;
        EffectTechnique _DoF_Technique;
        EffectParameter _DoF_BlurDistance;
        EffectParameter _DoF_ColorMap;

        public Texture2D tex;
        public Vector2 texCoords;

        chip_ninja game;
        public DepthOfField(Game game) 
            : base(game)
        {
            this.game = (chip_ninja)game;
        }

        public void DefineTex(Texture2D tex, Vector2 texCoords)
        {
            if (tex != null)
            {
                this.tex = tex;
                this.texCoords = texCoords;
            }
        }

        public override void ExctractParameters()
        {
            if (DoF_effect == null)
                DoF_effect = game.Content.Load<Effect>("Shaders/PostProcessing/Blur_DoF");

            //DoF
            _DoF_Technique = DoF_effect.Techniques["DoF"];
            _DoF_BlurDistance = DoF_effect.Parameters["BlurDistance"];
            _DoF_ColorMap = DoF_effect.Parameters["ColorMap"];

            base.ExctractParameters();
        }

        public new void Draw()
        {
            GenerateDoF();
        }

        void GenerateDoF()
        {
            DoF_effect.CurrentTechnique = _DoF_Technique;

            _DoF_BlurDistance.SetValue(0.003f);
            _DoF_ColorMap.SetValue(tex);
        }
    }
}

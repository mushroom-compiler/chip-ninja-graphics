﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace chip_ninja.Graphics.PostProcess.NormalMapping
{
    class NormalMapping : BasePostProcess
    {
        private Effect _multiTarget_effect;
        private EffectTechnique _multiTarget_Technique;
        private EffectParameter _multiTarget_ScreenWidth;
        private EffectParameter _multiTarget_ScreenHeight;
        private EffectParameter _multiTarget_AmbiantColor;
        private EffectParameter _multiTarget_Strength;
        private EffectParameter _multiTarget_Position;
        private EffectParameter _multiTarget_LightDecay;
        //private EffectParameter _multiTarget_ConeDirection;
        private EffectParameter _multiTarget_LightColor;
        private EffectParameter _multiTarget_SpecularStrength;
        private EffectParameter _multiTarget_ColorMap;
        private EffectParameter _multiTarget_NormapMap;

        private Effect _deferredCombined_effect;
        private EffectTechnique _deferredCombined_Technique;
        private EffectParameter _deferredCombined_Ambient;
        private EffectParameter _deferredCombined_AmbientLight;
        private EffectParameter _deferredCombined_AmbientColor;
        private EffectParameter _deferredCombined_ColorMap;
        private EffectParameter _deferredCombined_ShadowMap;
        private EffectParameter _deferredCombined_NormalMap;
        private EffectParameter _multiTarget_HighQuality;

        private RenderTarget2D ColorMap;
        private RenderTarget2D ShadowMap;
        private RenderTarget2D NormalMap;

        PresentationParameters pp;
        int width;
        int height;

        GraphicsDevice graphicsDevice;
        new SpriteBatch spriteBatch;

        //ScreenVertices screenVertices;

        public float specularStrength = 1.0f;
        public Color ambientLight = new Color(.1f, .1f, .1f, 1);

        public List<Light> lights = new List<Light>();
        public List<Texture2D> texs = new List<Texture2D>();
        public List<Texture2D> normals = new List<Texture2D>();
        public List<Vector2> offsets = new List<Vector2>();

        public VertexBuffer VertexBuffer;
        public VertexPositionColorTexture[] Vertices;

        chip_ninja game;

        public NormalMapping(Game game, SpriteBatch sB, ContentManager Content, Map map, string deferredCombined = "Shaders/PostProcessing/DeferredCombined", string multiTarget = "Shaders/PostProcessing/MultiTarget")
            : base(game)
        {
            this.game = (chip_ninja)game;

            graphicsDevice = game.GraphicsDevice;
            spriteBatch = sB;

            pp = game.GraphicsDevice.PresentationParameters;
            width = pp.BackBufferWidth;
            height = pp.BackBufferHeight;

            ColorMap = new RenderTarget2D(game.GraphicsDevice, width, height);
            ShadowMap = new RenderTarget2D(game.GraphicsDevice, width, height);
            NormalMap = new RenderTarget2D(game.GraphicsDevice, width, height, false, pp.BackBufferFormat, pp.DepthStencilFormat, pp.MultiSampleCount, RenderTargetUsage.DiscardContents);

            Vertices = new VertexPositionColorTexture[4];
            Vertices[0] = new VertexPositionColorTexture(new Vector3(-1, 1, 0), Color.White, new Vector2(0, 0));
            Vertices[1] = new VertexPositionColorTexture(new Vector3(1, 1, 0), Color.White, new Vector2(1, 0));
            Vertices[2] = new VertexPositionColorTexture(new Vector3(-1, -1, 0), Color.White, new Vector2(0, 1));
            Vertices[3] = new VertexPositionColorTexture(new Vector3(1, -1, 0), Color.White, new Vector2(1, 1));

            VertexBuffer = new VertexBuffer(graphicsDevice, typeof(VertexPositionColorTexture), Vertices.Length, BufferUsage.None);
            VertexBuffer.SetData(Vertices);

            normals.Add(null);

            //screenVertices = new ScreenVertices(game);
            //screenVertices.Initialize_VPCT();
            //screenVertices.Initialize_VPT();
        }

        public void addTex(Texture2D colorMap, Texture2D normalMap)
        {
            if (colorMap != null && normalMap != null)
            {
                texs.Add(colorMap);
                normals.Add(normalMap);
            }
        }

        public void addLight(Light light)
        {
            if (light != null)
                lights.Add(light);
        }

        public void addOffset(Vector2 offset)
        {
            if (offset != null)
                offsets.Add(offset);
        }

        public override void ExctractParameters()
        {
            if (_multiTarget_effect == null && _deferredCombined_effect == null)
            {
                _multiTarget_effect = game.Content.Load<Effect>("Shaders/PostProcessing/MultiTarget");
                _deferredCombined_effect = game.Content.Load<Effect>("Shaders/PostProcessing/DeferredCombined");
            }

            //MultiTarget
            _multiTarget_Technique = _multiTarget_effect.Techniques["DeferredPointLight"];
            _multiTarget_ScreenWidth = _multiTarget_effect.Parameters["screenWidth"];
            _multiTarget_ScreenHeight = _multiTarget_effect.Parameters["screenHeight"];
            _multiTarget_AmbiantColor = _multiTarget_effect.Parameters["ambientColor"];
            _multiTarget_Strength = _multiTarget_effect.Parameters["lightStrength"];
            _multiTarget_LightDecay = _multiTarget_effect.Parameters["lightDecay"];
            _multiTarget_Position = _multiTarget_effect.Parameters["lightPosition"];
            // _multiTarget_ConeDirection
            _multiTarget_LightColor = _multiTarget_effect.Parameters["lightColor"];
            _multiTarget_SpecularStrength = _multiTarget_effect.Parameters["specularStrength"];
            _multiTarget_ColorMap = _multiTarget_effect.Parameters["ColorMap"];
            _multiTarget_NormapMap = _multiTarget_effect.Parameters["NormalMap"];
            _multiTarget_HighQuality = _multiTarget_effect.Parameters["highQuality"];

            //DeferredCombined
            _deferredCombined_Technique = _deferredCombined_effect.Techniques["DeferredCombined"];
            _deferredCombined_Ambient = _deferredCombined_effect.Parameters["ambient"];
            _deferredCombined_AmbientLight = _deferredCombined_effect.Parameters["ambientLight"];
            _deferredCombined_AmbientColor = _deferredCombined_effect.Parameters["ambientColor"];
            _deferredCombined_ColorMap = _deferredCombined_effect.Parameters["ColorMap"];
            _deferredCombined_ShadowMap = _deferredCombined_effect.Parameters["ShadowMap"];
            _deferredCombined_NormalMap = _deferredCombined_effect.Parameters["NormalMap"];

            base.ExctractParameters();
        }

        public new void Draw(GameTime gameTime)
        {
            graphicsDevice.Clear(Color.Black);

            graphicsDevice.SetRenderTarget(ColorMap);
            graphicsDevice.Clear(Color.Black);

            for (int i = 0; i < texs.Count; i++)
                DrawColorMap(texs[i], offsets[i]);

            Switch_RT(NormalMap);
            //graphicsDevice.SetRenderTarget(null);
            //graphicsDevice.SetRenderTarget(NormalMap);

            for (int i = 1; i < normals.Count; i++)
                DrawNormalMap(normals[i], offsets[i]);

            Switch_RT(ShadowMap);
            //graphicsDevice.SetRenderTarget(null);

            GenerateShadowMap();
            
            graphicsDevice.SetRenderTarget(null);

            graphicsDevice.Clear(Color.Black);

            DrawCombinedMaps();
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            spriteBatch.Draw(ShadowMap, Vector2.Zero, Color.White);
            spriteBatch.End();

            //base.Draw(gameTime);
        }

        public void Update()
        {
            //if (Keyboard.GetState().IsKeyDown(Keys.Q)) lights[0].Color.X -= .01f;
            //if (Keyboard.GetState().IsKeyDown(Keys.W)) lights[0].Color.X += .01f;
            //if (Keyboard.GetState().IsKeyDown(Keys.A)) lights[0].Color.Y -= .01f;
            //if (Keyboard.GetState().IsKeyDown(Keys.S)) lights[0].Color.Y += .01f;
            //if (Keyboard.GetState().IsKeyDown(Keys.Z)) lights[0].Color.Z -= .01f;
            //if (Keyboard.GetState().IsKeyDown(Keys.X)) lights[0].Color.Z += .01f;

            //if (Keyboard.GetState().IsKeyDown(Keys.E)) lights[0].Decay -= 1;
            //if (Keyboard.GetState().IsKeyDown(Keys.R)) lights[0].Decay += 1;

            //if (Keyboard.GetState().IsKeyDown(Keys.D)) lights[0].Power -= .1f;
            //if (Keyboard.GetState().IsKeyDown(Keys.F)) lights[0].Power += .1f;

            //if (Keyboard.GetState().IsKeyDown(Keys.C)) lights[0].Position = new Vector3(lights[0].Position.X, lights[0].Position.Y, lights[0].Position.Z - 1);
            //if (Keyboard.GetState().IsKeyDown(Keys.V)) lights[0].Position = new Vector3(lights[0].Position.X, lights[0].Position.Y, lights[0].Position.Z + 1);

            //if (Keyboard.GetState().IsKeyDown(Keys.T)) ambientLight = new Color((byte)(ambientLight.R - 1), (byte)(ambientLight.G - 1), (byte)(ambientLight.B - 1));
            //if (Keyboard.GetState().IsKeyDown(Keys.Y)) ambientLight = new Color((byte)(ambientLight.R + 1), (byte)(ambientLight.G + 1), (byte)(ambientLight.B + 1));

            //if (Keyboard.GetState().IsKeyDown(Keys.D1)) lights[0].IsEnabled = !lights[0].IsEnabled;

            //if (Keyboard.GetState().IsKeyDown(Keys.G)) specularStrength -= .01f;
            //if (Keyboard.GetState().IsKeyDown(Keys.H)) specularStrength += .01f;

            //lights[0].Position = new Vector3(Mouse.GetState().X, Mouse.GetState().Y, lights[0].Position.Z);

            //if (Keyboard.GetState().IsKeyDown(Keys.L))
            //{
            //    Random rand = new Random();
            //    lights.Add(new Light()
            //    {
            //        IsEnabled = true,
            //        Color = new Vector4((float)rand.NextDouble(), (float)rand.NextDouble(), (float)rand.NextDouble(), 1.0f),
            //        Power = (float)rand.Next(1, 10) * .1f,
            //        Decay = rand.Next(100, 400),
            //        Position = new Vector3(
            //            rand.Next(0, pp.BackBufferWidth),
            //            rand.Next(0, pp.BackBufferHeight),
            //            80)
            //    });
            //}

            //Console.WriteLine(lights.Count);
        }

        private void Switch_RT(RenderTarget2D RT)
        {
            graphicsDevice.SetRenderTarget(null);
            graphicsDevice.SetRenderTarget(RT);

            graphicsDevice.Clear(Color.Black);
        }

        private void DrawColorMap(Texture2D colorMap, Vector2 offset)
        {
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive);
            spriteBatch.Draw(colorMap, offset, Color.White);
            spriteBatch.End();
        }

        //float offset = 300f;
        private void DrawNormalMap(Texture2D normalMap, Vector2 offset)
        {
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive);
            spriteBatch.Draw(normalMap, offset, Color.White);
            spriteBatch.End();
        }

        private void GenerateShadowMap()
        {
            //graphicsDevice.SetRenderTarget(ShadowMap);
            //graphicsDevice.Clear(Color.Transparent);

            foreach (var light in lights)
            {
                if (!light.IsEnabled) continue;

                //graphicDevice.SetVertexBuffer(screenVertices.VBO_VPCT_);

                graphicsDevice.SetVertexBuffer(VertexBuffer);

                _multiTarget_effect.CurrentTechnique = _multiTarget_Technique;

                _multiTarget_HighQuality.SetValue(game.effetcs);
                _multiTarget_Strength.SetValue(light.Power);
                _multiTarget_Position.SetValue(light.Position);
                _multiTarget_LightColor.SetValue(light.Color);
                _multiTarget_LightDecay.SetValue(light.Decay);
                _multiTarget_SpecularStrength.SetValue(specularStrength);

                _multiTarget_ScreenWidth.SetValue(graphicsDevice.Viewport.Width);
                _multiTarget_ScreenHeight.SetValue(graphicsDevice.Viewport.Height);
                _multiTarget_AmbiantColor.SetValue(ambientLight.ToVector4());
                _multiTarget_NormapMap.SetValue(NormalMap);
                _multiTarget_ColorMap.SetValue(ColorMap);
                _multiTarget_effect.CurrentTechnique.Passes[0].Apply();

                graphicsDevice.BlendState = BlendState.NonPremultiplied;

                graphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, Vertices, 0, 2);

                //graphicDevice.DrawUserIndexedPrimitives<VertexPositionColorTexture>(PrimitiveType.TriangleList, screenVertices.Vertices, 0, 2);
                //screenVertices.Draw_VPCT();
            }


            //return ShadowMap;
        }

        public static BlendState BlendBlack = new BlendState()
        {
            ColorBlendFunction = BlendFunction.Add,
            ColorSourceBlend = Blend.One,
            ColorDestinationBlend = Blend.One,

            AlphaBlendFunction = BlendFunction.Add,
            AlphaSourceBlend = Blend.SourceAlpha,
            AlphaDestinationBlend = Blend.One
        };

        private void DrawCombinedMaps()
        {
            _deferredCombined_effect.CurrentTechnique = _deferredCombined_Technique;

            _deferredCombined_Ambient.SetValue(1f);
            _deferredCombined_AmbientLight.SetValue(4);
            _deferredCombined_AmbientColor.SetValue(ambientLight.ToVector4());
            _deferredCombined_ColorMap.SetValue(ColorMap);
            _deferredCombined_ShadowMap.SetValue(ShadowMap);
            _deferredCombined_NormalMap.SetValue(NormalMap);
            _deferredCombined_effect.CurrentTechnique.Passes[0].Apply();

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            spriteBatch.Draw(ShadowMap, Vector2.Zero, Color.White);
            spriteBatch.End();
            //DEBUG
            //spriteBatch.Begin();
            //spriteBatch.Draw(ShadowMap, new Rectangle(0, 0, 200, 120), Color.White);
            //spriteBatch.Draw(NormalMap, new Rectangle(130, 0, 200, 120), Color.White);
            //spriteBatch.Draw(ColorMap, new Rectangle(260, 0, 200, 120), Color.White);
            //spriteBatch.End();
        }

    }
}

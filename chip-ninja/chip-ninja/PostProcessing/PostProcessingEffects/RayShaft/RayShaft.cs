﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace chip_ninja.Graphics.PostProcess.RayShaft
{
    public class RayShaft : BasePostProcessEffect
    {
        public LighteMask mask;
        public LightRay rays;
        public Vector2 screenRatio;
        public chip_ninja game;

        float speed = MathHelper.PiOver2, radius = 300f;
        Vector2 origin = Vector2.Zero;

        public RayShaft(Game game, Vector2 lightScreenSourcePos, string lightSourceImage, float lightSourceSize, float density, float decay, float weight, float exposure, int nbsamples, Vector2 screenRatio)
            : base(game)
        {
            mask = new LighteMask(game, lightScreenSourcePos, lightSourceImage, lightSourceSize, screenRatio);
            rays = new LightRay(game, lightScreenSourcePos, density, decay, weight, exposure, nbsamples);
            AddPostProcess(mask);
            AddPostProcess(rays);
            this.game = (chip_ninja)game;
        }

        public Vector2 lightPosOnScreen
        {
            set
            {
                mask.lightPosOnScreen = value;
                rays.lightPosOnScreen = value;
            }
            get
            {
                return rays.lightPosOnScreen;
            }
        }

        public Texture lightTex
        {
            get { return mask.lightTex; }
            set { mask.lightTex = value; }
        }

        public float SunSize
        {
            set { mask.sunSize = value; }
            get { return mask.sunSize; }
        }

        public float Density
        {
            get { return rays.Density; }
            set { rays.Density = value; }
        }

        public float Decay
        {
            get { return rays.Decay; }
            set { rays.Decay = value; }
        }

        public float Weight
        {
            get { return rays.Weight; }
            set { rays.Weight = value; }
        }

        public float Exposure
        {
            get { return rays.Exposure; }
            set { rays.Exposure = value; }
        }

        public int Nbsamples
        {
            get { return rays.nbSamples; }
            set { rays.nbSamples = value; }
        }

        public void Update(ContentManager Content, float time, Rectangle screenRect)
        {
            //lightPosOnScreen = new Vector2((float)(Math.Cos(time * speed) * radius + origin.X) / screenRect.Width + 0.5f, (float)(Math.Sin(time * speed) * radius + origin.Y) / screenRect.Height + 0.5f);

            if (time >= 4.1f)
                time = 1.8F;

            //Console.WriteLine("time : " + time);
            //Console.WriteLine("Sun Pos : " + lightPosOnScreen);

            //if (Keyboard.GetState().IsKeyDown(Keys.F1))
            //    mask.lightTex = Content.Load<Texture2D>("Textures/flare");
            //if (Keyboard.GetState().IsKeyDown(Keys.F2))
            //    mask.lightTex = Content.Load<Texture2D>("Textures/moon1");
            //if (Keyboard.GetState().IsKeyDown(Keys.F3))
            //    mask.lightTex = Content.Load<Texture2D>("Textures/flare3");
            //if (Keyboard.GetState().IsKeyDown(Keys.F4))
            //    mask.lightTex = Content.Load<Texture2D>("Textures/flare4");
            //if (Keyboard.GetState().IsKeyDown(Keys.F5))
            //    mask.lightTex = Content.Load<Texture2D>("Textures/flare6");
            //if (Keyboard.GetState().IsKeyDown(Keys.F6))
            //    mask.lightTex = Content.Load<Texture2D>("Textures/test_lense");

            //if (Keyboard.GetState().IsKeyDown(Keys.Z))
            //    this.lightPosOnScreen = new Vector2(lightPosOnScreen.X, lightPosOnScreen.Y - .01f);
            //if (Keyboard.GetState().IsKeyDown(Keys.S))
            //    lightPosOnScreen = new Vector2(lightPosOnScreen.X, lightPosOnScreen.Y + .01f);
            //if (Keyboard.GetState().IsKeyDown(Keys.Q))
            //    lightPosOnScreen = new Vector2(lightPosOnScreen.X - .01f, lightPosOnScreen.Y);
            //if (Keyboard.GetState().IsKeyDown(Keys.D))
            //    lightPosOnScreen = new Vector2(lightPosOnScreen.X + .01f, lightPosOnScreen.Y);

            //if (Keyboard.GetState().IsKeyDown(Keys.A))
            //    this.Exposure += .01f;
            //if (Keyboard.GetState().IsKeyDown(Keys.W))
            //    this.Exposure -= .01f;

            //if (Keyboard.GetState().IsKeyDown(Keys.E))
            //    this.SunSize += .01f;
            //if (Keyboard.GetState().IsKeyDown(Keys.C))
            //    this.SunSize -= .01f;

            if (Keyboard.GetState().IsKeyDown(Keys.R) && game.stateManager.CurrentState == game.enemyCuttingScreen || game.stateManager.CurrentState == game.enemyCuttingScreen)
                this.Density += .01f;
            //if (Keyboard.GetState().IsKeyDown(Keys.V))
            //    this.Density -= .01f;

            //if (Keyboard.GetState().IsKeyDown(Keys.T))
            //    this.Decay += .01f;
            //if (Keyboard.GetState().IsKeyDown(Keys.B))
            //    this.Decay -= .01f;

            if (/*Keyboard.GetState().IsKeyDown(Keys.Y) ||*/ game.stateManager.CurrentState == game.enemyCuttingScreen)
                this.Weight += .01f;
            //if (Keyboard.GetState().IsKeyDown(Keys.N))
            //    this.Weight -= .01f;
        }


    }
}

﻿using Microsoft.Xna.Framework;

namespace chip_ninja
{
    abstract public class Entity
    {
        protected Vector2 position;

        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public Entity()
        {
            position = Vector2.Zero;
        }

        public abstract void Update(GameTime gameTime);
    }
}

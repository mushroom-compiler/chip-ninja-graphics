﻿using Microsoft.Xna.Framework;

namespace chip_ninja
{
    public class Camera
    {
        public Matrix matrix;
        Vector2 offset;
        Vector2 lastPlayerPos;
        float lol;

        public Matrix Matrix
        {
            get { return matrix; }
        }

        public Vector2 Offset
        {
            get { return offset; }
            set { offset = value; }
        }

        public Camera(Vector2 position)
        {
            matrix = Matrix.Identity;
            matrix.Translation = new Vector3(-position, 0);
            offset = Vector2.Zero;
            lastPlayerPos = Vector2.Zero;
        }

        public void update(Player player)
        {
            offset = new Vector2(matrix.Translation.X, matrix.Translation.Y) + player.Position;

            Vector2 playerPos;
            if (player.Velocity.X < 0)
            {
                if (offset.X > 600)
                {
                    playerPos = new Vector2(lastPlayerPos.X, player.Position.Y);
                }
                else
                {
                    if (player.Position.X > 600)
                    {
                        playerPos = player.Position + new Vector2(-600, 0);
                        lastPlayerPos = playerPos;
                    }
                    else playerPos = new Vector2(0, player.Position.Y);
                }
            }
            else
            {
                if (offset.X < 1100)
                {
                    playerPos = new Vector2(lastPlayerPos.X, player.Position.Y);
                }
                else
                {
                    playerPos = player.Position + new Vector2(-1100, 0);
                    lastPlayerPos = playerPos;
                }
            }

            /*
            offset = offset > 400 ? 400 : offset;

            offset = offset < 0 ? 0 : offset;

            if (offset >= 400 || offset <= 0)
            {
                matrix.Translation = new Vector3(-player.Movement + new Vector2(offset, 0), 0); //We apply the translation matrix to make the view move.
                lol = offset;
            }
            matrix.Translation = new Vector3(-player.Movement + new Vector2(offset, + 500), 0);
            lol = 0;
             * */

            //playerPos = (player.Position.X <  400) ? new Vector2(0, player.Position.Y) : player.Position + new Vector2(- 400, 0);

            matrix.Translation = new Vector3(-playerPos + new Vector2(0, 700), 0);
        }

    }
}

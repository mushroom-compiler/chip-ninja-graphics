using System;
using System.Net.Sockets;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

using chip_ninja.Components;
using chip_ninja.GameScreens;
using chip_ninja.GameScreens.Menu;

namespace chip_ninja
{
    public class chip_ninja : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public InputHandler input;
        public GameStateManager stateManager;
        public TitleScreen titleScreen;
        public OptionsScreen optionsScreen;
        public MutliplayerTitleScreen multiplayerTitleScreen;
        public GameScreen gameScreen;
        public MultiplayerScreen multiplayerScreen;
        public EnemyCuttingScreen enemyCuttingScreen;
        public PauseScreen pauseScreen;
        public GameOverScreen gameOverScreen;

        public Player player;

        public Player player1;
        public Player player2;
        public Song menuSong;
        public Song gameSong;

        //public Server server;
        //public Client client;
        //public TcpClient client;

        public bool bplayer1 = true;
        public bool effetcs = true;

        const int screenWidth = 1600;
        const int screenHeight = 1000;

        public readonly Rectangle screenRectangle;

        //Pause saves

        //

        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        public string graphicsSettings = "High";

        public chip_ninja()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = false;
            //Here, We make the window borderless

            IntPtr windowHandle = this.Window.Handle;
            var control = Control.FromHandle(windowHandle);
            var form = control.FindForm();
            form.FormBorderStyle = FormBorderStyle.None;

            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;

            //graphics.IsFullScreen = true;

            stateManager = new GameStateManager(this);
            titleScreen = new TitleScreen(this, stateManager);
            gameScreen = new GameScreen(this, stateManager);
            optionsScreen = new OptionsScreen(this, stateManager);
            multiplayerScreen = new MultiplayerScreen(this, stateManager);
            multiplayerTitleScreen = new MutliplayerTitleScreen(this, stateManager);
            enemyCuttingScreen = new EnemyCuttingScreen(this, stateManager, null);
            pauseScreen = new PauseScreen(this, stateManager);
            gameOverScreen = new GameOverScreen(this, stateManager);

            screenRectangle = new Rectangle(
                0,
                0,
                screenWidth,
                screenHeight);

            Components.Add(stateManager);
            //Components.Add(new InputHandler(this));
            input = new InputHandler(this);
            stateManager.ChangeState(titleScreen);
        }

        protected override void Initialize()
        {
            menuSong = Content.Load<Song>(@"Songs\sadRobot");
            gameSong = Content.Load<Song>(@"Songs\sky");
            MediaPlayer.IsMuted = true;

            //client = new TcpClient();
            //client.NoDelay = true;
            //client.Connect(Console.ReadLine(), 4242);

            //client.

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //player = new Player();
            //MediaPlayer.IsMuted = true;
            //player.LoadContent(Content);
            //enemy.objTex = Content.Load<Texture2D>(@"Textures\particule_a_circle");

            player1 = new Player(Vector2.Zero);
            player2 = new Player(Vector2.Zero);
            player1.LoadContent(Content, "perso2", 1, 1);
            player2.LoadContent(Content, "perso3", 1, 1);
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (stateManager.CurrentState.ToString() == "chip_ninja.GameScreens.MultiplayerScreen")
            {
                if (bplayer1)
                    input.Update(gameTime, player1);
                else
                    input.Update(gameTime, player2);
            }

            else input.Update(gameTime, player);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            //Console.WriteLine(1 / (float)gameTime.ElapsedGameTime.TotalSeconds);

            base.Draw(gameTime);
        }
    }
}
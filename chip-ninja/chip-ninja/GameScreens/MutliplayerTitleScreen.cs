﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

using System;

using chip_ninja.Components;
using chip_ninja.GameScreens.Menu;

namespace chip_ninja.GameScreens
{
    public class MutliplayerTitleScreen : BaseGameState
    {
        Texture2D backgroundImage, textBoxTex;

        SpriteFont hell, hell_T;
        public List<GameScreens.Menu.Button> buttons;
        public GameScreens.Menu.Cursor cursor;
        public List<GameScreens.Menu.Box> boxes;

        int timeSpent;

        public MutliplayerTitleScreen(chip_ninja game, GameStateManager manager)
            : base(game, manager)
        {
        }

        protected override void LoadContent()
        {
            ContentManager Content = game.Content;
            backgroundImage = Content.Load<Texture2D>(@"Backgrounds\titlescreen");
            textBoxTex = Content.Load<Texture2D>(@"textbox");

            hell = Content.Load<SpriteFont>(@"SpriteFonts/hell");
            hell_T = Content.Load<SpriteFont>(@"SpriteFonts/hell-T");

            boxes = new List<Box>();
            buttons = new List<GameScreens.Menu.Button>();
            cursor = new GameScreens.Menu.Cursor(Content.Load<Texture2D>("cursor"));

            //cursor = new Cursor(Content.Load<Texture2D>("cursor"));

            //buttons.Add(new Button("Option", game.titleScreen));
            //buttons.Add(new Button("Exit game", game.titleScreen));

            boxes.Add(new Box(textBoxTex));
            buttons.Add(new Button("Client", game.multiplayerScreen));
            buttons[0].name = "Client";
            buttons.Add(new Button("Server", game.multiplayerScreen));
            buttons[1].name = "Server";

            for (int i = 0; i < buttons.Count; i++)
                cursor.buttonsPos.Add(new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString(buttons[i].text).X / 2, game.screenRectangle.Height * i / 22 + 800));

            for (int i = 0; i < boxes.Count; i++)
                cursor.boxesPos.Add(new Vector2(game.screenRectangle.Width / 2 - boxes[i].tex.Width / 3.5f, game.screenRectangle.Height * i / 12 + 700));

            base.LoadContent();

        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            //game.player.Position = Vector2.Zero;
            //game.player.Velocity = Vector2.Zero;

            boxes[0].text = game.input.endText;

            timeSpent += gameTime.ElapsedGameTime.Milliseconds;
        }

        public override void Draw(GameTime gameTime)
        {
            game.SpriteBatch.Begin();

            base.Draw(gameTime);

            game.SpriteBatch.Draw(backgroundImage, game.screenRectangle, Color.White);

            for (int i = 0; i < buttons.Count; i++)
                buttons[i].Draw(game.SpriteBatch, hell, cursor.buttonsPos[i]);

            for (int i = 0; i < boxes.Count; i++)
                boxes[i].Draw(game.SpriteBatch, hell, cursor.boxesPos[i], new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString(game.input.endText).X / 2, game.screenRectangle.Height * i / 8 + 730));

            if (cursor.index == 0)
            game.SpriteBatch.DrawString(hell, "Format : Player_number  (space)  IP", new Vector2(cursor.boxesPos[0].X - hell.MeasureString("Format : Player_number  (space)  IP").X / 5, cursor.boxesPos[0].Y - 50), Color.FromNonPremultiplied(250, 250, 250, 255));
            else  
            game.SpriteBatch.DrawString(hell, "Format : Player_number", new Vector2(cursor.boxesPos[0].X - hell.MeasureString("Format : Player_number").X / 30 + 7, cursor.boxesPos[0].Y - 50), Color.FromNonPremultiplied(250, 250, 250, 255));

            cursor.Draw(game.SpriteBatch, cursor.buttonsPos[cursor.index]);
            game.SpriteBatch.DrawString(hell_T, "Chip Ninja", new Vector2(game.screenRectangle.Width / 2 - hell_T.MeasureString("Chip Nina").X / 2, 100), Color.FromNonPremultiplied(250, 250, 250, 255));

            //if (((timeSpent / 1000) % 2) == 1)
            //    game.SpriteBatch.DrawString(hell, "Press Enter to Continue", new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString("Press Enter to Continue").X / 2, game.screenRectangle.Height * 3 / 4 + 50), Color.FromNonPremultiplied(250, 250, 250, 255));

            game.SpriteBatch.End();
        }
    }
}
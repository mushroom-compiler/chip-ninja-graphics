﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using chip_ninja.Components;
using chip_ninja.Graphics.Particule;
using chip_ninja.Graphics.PostProcess;
using chip_ninja.Graphics.PostProcess.NormalMapping;
using chip_ninja.Graphics.PostProcess.RayShaft;
using chip_ninja.Graphics.PostProcess.DepthOfField;
using chip_ninja.Background;
using chip_ninja.Enemies;
using chip_ninja.Interfaces;

namespace chip_ninja.GameScreens
{
    public class GameScreen : BaseGameState
    {
        //Stencil
        SpriteBatch spriteBatch;

        public Map map;
        public Camera camera;

        //public ParticleSystem pS;
        //ParticleSystemSettings pSS;

        ContentManager Content;

        SpriteBatch SpriteBatch;

        List<IAdverseEntity> enemyList;
        List<IAdverseEntity> tmpList;

        public List<IAdverseEntity> EnemyList
        {
            get { return enemyList; }
            set { enemyList = value; }
        }

        public BackgroundLevel1 bgLevel1;
        public BackgroundLevel2 bgLevel2;
        public BackgroundLevel3 bgLevel3;

        public BackgroundManager bgm;

        bool effects = false;

        public GameScreen(Game game, GameStateManager manager)
            : base(game, manager)
        {
            enemyList = new List<IAdverseEntity>();
            //enemyList.Add(new SniperMet(new Vector2(1500, 850)));
            //mettoru = new Met(new Vector2(1500, 850));
        }

        protected override void LoadContent()
        {
            Content = game.Content;
            //map = new Map("Level1", Content);

            bgm = new BackgroundManager(game);

            bgLevel1 = new BackgroundLevel1(game, GraphicsDevice);
            bgLevel2 = new BackgroundLevel2(game, GraphicsDevice);
            bgLevel3 = new BackgroundLevel3(game, GraphicsDevice);


            camera = new Camera(Vector2.Zero);

            SpriteBatch = game.SpriteBatch;


            foreach (var entity in enemyList)
            {
                entity.LoadContent(Content);
            }

            bgm.ChangeBackGround(bgLevel1);

            base.LoadContent();
        }

        public float lastTranslation;

        public override void Update(GameTime gameTime)
        {
            game.player.Update(gameTime, map, false, enemyList);
            if (game.player.Position.Y >= (map.Height - 1) * map.TileSize)
                game.player.Die();
            tmpList = new List<IAdverseEntity>(enemyList);

            List<IAdverseEntity> newSequence = enemyList.ConvertAll(new Converter<IAdverseEntity, IAdverseEntity>(Load));

            foreach (var entity in enemyList)
            {
                entity.Update(gameTime, map, game.player, tmpList);
            }

            enemyList = tmpList;

            lastTranslation = game.gameScreen.camera.matrix.Translation.X;
            camera.update(game.player);

            newSequence = enemyList.ConvertAll(new Converter<IAdverseEntity, IAdverseEntity>(Load));

            effects = game.effetcs;

            bgm.Update(gameTime, camera);

            if (game.player.FuryCounter > 10)
            {
                foreach (var entity in enemyList)
                {
                    if (!entity.IsAlive())
                    {
                        if (game.player.FuryCounter > 10)
                        {
                            game.stateManager.PushState(new GameScreens.EnemyCuttingScreen(game, game.stateManager, entity.GetTexture()));
                            game.player.FuryCounter = 0;
                        }
                    }
                }
            }

            enemyList.RemoveAll(x => !x.IsAlive());

            //game.enemyCuttingScreen.enemyToBeCut = enemyList[0].GetTexture();
            if (!game.player.IsAlive())
                game.stateManager.ChangeState(game.gameOverScreen);


            base.Update(gameTime);
        }

        public IAdverseEntity Load(IAdverseEntity entity)
        {
            game.Content = Content;

            if (entity.GetTexture() == null)
            {
                entity.LoadContent(Content);
            }
            return entity;
        }

        public override void Draw(GameTime gameTime)
        {
            bgm.Draw(gameTime, SpriteBatch, map, camera);

            base.Draw(gameTime);
        }
    }
}
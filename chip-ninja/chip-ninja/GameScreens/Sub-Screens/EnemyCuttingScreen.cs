﻿using System;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using chip_ninja.Components;
using chip_ninja.Graphics.Particule;
using chip_ninja.Graphics.PostProcess;
using chip_ninja.Graphics.PostProcess.NormalMapping;
using chip_ninja.Graphics.PostProcess.RayShaft;
using chip_ninja.Graphics.TextureCutting;

using VectorBatchLib;

namespace chip_ninja.GameScreens
{
    public class EnemyCuttingScreen : BaseGameState
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        RenderTarget2D rT1;

        Texture2D backgroundImage;

        PostProcessManager ppm;
        RayShaft rays;

        RenderTarget2D scene, bG, Map;

        ContentManager Content;

        SpriteBatch SpriteBatch;
        Rectangle screenRect;

        bool effects = false;

        Texture2D texture;
        BasicEffect effect;
        Matrix projection, view;

        GameStateManager stateManager;

        VectorBatch vectorBatch;

        World world;

        Body body;

        int finishCounter;

        Vector2 mouseDownPos, mouseUpPos;
        MouseState previousMouseState, mouseState;
        bool mouseDown = false;

        VertexPositionColorTexture[] points;
        int vertexCount;
        int[] indices;
        int triangleCount;
        int attackCounter;

        TexturedFixture texturedFixture;

        public ParticleSystem pS;
        ParticleSystemSettings pSS;
        //
        SpriteFont hellT;

        KeyboardState keyboardState, previousKeyboardState;

        public Texture2D enemyToBeCut;

        string sliceQuality = "Slices : High";

        public EnemyCuttingScreen(Game game, GameStateManager manager, Texture2D texture)
            : base(game, manager)
        {
            enemyToBeCut = texture;
            stateManager = manager;
            attackCounter = 6000;
        }

        protected override void LoadContent()
        {
            Content = game.Content;
            //map = new Map("Level1", Content);
            backgroundImage = Content.Load<Texture2D>(@"Backgrounds\titlescreen_test3");

            hellT = Content.Load<SpriteFont>("SpriteFonts/hell-T");

            ppm = new PostProcessManager(game);


            rays = new RayShaft(game, Vector2.One * .5f, @"Textures/flare", 1.5f, .97f, .97f, .5f, .25f, 64, new Vector2(16, 9));
            rays.lightPosOnScreen = Vector2.One * .5f;
            rays.mask.lightTex = Content.Load<Texture2D>("Textures/test_lense");

            screenRect = game.screenRectangle;
            scene = new RenderTarget2D(game.GraphicsDevice, screenRect.Width, screenRect.Height, false, SurfaceFormat.Color, DepthFormat.None);
            bG = new RenderTarget2D(game.GraphicsDevice, screenRect.Width, screenRect.Height, false, SurfaceFormat.Color, DepthFormat.None);
            Map = new RenderTarget2D(game.GraphicsDevice, screenRect.Width, screenRect.Height, false, SurfaceFormat.Color, DepthFormat.None);
            rT1 = new RenderTarget2D(game.GraphicsDevice, screenRect.Width, screenRect.Height);

            ppm.AddEffect(rays);
            ppm.ExtractParameters();

            SpriteBatch = game.SpriteBatch;

            pSS = new ParticleSystemSettings();
            pSS.SetLifeTimes(1.0f, 3f);
            pSS.SetScales(0.1f, 0.3f);
            pSS.ParticlesPerSec = 60.0f;
            pSS.InitialParticleCount = (int)(pSS.ParticlesPerSec * pSS.MaxLifeTime);
            pSS.SetDirectionAngles(160, 200);

            pS = new ParticleSystem(game, pSS);
            Components.Add(pS);
            pS.ParticuleTex = Content.Load<Texture2D>(@"Textures/particule_a");
            pS.OriginPosition = new Vector2(300, 300);
            pS.visible = true;

            //Cutting
            world = new World(new Vector2(0, -9.8f / 10f));
            BodyFactory.CreateEdge(world, new Vector2(-340, -80), new Vector2(340, -80));
            //world.AddController(new FarseerPhysics.Controllers.GravityController(100, 300, 20));

            vectorBatch = new VectorBatch(GraphicsDevice);

            projection = Matrix.CreateOrthographic(40 * GraphicsDevice.Viewport.AspectRatio, 40, 0, 1);
            view = Matrix.Identity;

            effect = new BasicEffect(GraphicsDevice)
            {
                Projection = projection,
                View = view,
                World = Matrix.Identity,
                VertexColorEnabled = true,
                TextureEnabled = true,
            };
            texture = enemyToBeCut;

            body = BodyFactory.CreateRectangle(world, texture.Width / 7f, texture.Height / 7f, 1, new TexturedFixture(texture, PolygonTools.CreateRectangle(texture.Width / 7f / 2f, texture.Height / 7f / 2f)));
            body.IsStatic = false;
            //body.IgnoreGravity = true;
            body.Mass = 0;

            base.LoadContent();
        }

        float time = 2.5f, step = 1f / 60f;
        int total_offset = 0;
        int i = 8, polygon_cte = 10;
        int dt;
        bool isLineVisible = false;




        public override void Update(GameTime gameTime)
        {

            attackCounter -= gameTime.ElapsedGameTime.Milliseconds;
            if (game.optionsScreen.buttons != null)
                sliceQuality = game.optionsScreen.buttons[1].text;

            time += (float)gameTime.ElapsedGameTime.TotalMinutes / 6;
            rays.Update(Content, time, screenRect);

            effects = game.effetcs;

            //Cutting
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                game.Exit();

            keyboardState = Keyboard.GetState();
            mouseState = Mouse.GetState();

            //dt = gameTime.ElapsedGameTime.Milliseconds;
            pS.OriginPosition = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

            if (previousMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Pressed)
            {
                i++;
                Vector3 unprojected = GraphicsDevice.Viewport.Unproject(new Vector3(mouseState.X, mouseState.Y, 0), projection, view, Matrix.Identity);
                mouseUpPos = new Vector2(unprojected.X, unprojected.Y);
                isLineVisible = true;
                pSS.InitialParticleCount *= 100;
                pSS.SetLifeTimes(1.0f, 6.0f);
                pSS.MaxSpeed *= 5;

                //TO BE REMOVED
                if (i < polygon_cte && sliceQuality == "Slices : High")
                    TexCutter.CutFirst(world, mouseDownPos, mouseUpPos, 0.1f);
                if (i < polygon_cte && sliceQuality == "Slices : Low" && world.BodyList.Count <= 50)
                    TexCutter.CutFirst(world, mouseDownPos, mouseUpPos, 0.1f);
            }
            else if (mouseState.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released)
            {
                Vector3 unprojected = GraphicsDevice.Viewport.Unproject(new Vector3(mouseState.X, mouseState.Y, 0), projection, view, Matrix.Identity);
                mouseDownPos = new Vector2(unprojected.X, unprojected.Y);
                mouseDown = true;
            }
            else
            {
                if (mouseDown)
                {
                    Vector3 unprojected = GraphicsDevice.Viewport.Unproject(new Vector3(mouseState.X, mouseState.Y, 0), projection, view, Matrix.Identity);
                    mouseUpPos = new Vector2(unprojected.X, unprojected.Y);
                }
            }

            //if (previousMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
            if (previousKeyboardState.IsKeyUp(Keys.R) && keyboardState.IsKeyDown(Keys.R) || previousMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
            {
                //mouseDownPos = new Vector2(0, 0);
                //mouseUpPos = new Vector2(0, 0);
                isLineVisible = false;
                dt = gameTime.ElapsedGameTime.Milliseconds;
                //TexCutter.CutFirst(world, mouseDownPos, mouseUpPos, 0.1f);
                //TO COMMENT
                if (i >= polygon_cte)
                    i = 0;
            }

            for (int i = 0; i < world.BodyList.Count; i++)
                if (attackCounter < 0 || keyboardState.IsKeyDown(Keys.NumPad9) && previousKeyboardState.IsKeyUp(Keys.NumPad9))
                {
                    //if (previousKeyboardState.IsKeyUp(Keys.Space) && keyboardState.IsKeyDown(Keys.Space))
                    world.Gravity = new Vector2(4.81f * 10f, 9.81f * 10f);
                }
                
            if (attackCounter < -1500)
                Finish();
           
            //if (world.BodyList.F Position.Y > game.screenRectangle.Y)
            //world.Gravity = new Vector2(

            //if (gameTime.ElapsedGameTime.Seconds % 10 == 0)
            //    world.Gravity *= new Vector2(-1, -1);

            if (mouseState.LeftButton == ButtonState.Released)
            {
                pSS.SetLifeTimes(1.0f, 3f);
                pSS.SetScales(0.1f, 0.3f);
                pSS.ParticlesPerSec = 60.0f;
                pSS.InitialParticleCount = (int)(pSS.ParticlesPerSec * pSS.MaxLifeTime);
                pSS.SetDirectionAngles(160, 200);
                pSS.MaxSpeed /= 5;
            }

            world.Step(step);

            previousKeyboardState = keyboardState;
            previousMouseState = mouseState;

            base.Update(gameTime);
        }



        float textAlphaAttenuation = 255;
        public override void Draw(GameTime gameTime)
        {

            game.gameScreen.bgm.Draw(gameTime, SpriteBatch, game.gameScreen.map, game.gameScreen.camera);
                //GraphicsDevice.SetRenderTarget(scene);
                //GraphicsDevice.Clear(ClearOptions.Target, Color.CornflowerBlue, 0, 0);

                //SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                //SpriteBatch.Draw(backgroundImage, new Rectangle(0, 0, screenRect.Width, screenRect.Height), Color.Black);
                //pS.Draw(SpriteBatch);
                //SpriteBatch.End();

                //SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                //map.Draw(SpriteBatch);
                //SpriteBatch.Draw(mask, new Rectangle(x, y, screenRect.Width, screenRect.Height), Color.Black);
                //game.player.Draw(SpriteBatch, gameTime);
                //SpriteBatch.End();

                //GraphicsDevice.SetRenderTarget(null);

                //ppm.Draw(gameTime, scene);

                //GraphicsDevice.SetRenderTarget(scene);

                //GraphicsDevice.Clear(Color.FromNonPremultiplied(138, 157, 179, 255));

                //SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                //SpriteBatch.Draw(backgroundImage, new Rectangle(0, 0, screenRect.Width, screenRect.Height), Color.White);
                //SpriteBatch.Draw(bg_color, new Rectangle(0, 0, screenRect.Width, screenRect.Height), Color.White);
                //pS.Draw(SpriteBatch);
                //SpriteBatch.End();
                //SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                //SpriteBatch.End();

                //GraphicsDevice.SetRenderTarget(null);
                //GraphicsDevice.Clear(Color.Black);

                //SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive);
                //SpriteBatch.Draw(ppm.scene, screenRect, Color.White);
                //SpriteBatch.Draw(scene, screenRect, Color.White);
                //pS.Draw(SpriteBatch);
                //SpriteBatch.End();

                //SpriteBatch.Begin(SpriteSortMode.Immediate, null, null, dSB1, null, alphaTest);
                //SpriteBatch.Draw(mask, new Rectangle(x, y, screenRect.Width, screenRect.Height), Color.White);
                //SpriteBatch.End();

                //SpriteBatch.Begin(SpriteSortMode.Immediate, null, null, dSB2, null, alphaTest);
                //SpriteBatch.Draw(bg_color, Vector2.Zero, Color.White);
                //SpriteBatch.End();

                //Alpha fix
                vectorBatch.Projection = projection;
                vectorBatch.View = view;

                if (isLineVisible)
                {
                    vectorBatch.Begin();
                    vectorBatch.DrawLine(mouseDownPos, mouseUpPos, Color.DarkOrange, 0.1f);
                    vectorBatch.End();
                }

                SpriteBatch.Begin();
                SpriteBatch.DrawString(hellT, (world.BodyList.Count - 1).ToString(), new Vector2(100, 10), Color.Red * textAlphaAttenuation, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
                SpriteBatch.End();
                //
                Console.WriteLine(world.BodyList.Count);

                foreach (var body in world.BodyList)
                    foreach (var fixture in body.FixtureList)
                        if (fixture.ShapeType == FarseerPhysics.Collision.Shapes.ShapeType.Polygon)
                        {
                            texturedFixture = (TexturedFixture)fixture.UserData;

                            //pixels = get_color_data(texturedFixture.texture);
                            //for (int i = 0; i < texture.Width; i++)
                            //    for (int j = 0; j < texture.Height; j++)
                            //        if (pixels[j * texture.Width + i] == Color.Black)
                            //            pixels[j * texture.Width + i] = Color.CornflowerBlue;

                            //texturedFixture.texture.GraphicsDevice.Textures[0] = null;
                            //texturedFixture.texture.SetData(pixels);
                            effect.Texture = texturedFixture.texture;

                            effect.CurrentTechnique.Passes[0].Apply();
                            texturedFixture.polygon.GetTriangleList(fixture.Body.Position, fixture.Body.Rotation, out points, out vertexCount, out indices, out triangleCount);

                            GraphicsDevice.SamplerStates[0] = SamplerState.AnisotropicWrap;
                            GraphicsDevice.RasterizerState = new RasterizerState() { FillMode = FillMode.Solid, CullMode = CullMode.None };

                            GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColorTexture>(PrimitiveType.TriangleList, points, 0, vertexCount, indices, 0, triangleCount);
                        }

                if (world.Gravity == new Vector2(4.81f * 10f, 9.81f * 10f))
                {
                    game.SpriteBatch.Begin();
                    game.SpriteBatch.DrawString(hellT, "Finished", new Vector2(game.screenRectangle.Width / 2 - hellT.MeasureString("Finished").X / 2, screenRect.Y + 400), Color.Red * textAlphaAttenuation, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
                    game.SpriteBatch.End();
                }
        }

        public void Finish()
        {
            stateManager.PopState();
        }
    }
}
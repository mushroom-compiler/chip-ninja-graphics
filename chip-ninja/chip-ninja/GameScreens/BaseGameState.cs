﻿using Microsoft.Xna.Framework;

using chip_ninja.Components;

namespace chip_ninja.GameScreens
{
    public abstract partial class BaseGameState : GameState
    {
        protected chip_ninja game;

        public BaseGameState(Game game, GameStateManager manager)
            : base(game, manager)
        {
            this.game = (chip_ninja)game;
        }
    }
}

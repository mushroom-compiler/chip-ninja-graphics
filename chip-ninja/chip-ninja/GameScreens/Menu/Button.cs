﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using chip_ninja.Components;

namespace chip_ninja.GameScreens.Menu
{
    public class Button
    {
        //public int width, height;
        //public Texture2D tex;
        public string text = "";
        public string name = "";
        public string action = "";
        public GameState screen;
        public List<string> textStates = new List<string>();
        public int index;

        public string currentLanguage = "Fr";

        public Button(/*int width, int height,*/ string text, GameState screen)
        {
            //this.width = width;
            //this.height = height;
            this.text = text;

            this.screen = screen;

            if (currentLanguage == "Eng")
                index = 1;
            else index = 2;
        }
        public Button(string text, string action)
        {
            this.text = text;
            this.action = action;
        }

        public Button()
        {
        }

        int engIndex;
        public void UpdateTextState()
        {
            if (currentLanguage == "Eng")
            {
                if (index == 3)
                    index = 1;

                index = index < 0 ? textStates.Count - 3 : index;
                index = index > textStates.Count - 3 ? 0 : index;

                engIndex = index;

                this.text = textStates[index];
            }
            else
            {
                if (index == 1)
                    index = 3;

                index = index < 2 ? textStates.Count - 1 : index;
                index = index > textStates.Count - 1 ? 2 : index;

                engIndex = index;

                this.text = textStates[index];
            }

            this.text = textStates[index];
        }

        public void DrawBox(SpriteBatch sB, Texture2D tex, SpriteFont font, Vector2 BoxPos, Vector2 textPos)
        {
            sB.Draw(tex, new Rectangle((int)BoxPos.X, (int)BoxPos.Y, tex.Width / 2, tex.Height / 2), Color.White);
            sB.DrawString(font, this.text, textPos, Color.FromNonPremultiplied(10, 10, 10, 255));
        }

        public void Draw(SpriteBatch sB, SpriteFont font, Vector2 pos, Color color)
        {
            sB.DrawString(font, this.text, pos, color);
        }

        public void Draw(SpriteBatch sB, SpriteFont font, Vector2 pos)
        {
            sB.DrawString(font, this.text, pos, Color.FromNonPremultiplied(250, 250, 250, 255));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using chip_ninja.Components;

namespace chip_ninja.GameScreens.Menu
{
    public class Box
    {
        public string text = "";
        public Texture2D tex;
        //public GameState screen;

        public Box(Texture2D tex)
        {
            this.tex = tex;
        }

        public void Draw(SpriteBatch sB, SpriteFont font, Vector2 BoxPos, Vector2 textPos)
        {
            sB.Draw(tex, new Rectangle((int)BoxPos.X, (int)BoxPos.Y + 20, (int)(tex.Width / 1.7f), tex.Height / 3), Color.White);
            sB.DrawString(font, this.text, textPos, Color.FromNonPremultiplied(10, 10, 10, 255));
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using chip_ninja.Components;

namespace chip_ninja.GameScreens.Menu
{
    public class Cursor
    {
        public Texture2D tex;
        public int index = 0;

        public List<Vector2> buttonsPos, boxesPos;

        public Cursor(Texture2D tex)
        {
            this.tex = tex;
            this.buttonsPos = new List<Vector2>();
            this.boxesPos = new List<Vector2>();
        }

        public void Draw(SpriteBatch sB, Vector2 pos)
        {
            pos.X -= 50;
            sB.Draw(this.tex, new Rectangle((int)pos.X, (int)pos.Y, tex.Width / 10, tex.Height / 10), Color.White);
            //sB.Draw(this.tex, new Rectangle((int)pos.X, (int)pos.Y, tex.Width, tex.Height), Color.White);
        }
    }
}

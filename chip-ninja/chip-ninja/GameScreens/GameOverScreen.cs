﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

using chip_ninja.Components;
using chip_ninja.GameScreens.Menu;

namespace chip_ninja.GameScreens
{
    public class GameOverScreen : BaseGameState
    {
        SpriteFont hell, hell_T;
        public List<GameScreens.Menu.Button> buttons;
        public GameScreens.Menu.Cursor cursor;

        int timeSpent;

        public GameOverScreen(chip_ninja game, GameStateManager manager)
            : base(game, manager)
        {
        }

        protected override void LoadContent()
        {
            ContentManager Content = game.Content;

            hell = Content.Load<SpriteFont>(@"SpriteFonts/hell");
            hell_T = Content.Load<SpriteFont>(@"SpriteFonts/hell-T");

            buttons = new List<GameScreens.Menu.Button>();
            buttons.Add(new GameScreens.Menu.Button("Resume", game.gameScreen));
            buttons[0].textStates.Add("Try again");
            buttons[0].textStates.Add("Reessayer");
            //buttons.Add(new GameScreens.Menu.Button("Options", game.multiplayerTitleScreen));
            cursor = new GameScreens.Menu.Cursor(Content.Load<Texture2D>("cursor"));

            base.LoadContent();
        }

        string currentLanguage = "Eng";
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (game.optionsScreen.buttons != null)
            {
                currentLanguage = game.optionsScreen.buttons[3].text;

                if (currentLanguage == "Language : English")
                    for (int i = 0; i < buttons.Count; i++)
                        buttons[i].text = buttons[i].textStates[0];
                else for (int i = 0; i < buttons.Count; i++)
                        buttons[i].text = buttons[i].textStates[1];
            }

            timeSpent += gameTime.ElapsedGameTime.Milliseconds;
        }

        public override void Draw(GameTime gameTime)
        {
            game.GraphicsDevice.Clear(Color.Black);

            game.SpriteBatch.Begin();

            base.Draw(gameTime);

            for (int i = 0; i < buttons.Count; i++)
            {
                cursor.buttonsPos.Add(new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString(buttons[i].text).X / 2, game.screenRectangle.Height * i / 22 + 800));
                buttons[i].Draw(game.SpriteBatch, hell, cursor.buttonsPos[i]);
            }

            cursor.Draw(game.SpriteBatch, cursor.buttonsPos[cursor.index]);
            game.SpriteBatch.DrawString(hell_T, "Game Over", new Vector2(game.screenRectangle.Width / 2 - hell_T.MeasureString("Game Over").X / 2, game.screenRectangle.Height / 2 - hell_T.MeasureString("Game Over").Y), Color.Red);

            game.SpriteBatch.End();
        }
    }
}

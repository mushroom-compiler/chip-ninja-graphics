﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using chip_ninja.Components;
using chip_ninja.Networking.Client;
using chip_ninja.Networking.Helper.Data;
using chip_ninja.Interfaces;
using chip_ninja.Animations;

namespace chip_ninja.GameScreens
{
    public class MultiplayerScreen : BaseGameState
    {
        List<Texture2D> bgList;
        int bgIndex = 0;

        Map map_multi;
        Camera camera;

        ContentManager Content;

        SpriteBatch SpriteBatch;
        Rectangle screenRect;

        Data.data player1Data, player2Data;
        Vector2 p1TmpPos, p2TmpPos;
        SpriteFont hell;

        public Client client;

        public MultiplayerScreen(Game game, GameStateManager manager)
            : base(game, manager)
        {

        }

        public override void Initialize()
        {

            base.Initialize();
        }

        protected override void LoadContent()
        {
            bgList = new List<Texture2D>();
            Content = game.Content;
            map_multi = new Map("Level_multi", Content, new List<IAdverseEntity>());

            hell = Content.Load<SpriteFont>(@"SpriteFonts/hell-T");

            //backgroundImage = Content.Load<Texture2D>(@"Backgrounds\titlescreen");
            //mouseMask = Content.Load<Texture2D>(@"Textures/MoutseMask");
            bgList.Add(Content.Load<Texture2D>(@"Backgrounds\Multi\multi_bg_1"));
            bgList.Add(Content.Load<Texture2D>(@"Backgrounds\Multi\multi_bg_1_i"));

            bgList.Add(Content.Load<Texture2D>(@"Backgrounds\Multi\multi_bg_2"));
            bgList.Add(Content.Load<Texture2D>(@"Backgrounds\Multi\multi_bg_2_i"));

            camera = new Camera(Vector2.Zero);

            screenRect = game.screenRectangle;
            SpriteBatch = game.SpriteBatch;

            //Networking
            player1Data = new Data.data();
            player2Data = new Data.data();

            game.player1.LoadContent(Content);
            game.player2.LoadContent(Content);

            p1TmpPos = new Vector2();
            p2TmpPos = new Vector2();

            string[] inputSplit = game.input.endText.Split(' ');
            //int port = 80;
            string ip = "127.0.0.1";
            Stopwatch sW = new Stopwatch();

            if (game.multiplayerTitleScreen.buttons[game.multiplayerTitleScreen.cursor.index].name == "Server") Process.Start(new ProcessStartInfo() { FileName = "chip-ninja.Networking.Server.exe" });
            else
            {
                //port = int.Parse(inputSplit[2]);
                ip = inputSplit[1];
            }

            //Console.WriteLine(ip);
            //To Be changed
            //Console.ReadLine();

            sW.Start();
            while (sW.Elapsed.Seconds <= 1)
            { }
            sW.Stop();

            game.bplayer1 = inputSplit[0] == "1" ? true : false;

            string nb_player = game.bplayer1 == true ? "1" : "2";

            client = new Client("player" + nb_player, ip);
            //End Networking

            MediaPlayer.Play(game.gameSong);

            game.player1.AttackState.Animation = new AnimationTest(Content.Load<Texture2D>("Attack_White"), 1, 1, 100, new Vector2(-45, 0));
            game.player1.WalkingState.Animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning_White"), 5, 1, 90);
            game.player1.StandingState.Animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning_White"), 5, 1, 90);
            game.player1.RunningState.Animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning_White"), 5, 1, 60);
            game.player1.JumpingState.Animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning_White"), 5, 1, 100);
            game.player1.DoubleJumpingState.Animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning_White"), 5, 1, 100);

            game.player2.AttackState.Animation = new AnimationTest(Content.Load<Texture2D>("Attack_Black"), 1, 1, 100, new Vector2(-45, 0));
            game.player2.WalkingState.Animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning_Black"), 5, 1, 90);
            game.player2.StandingState.Animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning_Black"), 5, 1, 90);
            game.player2.RunningState.Animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning_Black"), 5, 1, 60);
            game.player2.JumpingState.Animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning_Black"), 5, 1, 100);
            game.player2.DoubleJumpingState.Animation = new Animations.AnimationTest(Content.Load<Texture2D>("TestRunning_Black"), 5, 1, 100);

            game.player1.Opponent = game.player2;
            game.player2.Opponent = game.player1;
            


            base.LoadContent();
        }

        Rectangle hitbox1 = new Rectangle(), hitbox2 = new Rectangle();
        public override void Update(GameTime gameTime)
        {
            if (game.bplayer1 && client.connected && client.connectedClients.Count == 1)
            {
                p2TmpPos.X = client.connectedClients["player2"].x;
                p2TmpPos.Y = client.connectedClients["player2"].y;

                hitbox2 = new Rectangle(client.connectedClients["player2"].rectX, client.connectedClients["player2"].rectY, client.connectedClients["player2"].rectWidth, client.connectedClients["player2"].rectHeight);
                hitbox2 = new Rectangle(client.connectedClients["player2"].rectX, client.connectedClients["player2"].rectY, client.connectedClients["player2"].rectWidth, client.connectedClients["player2"].rectHeight);
                Console.WriteLine(client.connectedClients["player2"].rectWidth + " " + hitbox2.Width);
                if (hitbox2.Intersects(game.player1.Hitbox))
                    Console.WriteLine("I'm ded 2");

                bgIndex = client.connectedClients["player2"].index;

                player1Data.state = "position";
                game.player1.Update(gameTime, map_multi, true, new  List<IAdverseEntity>());
                player1Data.name = client.name;
                player1Data.x = game.player1.Position.X;
                player1Data.y = game.player1.Position.Y;
                player1Data.rectX = game.player1.CurrentState.DamageHitbox.X;
                player1Data.rectY = game.player1.CurrentState.DamageHitbox.Y;
                player1Data.rectWidth = game.player1.CurrentState.DamageHitbox.Width;
                player1Data.rectHeight = game.player1.CurrentState.DamageHitbox.Height;
                client.sendData(player1Data);

                //Lag-fix...?
                if (game.player2.Position == Vector2.Zero && p2TmpPos == Vector2.Zero)
                    ;//game.player2.Position = new Vector2(client.connectedClients["player2"].x, client.connectedClients["player2"].y);
                else
                    game.player2.Position = p2TmpPos;

                //Console.WriteLine(" Player 2 pos : " + client.connectedClients["player2"].x + " " + client.connectedClients["player2"].y);
                //game.player2.Update(gameTime, map_multi);
            }

            if (!game.bplayer1 && client.connected && client.connectedClients.Count == 1)
            {
                p1TmpPos.X = client.connectedClients["player1"].x;
                p1TmpPos.Y = client.connectedClients["player1"].y;

                hitbox1 = new Rectangle(client.connectedClients["player1"].rectX, client.connectedClients["player1"].rectY, client.connectedClients["player1"].rectWidth, client.connectedClients["player1"].rectHeight);

                if (hitbox1.Intersects(game.player2.Hitbox))
                    Console.WriteLine("I'm ded 1");

                bgIndex = client.connectedClients["player1"].index;

                game.player2.Update(gameTime, map_multi, true, new  List<IAdverseEntity>());
                player2Data.state = "position";
                player2Data.name = client.name;
                player2Data.x = game.player2.Position.X;
                player2Data.y = game.player2.Position.Y;
                player2Data.rectX = game.player2.CurrentState.DamageHitbox.X;
                player2Data.rectY = game.player2.CurrentState.DamageHitbox.Y;
                player2Data.rectWidth = game.player2.CurrentState.DamageHitbox.Width;
                player2Data.rectHeight = game.player2.CurrentState.DamageHitbox.Height;
                client.sendData(player2Data);
                game.player1.Opponent = game.player2;
                game.player2.Opponent = game.player1;

                //p2TmpPos.X = client.connectedClients["player2"].x;
                //p2TmpPos.Y = client.connectedClients["player2"].y;

                //Lag-fix...?
                if (game.player1.Position == Vector2.Zero && p1TmpPos == Vector2.Zero)
                    ;//game.player1.Position = new Vector2(client.connectedClients["player1"].x, client.connectedClients["player1"].y);
                else
                    game.player1.Position = p1TmpPos;

                //Console.WriteLine(" Player 1 pos : " + client.connectedClients["player1"].x + " " + client.connectedClients["player1"].y);
                //game.player1.Update(gameTime, map_multi);
            }

            //Console.WriteLine(client.connectedClients.Count);
            //if (client.connectedClients.Count == 1)

            camera.update(game.player1);

            if (game.player1.Life <= 0)
                Console.WriteLine("I'm ded 1 ");
            if (game.player2.Life <= 0)
                Console.WriteLine("I'm ded 2 ");

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            //SpriteBatch.Draw(backgroundImage, screenRect, Color.White);
            SpriteBatch.Draw(bgList[bgIndex], new Rectangle(0, 0, bgList[bgIndex].Width, bgList[bgIndex].Height), Color.White);
            map_multi.Draw(SpriteBatch);
            game.player1.Draw(SpriteBatch, gameTime);
            game.player2.Draw(SpriteBatch, gameTime);
            if (client.connectedClients.Count < 1)
                SpriteBatch.DrawString(hell, "Waiting for players...", new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString("Waiting for players...").X / 2, game.screenRectangle.Height / 22 + 350), Color.Red);
            SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}

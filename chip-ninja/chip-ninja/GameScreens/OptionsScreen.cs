﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

using chip_ninja.Components;
using chip_ninja.GameScreens.Menu;

namespace chip_ninja.GameScreens
{
    public class OptionsScreen : BaseGameState
    {
        Texture2D backgroundImage;

        SpriteFont hell, hell_T;
        public List<GameScreens.Menu.Button> buttons;
        public GameScreens.Menu.Cursor cursor;

        string currentLanguage = "Langue : Francais";

        Dictionary<string, List<string>> langPack = new Dictionary<string, List<string>>();

        int timeSpent;

        public OptionsScreen(chip_ninja game, GameStateManager manager)
            : base(game, manager)
        {
        }

        protected override void LoadContent()
        {
            ContentManager Content = game.Content;
            //MediaPlayer.Play(game.menuSong);
            backgroundImage = Content.Load<Texture2D>(@"Backgrounds\titlescreen");

            hell = Content.Load<SpriteFont>(@"SpriteFonts/hell");
            hell_T = Content.Load<SpriteFont>(@"SpriteFonts/hell-T");

            langPack.Add("Language : English", new List<string>() {  "You need at least Shader Model 3.0 for the game to play well", "Adjust number of slices that can be performed on an enemy", "Play or Pause music", "Select your language" });
            langPack.Add("Langue : Francais", new List<string>() {  "Vous avez besoin du Shader Model 3.0 au minimum pour pouvoir jouer", "Definir le nombre de decoupes possibles sur un ennemi", "Joue ou pause la musique", "Choisissez votre langue" });

            buttons = new List<GameScreens.Menu.Button>();
            buttons.Add(new GameScreens.Menu.Button("Graphics : High", "Define"));
            buttons[0].textStates.Add("Graphics : High");
            buttons[0].textStates.Add("Graphics : Low");
            buttons[0].textStates.Add("Graphismes : Eleves");
            buttons[0].textStates.Add("Graphismes : Faibles");

            buttons.Add(new GameScreens.Menu.Button("Slices : High", "Define"));
            buttons[1].textStates.Add("Slices : High");
            buttons[1].textStates.Add("Slices : Low");
            buttons[1].textStates.Add("Decoupes : Elevees");
            buttons[1].textStates.Add("Decoupes : Faibles");

            buttons.Add(new GameScreens.Menu.Button("Music : On", "Define"));
            buttons[2].textStates.Add("Music : Off");
            buttons[2].textStates.Add("Music : On");
            buttons[2].textStates.Add("Musique : Off");
            buttons[2].textStates.Add("Musique : On");

            buttons.Add(new GameScreens.Menu.Button("Language : English", "Define"));
            buttons[3].textStates.Add("Language : English");
            buttons[3].textStates.Add("Langue : Francais");
            buttons[3].textStates.Add("Language : English");
            buttons[3].textStates.Add("Langue : Francais");

            //buttons.Add(new GameScreens.Menu.Button("Options", game.multiplayerTitleScreen));
            cursor = new GameScreens.Menu.Cursor(Content.Load<Texture2D>("cursor"));

            //cursor = new Cursor(Content.Load<Texture2D>("cursor"));

            //buttons.Add(new Button("Option", game.titleScreen));
            //buttons.Add(new Button("Exit game", game.titleScreen));

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            //game.player.Position = Vector2.Zero;
            //game.player.Velocity = Vector2.Zero;

            for (int i = 0; i < buttons.Count; i++)
                buttons[i].UpdateTextState();

            currentLanguage = buttons[3].text;

            if (currentLanguage == "Language : English")
                for (int i = 0; i < buttons.Count; i++)
                    buttons[i].currentLanguage = "Eng";
            else for (int i = 0; i < buttons.Count; i++)
                    buttons[i].currentLanguage = "Fr";

            if (buttons[0].text == "Graphics : High" || buttons[0].text == "Graphismes : Eleves")
                game.effetcs = true;
            else game.effetcs = false;

            timeSpent += gameTime.ElapsedGameTime.Milliseconds;
        }

        public override void Draw(GameTime gameTime)
        {
            game.SpriteBatch.Begin();

            base.Draw(gameTime);

            game.SpriteBatch.Draw(backgroundImage, game.screenRectangle, Color.White);

            for (int i = 0; i < buttons.Count; i++)
            {
                cursor.buttonsPos.Add(new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString(buttons[i].text).X / 2, game.screenRectangle.Height * i / 22 + 800));
                buttons[i].Draw(game.SpriteBatch, hell, cursor.buttonsPos[i]);
            }

            cursor.Draw(game.SpriteBatch, cursor.buttonsPos[cursor.index]);
            game.SpriteBatch.DrawString(hell_T, "Options", new Vector2(game.screenRectangle.Width / 2 - hell_T.MeasureString("Chip Nina").X / 2, 100), Color.FromNonPremultiplied(250, 250, 250, 255));

            //if (((timeSpent / 1000) % 2) == 1)
            //    game.SpriteBatch.DrawString(hell, "Press Enter to Continue", new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString("Press Enter to Continue").X / 2, game.screenRectangle.Height * 3 / 4 + 50), Color.FromNonPremultiplied(250, 250, 250, 255));

            if (cursor.index == 0)
                game.SpriteBatch.DrawString(hell, langPack[currentLanguage][0], new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString(langPack[currentLanguage][0]).X / 2, 700), Color.FromNonPremultiplied(250, 250, 250, 255));
            if (cursor.index == 1)
                game.SpriteBatch.DrawString(hell, langPack[currentLanguage][1], new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString(langPack[currentLanguage][1]).X / 2, 700), Color.FromNonPremultiplied(250, 250, 250, 255));
            if (cursor.index == 2)
                game.SpriteBatch.DrawString(hell, langPack[currentLanguage][2], new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString(langPack[currentLanguage][2]).X / 2, 700), Color.FromNonPremultiplied(250, 250, 250, 255));
            if (cursor.index == 3)
                game.SpriteBatch.DrawString(hell, langPack[currentLanguage][3], new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString(langPack[currentLanguage][3]).X / 2, 700), Color.FromNonPremultiplied(250, 250, 250, 255));

            game.SpriteBatch.End();
        }
    }
}

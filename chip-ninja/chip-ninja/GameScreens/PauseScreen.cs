﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

using chip_ninja.Components;
using chip_ninja.GameScreens.Menu;

namespace chip_ninja.GameScreens
{
    public class PauseScreen : BaseGameState
    {
        Texture2D backgroundImage;

        SpriteFont hell, hell_T;
        public List<GameScreens.Menu.Button> buttons;
        public GameScreens.Menu.Cursor cursor;

        int timeSpent;

        public PauseScreen(chip_ninja game, GameStateManager manager)
            : base(game, manager)
        {
        }

        protected override void LoadContent()
        {
            ContentManager Content = game.Content;
            backgroundImage = Content.Load<Texture2D>(@"Backgrounds\titlescreen");

            hell = Content.Load<SpriteFont>(@"SpriteFonts/hell");
            hell_T = Content.Load<SpriteFont>(@"SpriteFonts/hell-T");

            buttons = new List<GameScreens.Menu.Button>();
            buttons.Add(new GameScreens.Menu.Button("Resume", game.gameScreen));
            buttons[0].textStates.Add("Resume");
            buttons[0].textStates.Add("Continuer");
            //buttons.Add(new GameScreens.Menu.Button("Options", game.multiplayerTitleScreen));
            cursor = new GameScreens.Menu.Cursor(Content.Load<Texture2D>("cursor"));

            //cursor = new Cursor(Content.Load<Texture2D>("cursor"));

            //buttons.Add(new Button("Option", game.titleScreen));
            //buttons.Add(new Button("Exit game", game.titleScreen));

            base.LoadContent();
        }

        string currentLanguage = "Eng";
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (game.optionsScreen.buttons != null)
            {
                currentLanguage = game.optionsScreen.buttons[3].text;

                if (currentLanguage == "Language : English")
                    for (int i = 0; i < buttons.Count; i++)
                        buttons[i].text = buttons[i].textStates[0];
                else for (int i = 0; i < buttons.Count; i++)
                        buttons[i].text = buttons[i].textStates[1];
            }

            timeSpent += gameTime.ElapsedGameTime.Milliseconds;
        }

        public override void Draw(GameTime gameTime)
        {
            game.SpriteBatch.Begin();

            base.Draw(gameTime);

            game.SpriteBatch.Draw(backgroundImage, game.screenRectangle, Color.White);

            for (int i = 0; i < buttons.Count; i++)
            {
                cursor.buttonsPos.Add(new Vector2(game.screenRectangle.Width / 2 - hell.MeasureString(buttons[i].text).X / 2, game.screenRectangle.Height * i / 22 + 800));
                buttons[i].Draw(game.SpriteBatch, hell, cursor.buttonsPos[i]);
            }

            cursor.Draw(game.SpriteBatch, cursor.buttonsPos[cursor.index]);
            game.SpriteBatch.DrawString(hell_T, "Pause", new Vector2(game.screenRectangle.Width / 2 - hell_T.MeasureString("Chip Nina").X / 2, 100), Color.FromNonPremultiplied(250, 250, 250, 255));

            game.SpriteBatch.End();
        }
    }
}

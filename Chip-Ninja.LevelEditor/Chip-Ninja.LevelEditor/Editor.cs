using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;
using System.Text;

using chip_ninja.LevelEditor.Menu;

namespace chip_Ninja.LevelEditor
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        SpriteFont hell;

        Texture2D panel, block, lineHoriz, lineVert;
        Texture2D met, sniper, bat;
        Texture2D textBox;
        Rectangle blockRect, mouseRect, metRect, sniperRect, batRect, helpRect, loadRect, dimensionsRect;

        Matrix camera;

        int mWidth = 300;
        int mHeight = 300;

        int[,] map;

        //
        Keys[] keys;
        bool[] anykeyUp;
        string[] SC_US = { ")", "!", "@", "#", "$", "%", "^", "&", "*", "(" };
        string[] SC_FR = { "&", "�", "\"", "'", "(", "-", "�", "_", "�", "�" };
        public string dimensionsText = "";
        public string loadText = "";

        bool dimensionsFocus = true;
        bool loadFocus = false;

        Box mapSizeBox, loadBox;
        //

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;

            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        bool blockChosen = false, metChosen = false, sniperChosen = false, batChosen = false;
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            hell = Content.Load<SpriteFont>("SpriteFonts/hell");

            panel = Content.Load<Texture2D>("panel");
            block = Content.Load<Texture2D>("block");
            met = Content.Load<Texture2D>("met");
            sniper = Content.Load<Texture2D>("metallsniper");
            bat = Content.Load<Texture2D>("bat");

            lineHoriz = Content.Load<Texture2D>("line_horiz");
            lineVert = Content.Load<Texture2D>("line_vert");

            textBox = Content.Load<Texture2D>(@"textbox");

            blockRect = new Rectangle(5, GraphicsDevice.Viewport.Height - panel.Height + 5, block.Width, block.Height);
            metRect = new Rectangle(10 + block.Width, GraphicsDevice.Viewport.Height - panel.Height + 5, met.Width, met.Height);
            sniperRect = new Rectangle(15 + block.Width + met.Width, GraphicsDevice.Viewport.Height - panel.Height + 5, sniper.Width, sniper.Height);
            batRect = new Rectangle(15 + block.Width + met.Width + sniper.Width, GraphicsDevice.Viewport.Height - panel.Height + 5, sniper.Width, sniper.Height);
            helpRect = new Rectangle(10, GraphicsDevice.Viewport.Height - 50, (int)hell.MeasureString("Help").X, (int)hell.MeasureString("Help").Y);
            loadRect = new Rectangle(GraphicsDevice.Viewport.Width - 300 - textBox.Width + 100, GraphicsDevice.Viewport.Height - 120, textBox.Width, textBox.Height);
            dimensionsRect = new Rectangle(GraphicsDevice.Viewport.Width - 300, GraphicsDevice.Viewport.Height - 120, textBox.Width, textBox.Height);

            mapSizeBox = new Box(textBox);
            loadBox = new Box(textBox);

            map = new int[mWidth, mHeight];
            for (int i = 0; i < mWidth; i++)
                for (int j = 0; j < mHeight; j++)
                    map[i, j] = 0;

            camera = Matrix.Identity;

            //
            keys = new Keys[160];
            Keys[] tempkeys;
            tempkeys = Enum.GetValues(typeof(Keys)).Cast<Keys>().ToArray<Keys>();
            int t = 0;

            for (int i = 0; i < tempkeys.Length; i++)
            {
                if (i == 1 || i == 11 || (i > 26 && i < 63) || i >= 63)
                {
                    keys[t] = tempkeys[i];
                    t++;
                }
            }

            anykeyUp = new bool[keys.Length];

            for (int i = 0; i < keys.Length; i++)
                anykeyUp[i] = true;

            //

        }

        Vector2 position = new Vector2(0, 0);

        Vector2 currentBlockPosition;

        KeyboardState lastKeyboardState, keyboardState;

        bool help = false;
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();

            mouseRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, 10, 10);

            lastKeyboardState = keyboardState;
            keyboardState = Keyboard.GetState();

            if (lastKeyboardState.IsKeyUp(Keys.F1) && keyboardState.IsKeyDown(Keys.F1))
                for (int i = 0; i < mWidth; i++)
                    for (int j = 0; j < mHeight; j++)
                        map[i, j] = 0;

            if (Mouse.GetState().LeftButton == ButtonState.Pressed && (blockChosen || metChosen || sniperChosen || batChosen || keyboardState.IsKeyDown(Keys.R)))
            {
                Vector2 mPos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

                currentBlockPosition = new Vector2((float)Math.Floor((decimal)((mPos.X + position.X) / block.Width)), (float)Math.Floor((decimal)((mPos.Y + position.Y) / block.Height)));

                if (blockChosen)
                    map[(int)(currentBlockPosition.X < 0 ? 0 : currentBlockPosition.X), (int)(currentBlockPosition.Y < 0 ? 0 : currentBlockPosition.Y)] = 2;
                else if (metChosen)
                    map[(int)(currentBlockPosition.X < 0 ? 0 : currentBlockPosition.X), (int)(currentBlockPosition.Y < 0 ? 0 : currentBlockPosition.Y)] = 30;
                else if (sniperChosen)
                    map[(int)(currentBlockPosition.X < 0 ? 0 : currentBlockPosition.X), (int)(currentBlockPosition.Y < 0 ? 0 : currentBlockPosition.Y)] = 31;
                else if (batChosen)
                    map[(int)(currentBlockPosition.X < 0 ? 0 : currentBlockPosition.X), (int)(currentBlockPosition.Y < 0 ? 0 : currentBlockPosition.Y)] = 32;

                if (keyboardState.IsKeyDown(Keys.R))
                    map[(int)(currentBlockPosition.X < 0 ? 0 : currentBlockPosition.X), (int)(currentBlockPosition.Y < 0 ? 0 : currentBlockPosition.Y)] = 0;
            }


            if (Mouse.GetState().LeftButton == ButtonState.Pressed && blockChosen && keyboardState.IsKeyDown(Keys.P))
            {
                Vector2 mPos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

                currentBlockPosition = new Vector2((float)Math.Floor((decimal)((mPos.X + position.X) / block.Width)), (float)Math.Floor((decimal)((mPos.Y + position.Y) / block.Height)));

                map[(int)(currentBlockPosition.X < 0 ? 0 : currentBlockPosition.X), (int)(currentBlockPosition.Y < 0 ? 0 : currentBlockPosition.Y)] = 1;
            }

            if (Mouse.GetState().RightButton == ButtonState.Pressed)
            {
                //X
                if (Mouse.GetState().X > GraphicsDevice.Viewport.Width - 150 && position.X <= 0)
                    camera.Translation = new Vector3(position.X--, position.Y, 0);

                if (Mouse.GetState().X < 150 && position.X <= 0)
                    camera.Translation = new Vector3(position.X++, position.Y, 0);

                if (position.X >= 0)
                    position.X = 0;

                //Y
                if (Mouse.GetState().Y < GraphicsDevice.Viewport.Height - panel.Height - 10 && position.Y <= 0)
                    camera.Translation = new Vector3(position.X, position.Y++, 0);

                if (Mouse.GetState().Y > 20 && position.Y <= 0 && position.Y <= 0)
                    camera.Translation = new Vector3(position.X, position.Y--, 0);

                if (position.Y >= 0)
                    position.Y = 0;

                Console.WriteLine(position);
            }

            if (mouseRect.Intersects(blockRect) && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                batChosen = false;
                sniperChosen = false;
                metChosen = false;
                blockChosen = true;
            }

            if (mouseRect.Intersects(metRect) && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                batChosen = false;
                sniperChosen = false;
                metChosen = true;
                blockChosen = false;
            }

            if (mouseRect.Intersects(sniperRect) && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                batChosen = false;
                sniperChosen = true;
                metChosen = false;
                blockChosen = false;
            }
            if (mouseRect.Intersects(batRect) && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                batChosen = true;
                sniperChosen = false;
                metChosen = false;
                blockChosen = false;
            }

            if (mouseRect.Intersects(loadRect) && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                loadFocus = true;
                dimensionsFocus = false;
            }

            if (mouseRect.Intersects(dimensionsRect) && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                loadFocus = false;
                dimensionsFocus = true;
            }

            help = false;
            if (mouseRect.Intersects(helpRect))
                help = true;

            if (keyboardState.IsKeyDown(Keys.G) && lastKeyboardState.IsKeyUp(Keys.G))
                isGridVisible = !isGridVisible;

            if (keyboardState.IsKeyDown(Keys.E) && lastKeyboardState.IsKeyUp(Keys.E))
                ExportMap();


            if (dimensionsFocus && !keyboardState.IsKeyDown(Keys.R) && !keyboardState.IsKeyDown(Keys.G) && !keyboardState.IsKeyDown(Keys.E) && !keyboardState.IsKeyDown(Keys.P))
                handleTextInput();
            else if (loadFocus)
                handleTextInput();

            mapSizeBox.text = dimensionsText;
            loadBox.text = loadText;

            if (dimensionsFocus && keyboardState.IsKeyDown(Keys.Enter) && lastKeyboardState.IsKeyUp(Keys.Enter) && dimensionsText.Contains('x'))
            {
                string[] s = dimensionsText.Split('x');

                mWidth = int.Parse(s[0]);
                mHeight = int.Parse(s[1]);

                map = new int[mWidth, mHeight];
                for (int i = 0; i < mWidth; i++)
                    for (int j = 0; j < mHeight; j++)
                        map[i, j] = 0;
            }

            if (loadFocus && keyboardState.IsKeyDown(Keys.Enter) && lastKeyboardState.IsKeyUp(Keys.Enter) && loadText != null)
                ImportMap();


            base.Update(gameTime);
        }

        public void OpenMap()
        {
            //try
            //{
            //    using (StreamReader sr = new StreamReader("C:\"))
            //    {
            //        String line = sr.ReadToEnd();
            //        Console.WriteLine(line);
            //    }
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("The file could not be read:");
            //    Console.WriteLine(e.Message);
            //}
        }

        public void ImportMap()
        {
            string path = @"C:\Temp\" + loadText + ".txt";

            int rwidth = 0;
            int rheight = File.ReadLines(path).Count();

            StreamReader sReader = new StreamReader(path);
            string line = sReader.ReadLine();
            line = sReader.ReadLine();
            string[] tileNo = line.Split(',');

            rwidth = tileNo.Count();
            map = new int[rheight, rwidth];
            sReader.Close();

            sReader = new StreamReader(path);

            line = sReader.ReadLine();
            tileNo = line.Split(',');

            mHeight = Convert.ToInt32(tileNo[0]);
            mWidth = Convert.ToInt32(tileNo[1]);

            for (int y = 0; y < rheight - 1; y++)
            {
                line = sReader.ReadLine();
                tileNo = line.Split(',');

                for (int x = 0; x < rwidth; x++)
                    map[y, x] = Convert.ToInt32(tileNo[x]);
            }
            sReader.Close();
            mHeight = rheight - 1;
            mWidth = rwidth - 1;
        }

        public void ExportMap()
        {
            string fileName = @"C:\Temp\map.txt";
            FileInfo fI = new FileInfo(fileName);

            try
            {
                if (fI.Exists)
                    fI.Delete();

                using (FileStream fs = fI.Create())
                {
                    Byte[] txt = new UTF8Encoding(true).GetBytes(mWidth + "," + mHeight + "," + 20 + "," + 1 + "," + 0 + "," + 0 + "," + 1 + "\n");
                    fs.Write(txt, 0, txt.Length);
                    byte[] t;
                    for (int i = 0; i < mWidth; i++)
                    {
                        byte[] a = new UTF8Encoding(true).GetBytes(map[i, 0].ToString());
                        fs.Write(a, 0, a.Length);
                        for (int j = 1; j < mHeight; j++)
                        {
                            t = new UTF8Encoding(true).GetBytes("," + map[i, j]);
                            fs.Write(t, 0, t.Length);
                        }
                        t = new UTF8Encoding(true).GetBytes("\n");
                        fs.Write(t, 0, t.Length);
                    }

                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        bool isGridVisible = true;
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, camera);

            if (isGridVisible)
            {
                for (int j = 0; j < 3 * GraphicsDevice.Viewport.Width; j += GraphicsDevice.Viewport.Width)
                    for (int i = 0; i < GraphicsDevice.Viewport.Height; i += block.Height)
                        spriteBatch.Draw(lineHoriz, new Vector2(j, i), Color.White);

                for (int i = 0; i < 3 * GraphicsDevice.Viewport.Width; i += block.Width)
                    spriteBatch.Draw(lineVert, new Vector2(i, 0), Color.White);
            }

            for (int i = 0; i < 100; i++)
                for (int j = 0; j < 100; j++)
                {
                    if (map[i, j] == 2 || map[i, j] == 1)
                        spriteBatch.Draw(block, new Vector2(i * block.Width, j * block.Height), Color.White);
                    if (map[i, j] == 30)
                        spriteBatch.Draw(met, new Vector2(i * block.Width, j * block.Height), Color.White);
                    if (map[i, j] == 31)
                        spriteBatch.Draw(sniper, new Vector2(i * block.Width, j * block.Height), Color.White);
                    if (map[i, j] == 32)
                        spriteBatch.Draw(bat, new Vector2(i * block.Width, j * block.Height), Color.White);
                }
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);
            spriteBatch.Draw(panel, new Vector2(0, GraphicsDevice.Viewport.Height - panel.Height), Color.White);
            spriteBatch.Draw(block, new Vector2(5, GraphicsDevice.Viewport.Height - panel.Height + 5), Color.White);
            spriteBatch.Draw(met, new Vector2(5 + block.Width, GraphicsDevice.Viewport.Height - panel.Height + 5), Color.White);
            spriteBatch.Draw(sniper, new Vector2(5 + met.Width + block.Width, GraphicsDevice.Viewport.Height - panel.Height + 5), Color.White);
            spriteBatch.Draw(bat, new Vector2(5 + met.Width + block.Width + sniper.Width, GraphicsDevice.Viewport.Height - panel.Height + 5), Color.White);

            if (blockChosen)
                spriteBatch.Draw(block, new Vector2(Mouse.GetState().X, Mouse.GetState().Y), Color.White);
            else if (metChosen)
                spriteBatch.Draw(met, new Vector2(Mouse.GetState().X, Mouse.GetState().Y), Color.White);
            else if (sniperChosen)
                spriteBatch.Draw(sniper, new Vector2(Mouse.GetState().X, Mouse.GetState().Y), Color.White);
            else if (batChosen)
                spriteBatch.Draw(bat, new Vector2(Mouse.GetState().X, Mouse.GetState().Y), Color.White);

            spriteBatch.DrawString(hell, "Map Dimensions :", new Vector2(GraphicsDevice.Viewport.Width - 265, GraphicsDevice.Viewport.Height - 140), Color.Red);
            mapSizeBox.Draw(spriteBatch, hell, new Vector2(GraphicsDevice.Viewport.Width - 300, GraphicsDevice.Viewport.Height - 120), new Vector2(GraphicsDevice.Viewport.Width - 290, GraphicsDevice.Viewport.Height - 90));

            spriteBatch.DrawString(hell, "Load Map :", new Vector2(GraphicsDevice.Viewport.Width - 635, GraphicsDevice.Viewport.Height - 140), Color.Red);
            loadBox.Draw(spriteBatch, hell, new Vector2(GraphicsDevice.Viewport.Width - 300 - textBox.Width + 100, GraphicsDevice.Viewport.Height - 120), new Vector2(loadRect.X + 10, loadRect.Y + 30));

            spriteBatch.DrawString(hell, "Level Editor", new Vector2(GraphicsDevice.Viewport.Width / 2 - hell.MeasureString("Level Editor").X / 2, 0), Color.Red);
            spriteBatch.DrawString(hell, "Help", new Vector2(10, GraphicsDevice.Viewport.Height - 50), Color.Red);
            if (help)
                spriteBatch.DrawString(hell, "Right click + side : scoll \nG : Grid on or off \nF1 : Reset map \nLeft click + R : Remove a block or ennemy from the map \nE : Export Map   |   Left Click + P : Pass-through blocks", new Vector2(60 + hell.MeasureString("Help").X, GraphicsDevice.Viewport.Height - 155), Color.Red);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        public void handleTextInput()
        {
            KeyboardState keyboardstate = Keyboard.GetState();
            int i = 0;

            string finalText = "";

            if (loadFocus)
                finalText = loadText;
            else if (dimensionsFocus)
                finalText = dimensionsText;

            foreach (Keys key in keys)
            {
                if (keyboardstate.IsKeyDown(key))
                {
                    if (anykeyUp[i])
                    {
                        if (key == Keys.Back && finalText != "")
                            finalText = finalText.Remove(finalText.Length - 1);
                        if (key == Keys.Space)
                            finalText += " ";
                        if (key == Keys.OemPeriod)
                            finalText += ".";
                        //if (key == Keys.Decimal)
                        //endText += ".";
                        if (i > 1 && i < 12)
                        {
                            if (keyboardstate.IsKeyDown(Keys.RightShift) || keyboardstate.IsKeyDown(Keys.LeftShift))
                                finalText += key.ToString()[1];
                            else finalText += SC_US[i - 2];
                        }
                        if (i > 11 && i < 38)
                        {
                            if (keyboardstate.IsKeyDown(Keys.RightShift) || keyboardstate.IsKeyDown(Keys.LeftShift))
                                finalText += key.ToString();
                            else finalText += key.ToString().ToLower();
                        }
                        if (i == 40)
                            finalText += ".";
                    }

                    anykeyUp[i] = false;
                }

                else if (keyboardstate.IsKeyUp(key)) anykeyUp[i] = true;
                i++;
            }

            if (loadFocus)
                loadText = finalText;
            else if (dimensionsFocus)
                dimensionsText = finalText;
        }
    }
}
